# How to Use #

## Install dependencies
`composer install`

## Database
Restore database from **blogcraft.sql**

## Setup
* Create a copy of `.env.example` and rename it to `.env`
* Update the corresponding data