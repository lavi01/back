-- phpMyAdmin SQL Dump
-- version 4.9.7deb1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Nov 15, 2021 at 02:37 PM
-- Server version: 8.0.27-0ubuntu0.21.04.1
-- PHP Version: 7.4.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `blogcraft`
--

-- --------------------------------------------------------

--
-- Table structure for table `blg_announcements`
--

CREATE TABLE `blg_announcements` (
  `id` int NOT NULL,
  `userId` int NOT NULL,
  `pluginId` int DEFAULT NULL,
  `heading` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `body` text COLLATE utf8_unicode_ci NOT NULL,
  `unread` tinyint(1) NOT NULL DEFAULT '1',
  `dateRead` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `blg_assetindexdata`
--

CREATE TABLE `blg_assetindexdata` (
  `id` int NOT NULL,
  `sessionId` varchar(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `volumeId` int NOT NULL,
  `uri` text COLLATE utf8_unicode_ci,
  `size` bigint UNSIGNED DEFAULT NULL,
  `timestamp` datetime DEFAULT NULL,
  `recordId` int DEFAULT NULL,
  `inProgress` tinyint(1) DEFAULT '0',
  `completed` tinyint(1) DEFAULT '0',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `blg_assets`
--

CREATE TABLE `blg_assets` (
  `id` int NOT NULL,
  `volumeId` int DEFAULT NULL,
  `folderId` int NOT NULL,
  `uploaderId` int DEFAULT NULL,
  `filename` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `kind` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'unknown',
  `width` int UNSIGNED DEFAULT NULL,
  `height` int UNSIGNED DEFAULT NULL,
  `size` bigint UNSIGNED DEFAULT NULL,
  `focalPoint` varchar(13) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deletedWithVolume` tinyint(1) DEFAULT NULL,
  `keptFile` tinyint(1) DEFAULT NULL,
  `dateModified` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `blg_assets`
--

INSERT INTO `blg_assets` (`id`, `volumeId`, `folderId`, `uploaderId`, `filename`, `kind`, `width`, `height`, `size`, `focalPoint`, `deletedWithVolume`, `keptFile`, `dateModified`, `dateCreated`, `dateUpdated`, `uid`) VALUES
(10, 1, 1, 1, 'post-10.jpeg', 'image', 920, 613, 248549, NULL, NULL, NULL, '2021-11-15 03:28:32', '2021-11-15 03:28:32', '2021-11-15 03:28:32', '8c7af28d-e3e2-4672-b2f2-f0ff6fec5375'),
(19, 1, 1, 1, 'post-01.jpeg', 'image', 603, 920, 65140, NULL, NULL, NULL, '2021-11-15 03:45:07', '2021-11-15 03:45:07', '2021-11-15 03:45:07', 'ebc2be9a-18e0-435f-9ba6-39bd36d0aaa7'),
(20, 1, 1, 1, 'post-02.jpeg', 'image', 480, 720, 41614, NULL, NULL, NULL, '2021-11-15 03:45:09', '2021-11-15 03:45:09', '2021-11-15 03:45:09', 'e55f52ae-a286-4982-b5d6-a47dd16abcb2'),
(21, 1, 1, 1, 'post-03.jpeg', 'image', 484, 720, 44087, NULL, NULL, NULL, '2021-11-15 03:45:10', '2021-11-15 03:45:10', '2021-11-15 03:45:10', '5e7eca45-0e93-4f5f-8eba-4922e66b8366'),
(22, 1, 1, 1, 'post-04.jpeg', 'image', 480, 720, 22485, NULL, NULL, NULL, '2021-11-15 03:45:11', '2021-11-15 03:45:11', '2021-11-15 03:45:11', '7b3e2f4e-8a3b-497c-bbfe-958a8d928b07'),
(23, 1, 1, 1, 'post-05.jpeg', 'image', 568, 426, 75220, NULL, NULL, NULL, '2021-11-15 03:45:12', '2021-11-15 03:45:12', '2021-11-15 03:45:12', '3316f296-ebd4-4487-9ae1-f82d773b9cad'),
(24, 1, 1, 1, 'post-06.jpeg', 'image', 568, 379, 29006, NULL, NULL, NULL, '2021-11-15 03:45:13', '2021-11-15 03:45:13', '2021-11-15 03:45:13', 'f127a9bd-1631-438d-9682-e098ab8a506d'),
(25, 1, 1, 1, 'post-07.jpeg', 'image', 568, 379, 72140, NULL, NULL, NULL, '2021-11-15 03:45:14', '2021-11-15 03:45:14', '2021-11-15 03:45:14', '99fef96d-ad8b-45d6-929b-db74a262381a'),
(26, 1, 1, 1, 'post-08.jpeg', 'image', 680, 453, 93565, NULL, NULL, NULL, '2021-11-15 03:45:16', '2021-11-15 03:45:16', '2021-11-15 03:45:16', '53587787-dc9f-4edf-8197-73f68117fef0'),
(27, 1, 1, 1, 'post-09.jpeg', 'image', 480, 720, 23311, NULL, NULL, NULL, '2021-11-15 03:45:17', '2021-11-15 03:45:17', '2021-11-15 03:45:17', 'bccd0d31-5b8f-4eb5-845d-d165b30f6008'),
(36, 1, 1, NULL, 'author.jpeg', 'image', 200, 200, 11717, NULL, NULL, NULL, '2021-11-15 03:59:37', '2021-11-15 03:59:37', '2021-11-15 03:59:37', '6642fc84-9291-4e11-a756-08c6e26e2c62'),
(66, 1, 1, 1, 'man-g9e6b97198_640.jpg', 'image', 480, 640, 76546, NULL, NULL, NULL, '2021-11-15 18:50:34', '2021-11-15 18:50:34', '2021-11-15 18:50:34', '005192e0-1835-4e03-9fe9-135b0e8aa62c'),
(67, 1, 1, 1, 'autumn-gce5060330_640.jpg', 'image', 640, 427, 54476, NULL, NULL, NULL, '2021-11-15 18:50:36', '2021-11-15 18:50:36', '2021-11-15 18:50:36', 'c503cdb3-67fa-40eb-8bc8-03c73cc41b3e'),
(68, 1, 1, 1, 'mountain-g5dd207968_640.jpg', 'image', 640, 427, 60058, NULL, NULL, NULL, '2021-11-15 18:50:38', '2021-11-15 18:50:38', '2021-11-15 18:50:38', '9d392a36-c361-4355-8d01-a918e61b3674');

-- --------------------------------------------------------

--
-- Table structure for table `blg_assettransformindex`
--

CREATE TABLE `blg_assettransformindex` (
  `id` int NOT NULL,
  `assetId` int NOT NULL,
  `filename` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `format` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `location` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `volumeId` int DEFAULT NULL,
  `fileExists` tinyint(1) NOT NULL DEFAULT '0',
  `inProgress` tinyint(1) NOT NULL DEFAULT '0',
  `error` tinyint(1) NOT NULL DEFAULT '0',
  `dateIndexed` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `blg_assettransformindex`
--

INSERT INTO `blg_assettransformindex` (`id`, `assetId`, `filename`, `format`, `location`, `volumeId`, `fileExists`, `inProgress`, `error`, `dateIndexed`, `dateCreated`, `dateUpdated`, `uid`) VALUES
(1, 19, 'post-01.jpg', 'jpg', '_postsList', 1, 1, 0, 0, '2021-11-15 19:03:53', '2021-11-15 19:03:53', '2021-11-15 19:03:54', '8bb5021b-d51d-42cb-83d5-994bff7dbbb0'),
(2, 22, 'post-04.jpg', 'jpg', '_postsList', 1, 1, 0, 0, '2021-11-15 19:03:54', '2021-11-15 19:03:54', '2021-11-15 19:03:55', '09a6ba10-4729-465f-a27a-5db25acf4c33'),
(3, 27, 'post-09.jpg', 'jpg', '_postsList', 1, 1, 0, 0, '2021-11-15 19:03:55', '2021-11-15 19:03:55', '2021-11-15 19:03:55', 'b7241fdf-835d-42a0-91a7-b4c4d71e10f3'),
(4, 20, 'post-02.jpg', 'jpg', '_postsList', 1, 1, 0, 0, '2021-11-15 19:03:56', '2021-11-15 19:03:56', '2021-11-15 19:03:56', 'ab898cb1-5983-475e-a780-f6959e4c5f7f'),
(5, 21, 'post-03.jpg', 'jpg', '_postsList', 1, 1, 0, 0, '2021-11-15 19:03:56', '2021-11-15 19:03:56', '2021-11-15 19:03:57', '307a3338-e2a7-4fe7-8498-9c7bc9aa8c09'),
(6, 10, 'post-10.jpg', 'jpg', '_postsList', 1, 1, 0, 0, '2021-11-15 19:03:57', '2021-11-15 19:03:57', '2021-11-15 19:03:58', '2538d4ba-bb6f-48ee-a7fa-cd865956bc4c'),
(7, 68, 'mountain-g5dd207968_640.jpg', 'jpg', '_postsList', 1, 1, 0, 0, '2021-11-15 19:15:49', '2021-11-15 19:15:49', '2021-11-15 19:15:50', '93ac43ca-ae5d-4fc7-8b94-c6b4c39c2977'),
(8, 67, 'autumn-gce5060330_640.jpg', 'jpg', '_postsList', 1, 1, 0, 0, '2021-11-15 19:15:50', '2021-11-15 19:15:50', '2021-11-15 19:15:51', 'cacb901c-a239-4bf5-bbd3-0a76275f2acc'),
(9, 66, 'man-g9e6b97198_640.jpg', 'jpg', '_postsList', 1, 1, 0, 0, '2021-11-15 19:15:51', '2021-11-15 19:15:51', '2021-11-15 19:15:51', '6814a653-8bd2-4a0b-a7bc-1b2c8bd29af3'),
(10, 23, 'post-05.jpg', 'jpg', '_postsList', 1, 1, 0, 0, '2021-11-15 19:15:52', '2021-11-15 19:15:52', '2021-11-15 19:15:52', 'ae4b5ab5-20a3-4093-9366-7fd9db91b580'),
(11, 24, 'post-06.jpg', 'jpg', '_postsList', 1, 1, 0, 0, '2021-11-15 19:15:52', '2021-11-15 19:15:52', '2021-11-15 19:15:53', 'a0dedd7e-64f2-4944-9e0b-1b575b292e52'),
(12, 25, 'post-07.jpg', 'jpg', '_postsList', 1, 1, 0, 0, '2021-11-15 19:15:53', '2021-11-15 19:15:53', '2021-11-15 19:15:53', '9a49eda6-3555-420e-8fa9-1554275ee444');

-- --------------------------------------------------------

--
-- Table structure for table `blg_assettransforms`
--

CREATE TABLE `blg_assettransforms` (
  `id` int NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `handle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mode` enum('stretch','fit','crop') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'crop',
  `position` enum('top-left','top-center','top-right','center-left','center-center','center-right','bottom-left','bottom-center','bottom-right') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'center-center',
  `width` int UNSIGNED DEFAULT NULL,
  `height` int UNSIGNED DEFAULT NULL,
  `format` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `quality` int DEFAULT NULL,
  `interlace` enum('none','line','plane','partition') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'none',
  `dimensionChangeTime` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `blg_assettransforms`
--

INSERT INTO `blg_assettransforms` (`id`, `name`, `handle`, `mode`, `position`, `width`, `height`, `format`, `quality`, `interlace`, `dimensionChangeTime`, `dateCreated`, `dateUpdated`, `uid`) VALUES
(1, 'Posts List', 'postsList', 'crop', 'center-center', 568, 379, 'jpg', 60, 'none', '2021-11-15 18:49:28', '2021-11-15 18:49:28', '2021-11-15 18:49:28', '5cbe3726-c50c-4c54-bb09-3f83d0b51b46');

-- --------------------------------------------------------

--
-- Table structure for table `blg_categories`
--

CREATE TABLE `blg_categories` (
  `id` int NOT NULL,
  `groupId` int NOT NULL,
  `parentId` int DEFAULT NULL,
  `deletedWithGroup` tinyint(1) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `blg_categorygroups`
--

CREATE TABLE `blg_categorygroups` (
  `id` int NOT NULL,
  `structureId` int NOT NULL,
  `fieldLayoutId` int DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `handle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `defaultPlacement` enum('beginning','end') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'end',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `dateDeleted` datetime DEFAULT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `blg_categorygroups_sites`
--

CREATE TABLE `blg_categorygroups_sites` (
  `id` int NOT NULL,
  `groupId` int NOT NULL,
  `siteId` int NOT NULL,
  `hasUrls` tinyint(1) NOT NULL DEFAULT '1',
  `uriFormat` text COLLATE utf8_unicode_ci,
  `template` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `blg_changedattributes`
--

CREATE TABLE `blg_changedattributes` (
  `elementId` int NOT NULL,
  `siteId` int NOT NULL,
  `attribute` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `propagated` tinyint(1) NOT NULL,
  `userId` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `blg_changedattributes`
--

INSERT INTO `blg_changedattributes` (`elementId`, `siteId`, `attribute`, `dateUpdated`, `propagated`, `userId`) VALUES
(1, 1, 'photoId', '2021-11-15 03:59:38', 0, 1),
(2, 1, 'authorId', '2021-11-15 01:07:36', 0, 1),
(2, 1, 'title', '2021-11-15 01:06:42', 0, 1),
(2, 1, 'uri', '2021-11-15 01:07:36', 0, 1),
(14, 1, 'slug', '2021-11-15 03:52:36', 0, 1),
(14, 1, 'title', '2021-11-15 03:52:36', 0, 1),
(14, 1, 'uri', '2021-11-15 03:50:03', 0, 1),
(18, 1, 'enabled', '2021-11-15 03:53:02', 0, 1),
(18, 1, 'slug', '2021-11-15 03:53:02', 0, 1),
(18, 1, 'title', '2021-11-15 03:52:08', 0, 1),
(18, 1, 'uri', '2021-11-15 03:52:08', 0, 1),
(41, 1, 'slug', '2021-11-15 18:34:52', 0, 1),
(41, 1, 'title', '2021-11-15 18:34:02', 0, 1),
(41, 1, 'uri', '2021-11-15 18:34:52', 0, 1),
(45, 1, 'slug', '2021-11-15 18:39:53', 0, 1),
(45, 1, 'title', '2021-11-15 18:39:23', 0, 1),
(45, 1, 'uri', '2021-11-15 18:39:53', 0, 1),
(47, 1, 'slug', '2021-11-15 18:41:55', 0, 1),
(47, 1, 'title', '2021-11-15 18:41:38', 0, 1),
(47, 1, 'uri', '2021-11-15 18:41:55', 0, 1),
(49, 1, 'slug', '2021-11-15 18:43:00', 0, 1),
(49, 1, 'title', '2021-11-15 18:42:42', 0, 1),
(49, 1, 'uri', '2021-11-15 18:43:00', 0, 1),
(51, 1, 'slug', '2021-11-15 18:43:39', 0, 1),
(51, 1, 'title', '2021-11-15 18:43:19', 0, 1),
(51, 1, 'uri', '2021-11-15 18:43:39', 0, 1),
(53, 1, 'slug', '2021-11-15 18:44:48', 0, 1),
(53, 1, 'title', '2021-11-15 18:44:22', 0, 1),
(53, 1, 'uri', '2021-11-15 18:44:48', 0, 1),
(55, 1, 'slug', '2021-11-15 18:45:24', 0, 1),
(55, 1, 'title', '2021-11-15 18:45:00', 0, 1),
(55, 1, 'uri', '2021-11-15 18:45:24', 0, 1),
(57, 1, 'slug', '2021-11-15 18:46:32', 0, 1),
(57, 1, 'title', '2021-11-15 18:46:12', 0, 1),
(57, 1, 'uri', '2021-11-15 18:46:32', 0, 1),
(65, 1, 'slug', '2021-11-15 18:51:18', 0, 1),
(65, 1, 'title', '2021-11-15 18:50:21', 0, 1),
(65, 1, 'uri', '2021-11-15 18:51:18', 0, 1),
(71, 1, 'slug', '2021-11-15 18:52:01', 0, 1),
(71, 1, 'title', '2021-11-15 18:51:35', 0, 1),
(71, 1, 'uri', '2021-11-15 18:52:01', 0, 1),
(74, 1, 'slug', '2021-11-15 18:52:40', 0, 1),
(74, 1, 'title', '2021-11-15 18:52:13', 0, 1),
(74, 1, 'uri', '2021-11-15 18:52:40', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `blg_changedfields`
--

CREATE TABLE `blg_changedfields` (
  `elementId` int NOT NULL,
  `siteId` int NOT NULL,
  `fieldId` int NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `propagated` tinyint(1) NOT NULL,
  `userId` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `blg_changedfields`
--

INSERT INTO `blg_changedfields` (`elementId`, `siteId`, `fieldId`, `dateUpdated`, `propagated`, `userId`) VALUES
(2, 1, 1, '2021-11-15 03:28:50', 0, 1),
(2, 1, 2, '2021-11-15 03:28:50', 0, 1),
(2, 1, 3, '2021-11-15 03:28:50', 0, 1),
(2, 1, 4, '2021-11-15 03:28:50', 0, 1),
(2, 1, 5, '2021-11-15 03:28:50', 0, 1),
(12, 1, 4, '2021-11-15 03:33:53', 0, 1),
(12, 1, 5, '2021-11-15 03:33:53', 0, 1),
(12, 1, 7, '2021-11-15 03:33:53', 0, 1),
(14, 1, 1, '2021-11-15 03:51:40', 0, 1),
(14, 1, 2, '2021-11-15 03:51:40', 0, 1),
(14, 1, 3, '2021-11-15 03:51:40', 0, 1),
(14, 1, 4, '2021-11-15 18:16:34', 0, 1),
(14, 1, 5, '2021-11-15 18:16:34', 0, 1),
(14, 1, 7, '2021-11-15 18:16:34', 0, 1),
(14, 1, 8, '2021-11-15 03:51:40', 0, 1),
(18, 1, 1, '2021-11-15 03:51:37', 0, 1),
(18, 1, 2, '2021-11-15 03:51:37', 0, 1),
(18, 1, 3, '2021-11-15 03:51:37', 0, 1),
(18, 1, 4, '2021-11-15 18:16:52', 0, 1),
(18, 1, 5, '2021-11-15 18:16:52', 0, 1),
(18, 1, 7, '2021-11-15 18:16:52', 0, 1),
(18, 1, 8, '2021-11-15 03:51:37', 0, 1),
(41, 1, 1, '2021-11-15 18:34:20', 0, 1),
(41, 1, 2, '2021-11-15 18:35:38', 0, 1),
(41, 1, 3, '2021-11-15 18:34:38', 0, 1),
(41, 1, 4, '2021-11-15 18:34:45', 0, 1),
(41, 1, 5, '2021-11-15 18:34:41', 0, 1),
(41, 1, 8, '2021-11-15 18:34:02', 0, 1),
(45, 1, 1, '2021-11-15 18:39:32', 0, 1),
(45, 1, 2, '2021-11-15 18:39:39', 0, 1),
(45, 1, 3, '2021-11-15 18:39:47', 0, 1),
(45, 1, 4, '2021-11-15 18:39:23', 0, 1),
(45, 1, 5, '2021-11-15 18:39:23', 0, 1),
(45, 1, 7, '2021-11-15 18:39:42', 0, 1),
(45, 1, 8, '2021-11-15 18:39:23', 0, 1),
(47, 1, 1, '2021-11-15 18:41:34', 0, 1),
(47, 1, 2, '2021-11-15 18:41:44', 0, 1),
(47, 1, 3, '2021-11-15 18:41:48', 0, 1),
(47, 1, 4, '2021-11-15 18:41:34', 0, 1),
(47, 1, 5, '2021-11-15 18:41:34', 0, 1),
(47, 1, 7, '2021-11-15 18:41:51', 0, 1),
(47, 1, 8, '2021-11-15 18:41:38', 0, 1),
(49, 1, 1, '2021-11-15 18:42:37', 0, 1),
(49, 1, 2, '2021-11-15 18:42:49', 0, 1),
(49, 1, 4, '2021-11-15 18:42:37', 0, 1),
(49, 1, 5, '2021-11-15 18:42:37', 0, 1),
(49, 1, 7, '2021-11-15 18:42:55', 0, 1),
(49, 1, 8, '2021-11-15 18:42:42', 0, 1),
(51, 1, 1, '2021-11-15 18:43:12', 0, 1),
(51, 1, 2, '2021-11-15 18:43:29', 0, 1),
(51, 1, 4, '2021-11-15 18:43:12', 0, 1),
(51, 1, 5, '2021-11-15 18:43:12', 0, 1),
(51, 1, 7, '2021-11-15 18:43:35', 0, 1),
(51, 1, 8, '2021-11-15 18:43:19', 0, 1),
(53, 1, 1, '2021-11-15 18:44:18', 0, 1),
(53, 1, 2, '2021-11-15 18:44:28', 0, 1),
(53, 1, 4, '2021-11-15 18:44:42', 0, 1),
(53, 1, 5, '2021-11-15 18:44:41', 0, 1),
(53, 1, 8, '2021-11-15 18:44:22', 0, 1),
(55, 1, 1, '2021-11-15 18:44:56', 0, 1),
(55, 1, 2, '2021-11-15 18:45:06', 0, 1),
(55, 1, 3, '2021-11-15 18:45:13', 0, 1),
(55, 1, 4, '2021-11-15 18:45:17', 0, 1),
(55, 1, 5, '2021-11-15 18:45:17', 0, 1),
(55, 1, 8, '2021-11-15 18:45:00', 0, 1),
(57, 1, 1, '2021-11-15 18:46:08', 0, 1),
(57, 1, 2, '2021-11-15 18:46:21', 0, 1),
(57, 1, 4, '2021-11-15 18:46:29', 0, 1),
(57, 1, 5, '2021-11-15 18:46:27', 0, 1),
(57, 1, 8, '2021-11-15 18:46:12', 0, 1),
(65, 1, 1, '2021-11-15 18:50:15', 0, 1),
(65, 1, 2, '2021-11-15 18:50:47', 0, 1),
(65, 1, 3, '2021-11-15 18:51:09', 0, 1),
(65, 1, 4, '2021-11-15 18:51:12', 0, 1),
(65, 1, 5, '2021-11-15 18:51:10', 0, 1),
(65, 1, 8, '2021-11-15 18:50:21', 0, 1),
(71, 1, 1, '2021-11-15 18:51:31', 0, 1),
(71, 1, 2, '2021-11-15 18:51:42', 0, 1),
(71, 1, 3, '2021-11-15 18:51:55', 0, 1),
(71, 1, 4, '2021-11-15 18:51:55', 0, 1),
(71, 1, 5, '2021-11-15 18:51:55', 0, 1),
(71, 1, 8, '2021-11-15 18:51:35', 0, 1),
(74, 1, 1, '2021-11-15 18:52:10', 0, 1),
(74, 1, 2, '2021-11-15 18:52:20', 0, 1),
(74, 1, 3, '2021-11-15 18:52:33', 0, 1),
(74, 1, 4, '2021-11-15 18:52:38', 0, 1),
(74, 1, 5, '2021-11-15 18:52:33', 0, 1),
(74, 1, 8, '2021-11-15 18:52:13', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `blg_content`
--

CREATE TABLE `blg_content` (
  `id` int NOT NULL,
  `elementId` int NOT NULL,
  `siteId` int NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `field_excerpt_oebprhdp` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `field_views_ccmcxfrr` int DEFAULT NULL,
  `field_likes_mksqzuki` int DEFAULT NULL,
  `field_featured_qjffftaa_old` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `field_articleTitle_gaxmcqla` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `field_featured_qjffftaa` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `blg_content`
--

INSERT INTO `blg_content` (`id`, `elementId`, `siteId`, `title`, `dateCreated`, `dateUpdated`, `uid`, `field_excerpt_oebprhdp`, `field_views_ccmcxfrr`, `field_likes_mksqzuki`, `field_featured_qjffftaa_old`, `field_articleTitle_gaxmcqla`, `field_featured_qjffftaa`) VALUES
(1, 1, 1, NULL, '2021-11-14 23:59:07', '2021-11-15 03:59:37', '47bda497-5943-4547-b837-c4267641cace', NULL, NULL, NULL, NULL, NULL, NULL),
(2, 2, 1, 'Articles', '2021-11-15 01:06:17', '2021-11-15 03:28:50', 'f63ed041-57f2-40da-a98b-d995fe0b9635', 'This is the content', NULL, NULL, NULL, NULL, NULL),
(3, 3, 1, 'Entries', '2021-11-15 01:06:18', '2021-11-15 01:06:18', '985373d8-3f97-481c-8a6a-9e764cd67d71', NULL, NULL, NULL, NULL, NULL, NULL),
(4, 4, 1, 'Entries', '2021-11-15 01:06:18', '2021-11-15 01:06:18', 'de9b56a5-f649-44a3-8c08-578278a62856', NULL, NULL, NULL, NULL, NULL, NULL),
(5, 5, 1, 'Articles', '2021-11-15 01:06:42', '2021-11-15 01:06:42', '1b7c02c6-7bd9-4355-9b52-d5491dbb3245', NULL, NULL, NULL, NULL, NULL, NULL),
(6, 6, 1, 'Articles', '2021-11-15 01:06:43', '2021-11-15 01:06:43', '11c0be42-1df0-46bd-ae85-ed43c7396a9c', NULL, NULL, NULL, NULL, NULL, NULL),
(7, 7, 1, 'Books', '2021-11-15 03:26:43', '2021-11-15 03:26:43', 'a34c8dce-8ac7-4e23-87a5-a8546a46898d', NULL, NULL, NULL, NULL, NULL, NULL),
(10, 10, 1, 'Post 10', '2021-11-15 03:28:32', '2021-11-15 03:28:32', '7f575dce-ffe4-4cd6-b84b-6a7aa1a9dae7', NULL, NULL, NULL, NULL, NULL, NULL),
(11, 11, 1, 'Articles', '2021-11-15 03:28:50', '2021-11-15 03:28:50', '0a1cbe93-943f-4fc5-8e2c-4cdc050f75df', 'This is the content', NULL, NULL, NULL, NULL, NULL),
(12, 12, 1, 'Articles', '2021-11-15 03:33:52', '2021-11-15 03:33:53', '62394560-82af-4b7a-9217-e74f4429bf83', 'This is the content', NULL, NULL, '[\"setAsAFeaturedArticle\"]', NULL, NULL),
(13, 13, 1, 'Articles', '2021-11-15 03:35:45', '2021-11-15 03:35:45', '3db5aca8-0193-4e12-b782-899c103b036a', NULL, NULL, NULL, '[]', NULL, NULL),
(14, 14, 1, 'Rebuild Trust After Sexual Betrayal', '2021-11-15 03:39:50', '2021-11-15 18:16:34', 'c2c549b6-8d76-4700-9900-e26d0bf645b5', 'Gain a clear understanding of how you are being impacted by infidelity and how you can begin to respond in ways that will bring peace and control to your life.', NULL, NULL, '[\"setAsAFeaturedArticle\"]', 'Rebuild Trust After Sexual Betrayal', 1),
(15, 15, 1, 'Articles', '2021-11-15 03:41:07', '2021-11-15 03:41:07', '5a732522-0529-4f86-b36d-faf4ddc03858', 'Gain a clear understanding of how you are being impacted by infidelity and how you can begin to respond in ways that will bring peace and control to your life.', NULL, NULL, '[\"setAsAFeaturedArticle\"]', 'Rebuild Trust After Sexual Betrayal', NULL),
(16, 16, 1, 'Articles', '2021-11-15 03:42:07', '2021-11-15 03:42:07', '5d4894bb-0fbd-4e92-b2b7-0e319cab743e', 'Gain a clear understanding of how you are being impacted by infidelity and how you can begin to respond in ways that will bring peace and control to your life.', NULL, NULL, '[\"setAsAFeaturedArticle\"]', 'Rebuild Trust After Sexual Betrayal', NULL),
(18, 18, 1, 'Rebuild Trust After Sexual Betrayal', '2021-11-15 03:42:24', '2021-11-15 18:16:52', 'b4c74726-4d10-4533-a434-4b6be2b832fb', 'Gain a clear understanding of how you are being impacted by infidelity and how you can begin to respond in ways that will bring peace and control to your life.', NULL, NULL, '[\"setAsAFeaturedArticle\"]', 'Rebuild Trust After Sexual Betrayal', 1),
(19, 19, 1, 'Post 01', '2021-11-15 03:45:07', '2021-11-15 03:45:07', 'b36d8057-e1a8-468d-9095-26cfb9390662', NULL, NULL, NULL, NULL, NULL, NULL),
(20, 20, 1, 'Post 02', '2021-11-15 03:45:08', '2021-11-15 03:45:08', '152ecccd-852c-467f-ab31-be39272e9a67', NULL, NULL, NULL, NULL, NULL, NULL),
(21, 21, 1, 'Post 03', '2021-11-15 03:45:09', '2021-11-15 03:45:09', 'b4966058-6536-4818-85dc-e3dc7b3c7bae', NULL, NULL, NULL, NULL, NULL, NULL),
(22, 22, 1, 'Post 04', '2021-11-15 03:45:11', '2021-11-15 03:45:11', '3c7f8325-79b9-4493-9acd-9d569cabfe9b', NULL, NULL, NULL, NULL, NULL, NULL),
(23, 23, 1, 'Post 05', '2021-11-15 03:45:12', '2021-11-15 03:45:12', '40e37212-07f9-48b8-88a2-21d8404495b1', NULL, NULL, NULL, NULL, NULL, NULL),
(24, 24, 1, 'Post 06', '2021-11-15 03:45:13', '2021-11-15 03:45:13', '58ced2ca-00ea-4c0e-a75f-83e828fb030f', NULL, NULL, NULL, NULL, NULL, NULL),
(25, 25, 1, 'Post 07', '2021-11-15 03:45:14', '2021-11-15 03:45:14', 'd1fec6cc-355b-4b14-8f52-84bf838dfdf6', NULL, NULL, NULL, NULL, NULL, NULL),
(26, 26, 1, 'Post 08', '2021-11-15 03:45:15', '2021-11-15 03:45:15', 'a99ea92c-888e-48a0-ba74-4dc45294003e', NULL, NULL, NULL, NULL, NULL, NULL),
(27, 27, 1, 'Post 09', '2021-11-15 03:45:17', '2021-11-15 03:45:17', 'c3d9402a-db03-4ddd-ba91-b22879680248', NULL, NULL, NULL, NULL, NULL, NULL),
(28, 28, 1, 'Articles', '2021-11-15 03:45:35', '2021-11-15 03:45:35', 'ef1375fb-23e9-4ee8-ad63-ac89491e7c67', 'Gain a clear understanding of how you are being impacted by infidelity and how you can begin to respond in ways that will bring peace and control to your life.', NULL, NULL, '[]', 'Rebuild Trust After Sexual Betrayal', NULL),
(30, 30, 1, 'Rebuild Trust After Sexual Betrayal', '2021-11-15 03:52:08', '2021-11-15 03:52:08', '05ef361f-a9dc-4f62-b477-8eb4b4e4c144', 'Gain a clear understanding of how you are being impacted by infidelity and how you can begin to respond in ways that will bring peace and control to your life.', NULL, NULL, '[]', 'Rebuild Trust After Sexual Betrayal', NULL),
(31, 31, 1, 'Rebuild Trust After Sexual Betrayal', '2021-11-15 03:52:36', '2021-11-15 03:52:36', 'f669abae-7e19-44bc-920c-60bbe87c364a', 'Gain a clear understanding of how you are being impacted by infidelity and how you can begin to respond in ways that will bring peace and control to your life.', NULL, NULL, '[\"setAsAFeaturedArticle\"]', 'Rebuild Trust After Sexual Betrayal', NULL),
(33, 33, 1, 'Rebuild Trust After Sexual Betrayal', '2021-11-15 03:52:42', '2021-11-15 03:52:42', '6c18da2d-0edb-431c-a5c5-f58dbc97e8cb', 'Gain a clear understanding of how you are being impacted by infidelity and how you can begin to respond in ways that will bring peace and control to your life.', NULL, NULL, '[\"setAsAFeaturedArticle\"]', 'Rebuild Trust After Sexual Betrayal', NULL),
(35, 35, 1, 'Rebuild Trust After Sexual Betrayal', '2021-11-15 03:53:02', '2021-11-15 03:53:02', '4feb59f7-253e-4dec-8a55-a9d70dfc93ee', 'Gain a clear understanding of how you are being impacted by infidelity and how you can begin to respond in ways that will bring peace and control to your life.', NULL, NULL, '[\"setAsAFeaturedArticle\"]', 'Rebuild Trust After Sexual Betrayal', NULL),
(36, 36, 1, 'Author', '2021-11-15 03:59:37', '2021-11-15 03:59:37', 'a1d9a36d-1a41-4236-9a39-3a233cfcb254', NULL, NULL, NULL, NULL, NULL, NULL),
(38, 38, 1, 'Rebuild Trust After Sexual Betrayal', '2021-11-15 18:16:35', '2021-11-15 18:16:35', '2cfb694a-e606-4918-bc78-3b9e7c454f84', 'Gain a clear understanding of how you are being impacted by infidelity and how you can begin to respond in ways that will bring peace and control to your life.', NULL, NULL, NULL, 'Rebuild Trust After Sexual Betrayal', 1),
(40, 40, 1, 'Rebuild Trust After Sexual Betrayal', '2021-11-15 18:16:52', '2021-11-15 18:16:52', 'f4f12ac5-cded-4769-a933-3b3f319b867b', 'Gain a clear understanding of how you are being impacted by infidelity and how you can begin to respond in ways that will bring peace and control to your life.', NULL, NULL, NULL, 'Rebuild Trust After Sexual Betrayal', 1),
(41, 41, 1, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit', '2021-11-15 18:24:54', '2021-11-15 18:35:38', '6150e5c0-5e73-4d28-9206-a44bd2961886', 'Pellentesque a accumsan sapien, non laoreet quam. Aliquam rhoncus lacinia dui, laoreet laoreet est rhoncus id. Sed aliquet ligula vel lobortis feugiat. Mauris imperdiet condimentum elit id scelerisque. Praesent elit risus, pretium sit amet purus ac, mattis posuere tortor. Nam euismod ut nibh ac facilisis. Aliquam sollicitudin viverra tellus. Aenean non ante ac ante fringilla porttitor. Integer non odio ex. Cras at metus nibh. Fusce vel nibh vitae nunc accumsan vulputate ut non nunc.', 100, 50, NULL, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit', 0),
(42, 42, 1, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit', '2021-11-15 18:34:52', '2021-11-15 18:34:52', '01b26307-9119-4296-9a45-d1a0b036ea98', 'Pellentesque a accumsan sapien, non laoreet quam. Aliquam rhoncus lacinia dui, laoreet laoreet est rhoncus id. Sed aliquet ligula vel lobortis feugiat. Mauris imperdiet condimentum elit id scelerisque. Praesent elit risus, pretium sit amet purus ac, mattis posuere tortor. Nam euismod ut nibh ac facilisis. Aliquam sollicitudin viverra tellus. Aenean non ante ac ante fringilla porttitor. Integer non odio ex. Cras at metus nibh. Fusce vel nibh vitae nunc accumsan vulputate ut non nunc.', 100, 50, NULL, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit', 0),
(44, 44, 1, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit', '2021-11-15 18:35:38', '2021-11-15 18:35:38', 'fa28621b-00cb-4886-88b0-2ef02b631b64', 'Pellentesque a accumsan sapien, non laoreet quam. Aliquam rhoncus lacinia dui, laoreet laoreet est rhoncus id. Sed aliquet ligula vel lobortis feugiat. Mauris imperdiet condimentum elit id scelerisque. Praesent elit risus, pretium sit amet purus ac, mattis posuere tortor. Nam euismod ut nibh ac facilisis. Aliquam sollicitudin viverra tellus. Aenean non ante ac ante fringilla porttitor. Integer non odio ex. Cras at metus nibh. Fusce vel nibh vitae nunc accumsan vulputate ut non nunc.', 100, 50, NULL, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit', 0),
(45, 45, 1, 'Quisque ac ligula a odio condimentum sagittis', '2021-11-15 18:39:00', '2021-11-15 18:39:53', '1bb1deee-b1bf-4b28-96e3-f32d90331b44', 'Vivamus non sollicitudin velit, faucibus convallis est. Integer efficitur quis arcu ac semper. Nullam varius mattis ligula at pharetra. Vestibulum velit enim, lacinia eget scelerisque eu, condimentum quis elit. Fusce ut ex a nisl eleifend scelerisque nec sit amet elit. In vulputate pulvinar lacus nec volutpat.', NULL, NULL, NULL, 'Quisque ac ligula a odio condimentum sagittis', 1),
(46, 46, 1, 'Quisque ac ligula a odio condimentum sagittis', '2021-11-15 18:39:53', '2021-11-15 18:39:53', '03c5c58e-4c4d-4a53-b38e-521ce3c395b9', 'Vivamus non sollicitudin velit, faucibus convallis est. Integer efficitur quis arcu ac semper. Nullam varius mattis ligula at pharetra. Vestibulum velit enim, lacinia eget scelerisque eu, condimentum quis elit. Fusce ut ex a nisl eleifend scelerisque nec sit amet elit. In vulputate pulvinar lacus nec volutpat.', NULL, NULL, NULL, 'Quisque ac ligula a odio condimentum sagittis', 1),
(47, 47, 1, 'Aliquam erat volutpat', '2021-11-15 18:41:22', '2021-11-15 18:41:55', '36facac8-10e8-4d41-9ba6-e359d1530457', 'Aliquam erat volutpat. Vivamus diam justo, rhoncus non est et, tempor rhoncus mi. Vestibulum ut risus eros. Vivamus tellus elit, pellentesque a auctor id, interdum vitae ex. Phasellus sollicitudin vitae ex eget hendrerit. Aenean et neque auctor, dapibus elit et, porttitor lacus. Cras vitae lorem ante. In convallis consectetur tincidunt. Nulla vitae justo id ante tempus sollicitudin a vitae est.', NULL, NULL, NULL, 'Aliquam erat volutpat', 1),
(48, 48, 1, 'Aliquam erat volutpat', '2021-11-15 18:41:55', '2021-11-15 18:41:55', '447c34a2-e191-4614-ba40-9dec61b3438f', 'Aliquam erat volutpat. Vivamus diam justo, rhoncus non est et, tempor rhoncus mi. Vestibulum ut risus eros. Vivamus tellus elit, pellentesque a auctor id, interdum vitae ex. Phasellus sollicitudin vitae ex eget hendrerit. Aenean et neque auctor, dapibus elit et, porttitor lacus. Cras vitae lorem ante. In convallis consectetur tincidunt. Nulla vitae justo id ante tempus sollicitudin a vitae est.', NULL, NULL, NULL, 'Aliquam erat volutpat', 1),
(49, 49, 1, 'Vivamus urna lacus, posuere quis urna eget, iaculis suscipit ex', '2021-11-15 18:42:21', '2021-11-15 18:43:00', '63d5e749-0cf1-4246-b1fb-2cc989cc54d2', 'Vivamus urna lacus, posuere quis urna eget, iaculis suscipit ex. Integer sit amet luctus justo. Duis sit amet sem nec augue commodo aliquet et vitae magna. Cras consectetur, dui eget egestas varius, justo ligula placerat ligula, vitae lobortis dui ante ut metus. Cras id elementum ligula. Nulla tincidunt, arcu at semper tincidunt, est turpis hendrerit tellus, in euismod quam lacus ut risus. Curabitur accumsan velit ac orci venenatis, ut lobortis metus sollicitudin.', NULL, NULL, NULL, 'Vivamus urna lacus, posuere quis urna eget, iaculis suscipit ex', 1),
(50, 50, 1, 'Vivamus urna lacus, posuere quis urna eget, iaculis suscipit ex', '2021-11-15 18:43:00', '2021-11-15 18:43:00', '7888331e-3dee-4183-a554-648211a0259a', 'Vivamus urna lacus, posuere quis urna eget, iaculis suscipit ex. Integer sit amet luctus justo. Duis sit amet sem nec augue commodo aliquet et vitae magna. Cras consectetur, dui eget egestas varius, justo ligula placerat ligula, vitae lobortis dui ante ut metus. Cras id elementum ligula. Nulla tincidunt, arcu at semper tincidunt, est turpis hendrerit tellus, in euismod quam lacus ut risus. Curabitur accumsan velit ac orci venenatis, ut lobortis metus sollicitudin.', NULL, NULL, NULL, 'Vivamus urna lacus, posuere quis urna eget, iaculis suscipit ex', 1),
(51, 51, 1, 'Suspendisse ultrices maximus massa, ac aliquet quam pellentesque ut', '2021-11-15 18:43:01', '2021-11-15 18:43:39', '43d0898d-cb39-4208-ad20-4f8a31e96c9e', 'Suspendisse ultrices maximus massa, ac aliquet quam pellentesque ut. Quisque porttitor pellentesque ultrices. Vivamus arcu purus, dictum ac ornare at, consectetur quis leo. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Sed semper massa in semper vulputate. Nulla tempor quam nibh, a rhoncus nunc molestie eu. Duis vitae elit at risus elementum lobortis. Donec maximus sem vitae tincidunt pharetra. Duis gravida sit amet ipsum vitae auctor. Nam id sagittis dui, feugiat imperdiet risus. Donec rutrum lectus quis nunc bibendum, vel viverra turpis elementum. Ut commodo massa et tellus vehicula euismod.', NULL, NULL, NULL, 'Suspendisse ultrices maximus massa, ac aliquet quam pellentesque ut', 1),
(52, 52, 1, 'Suspendisse ultrices maximus massa, ac aliquet quam pellentesque ut', '2021-11-15 18:43:39', '2021-11-15 18:43:39', '16b65576-7d92-471f-bc83-aa217bc9efdb', 'Suspendisse ultrices maximus massa, ac aliquet quam pellentesque ut. Quisque porttitor pellentesque ultrices. Vivamus arcu purus, dictum ac ornare at, consectetur quis leo. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Sed semper massa in semper vulputate. Nulla tempor quam nibh, a rhoncus nunc molestie eu. Duis vitae elit at risus elementum lobortis. Donec maximus sem vitae tincidunt pharetra. Duis gravida sit amet ipsum vitae auctor. Nam id sagittis dui, feugiat imperdiet risus. Donec rutrum lectus quis nunc bibendum, vel viverra turpis elementum. Ut commodo massa et tellus vehicula euismod.', NULL, NULL, NULL, 'Suspendisse ultrices maximus massa, ac aliquet quam pellentesque ut', 1),
(53, 53, 1, 'Duis sit amet facilisis sapien', '2021-11-15 18:44:09', '2021-11-15 18:44:47', '9a7c9168-457c-4a6d-a39e-8e9b66bc1d47', 'Duis sit amet facilisis sapien. Sed sed consectetur turpis. Proin arcu est, condimentum nec viverra non, varius quis massa. Nam commodo nunc sem, vitae tincidunt orci bibendum et. Nulla sit amet ex quis eros pretium mollis non quis augue. Quisque vitae nulla felis. Vivamus mollis non metus in fringilla. Donec bibendum semper nisl nec venenatis. Cras sodales nunc a ante vehicula, sit amet tincidunt mi commodo.', 15, 10, NULL, 'Duis sit amet facilisis sapien', 0),
(54, 54, 1, 'Duis sit amet facilisis sapien', '2021-11-15 18:44:47', '2021-11-15 18:44:47', 'f94548ca-6aeb-4689-8ccd-6f87e541e559', 'Duis sit amet facilisis sapien. Sed sed consectetur turpis. Proin arcu est, condimentum nec viverra non, varius quis massa. Nam commodo nunc sem, vitae tincidunt orci bibendum et. Nulla sit amet ex quis eros pretium mollis non quis augue. Quisque vitae nulla felis. Vivamus mollis non metus in fringilla. Donec bibendum semper nisl nec venenatis. Cras sodales nunc a ante vehicula, sit amet tincidunt mi commodo.', 15, 10, NULL, 'Duis sit amet facilisis sapien', 0),
(55, 55, 1, 'Aenean faucibus mollis dui malesuada commodo', '2021-11-15 18:44:49', '2021-11-15 18:45:24', 'f5134b54-d7b8-419d-b4b9-31d1fe546925', 'Aenean faucibus mollis dui malesuada commodo. Nullam nec enim blandit, viverra turpis ac, eleifend dolor. Vestibulum ac diam rhoncus, congue lectus vitae, faucibus nunc. In arcu dolor, fringilla non consectetur et, accumsan eu est. Nullam elementum velit ut magna interdum, nec mollis dolor tristique. Nunc eu dictum elit. Nulla vel velit congue, venenatis arcu at, molestie mauris.', 5, 1, NULL, 'Aenean faucibus mollis dui malesuada commodo', 0),
(56, 56, 1, 'Aenean faucibus mollis dui malesuada commodo', '2021-11-15 18:45:24', '2021-11-15 18:45:24', '6ed495bb-0c44-4a14-ad71-47f43df82aff', 'Aenean faucibus mollis dui malesuada commodo. Nullam nec enim blandit, viverra turpis ac, eleifend dolor. Vestibulum ac diam rhoncus, congue lectus vitae, faucibus nunc. In arcu dolor, fringilla non consectetur et, accumsan eu est. Nullam elementum velit ut magna interdum, nec mollis dolor tristique. Nunc eu dictum elit. Nulla vel velit congue, venenatis arcu at, molestie mauris.', 5, 1, NULL, 'Aenean faucibus mollis dui malesuada commodo', 0),
(57, 57, 1, 'In viverra est et pretium finibus', '2021-11-15 18:45:58', '2021-11-15 18:46:31', 'aaab3c04-5c1d-405a-ab54-b92e89736845', 'In viverra est et pretium finibus. Praesent quis dictum ante. Nullam molestie, orci id rutrum vestibulum, diam mauris venenatis ipsum, sit amet feugiat urna lectus eu magna. Praesent eleifend lorem nec tempus rhoncus. Pellentesque euismod auctor mi, sed feugiat felis consequat a. Vestibulum at ullamcorper tellus, et vestibulum sem. Nullam eget condimentum ex, non fermentum arcu. Maecenas at tincidunt diam. Donec pharetra lorem non orci egestas, eu auctor massa consectetur. Quisque vitae ullamcorper leo. In eleifend orci in nisl semper, id blandit nibh dignissim. Integer sodales posuere lectus ut sagittis. Etiam rhoncus nisl at ipsum convallis, eget interdum diam dapibus.', 12, 8, NULL, 'In viverra est et pretium finibus', 0),
(58, 58, 1, 'In viverra est et pretium finibus', '2021-11-15 18:46:31', '2021-11-15 18:46:31', '510685d4-9911-4e90-8052-f41298320375', 'In viverra est et pretium finibus. Praesent quis dictum ante. Nullam molestie, orci id rutrum vestibulum, diam mauris venenatis ipsum, sit amet feugiat urna lectus eu magna. Praesent eleifend lorem nec tempus rhoncus. Pellentesque euismod auctor mi, sed feugiat felis consequat a. Vestibulum at ullamcorper tellus, et vestibulum sem. Nullam eget condimentum ex, non fermentum arcu. Maecenas at tincidunt diam. Donec pharetra lorem non orci egestas, eu auctor massa consectetur. Quisque vitae ullamcorper leo. In eleifend orci in nisl semper, id blandit nibh dignissim. Integer sodales posuere lectus ut sagittis. Etiam rhoncus nisl at ipsum convallis, eget interdum diam dapibus.', 12, 8, NULL, 'In viverra est et pretium finibus', 0),
(59, 59, 1, 'Duis sit amet facilisis sapien', '2021-11-15 18:46:44', '2021-11-15 18:46:44', '4b763d05-3f7a-4e62-ae65-96cb43fb26d2', 'Duis sit amet facilisis sapien. Sed sed consectetur turpis. Proin arcu est, condimentum nec viverra non, varius quis massa. Nam commodo nunc sem, vitae tincidunt orci bibendum et. Nulla sit amet ex quis eros pretium mollis non quis augue. Quisque vitae nulla felis. Vivamus mollis non metus in fringilla. Donec bibendum semper nisl nec venenatis. Cras sodales nunc a ante vehicula, sit amet tincidunt mi commodo.', 15, 10, NULL, 'Duis sit amet facilisis sapien', 0),
(60, 60, 1, 'Duis sit amet facilisis sapien', '2021-11-15 18:46:44', '2021-11-15 18:46:44', 'd09bcaa8-8386-4ca0-86c3-1c488d3d5ece', 'Duis sit amet facilisis sapien. Sed sed consectetur turpis. Proin arcu est, condimentum nec viverra non, varius quis massa. Nam commodo nunc sem, vitae tincidunt orci bibendum et. Nulla sit amet ex quis eros pretium mollis non quis augue. Quisque vitae nulla felis. Vivamus mollis non metus in fringilla. Donec bibendum semper nisl nec venenatis. Cras sodales nunc a ante vehicula, sit amet tincidunt mi commodo.', 15, 10, NULL, 'Duis sit amet facilisis sapien', 0),
(61, 61, 1, 'Aenean faucibus mollis dui malesuada commodo', '2021-11-15 18:46:44', '2021-11-15 18:46:44', '558f3ede-778c-4719-bc34-ca3d88e55809', 'Aenean faucibus mollis dui malesuada commodo. Nullam nec enim blandit, viverra turpis ac, eleifend dolor. Vestibulum ac diam rhoncus, congue lectus vitae, faucibus nunc. In arcu dolor, fringilla non consectetur et, accumsan eu est. Nullam elementum velit ut magna interdum, nec mollis dolor tristique. Nunc eu dictum elit. Nulla vel velit congue, venenatis arcu at, molestie mauris.', 5, 1, NULL, 'Aenean faucibus mollis dui malesuada commodo', 0),
(62, 62, 1, 'Aenean faucibus mollis dui malesuada commodo', '2021-11-15 18:46:44', '2021-11-15 18:46:44', '2b83e684-1a6b-4eb5-b383-b5a46135670d', 'Aenean faucibus mollis dui malesuada commodo. Nullam nec enim blandit, viverra turpis ac, eleifend dolor. Vestibulum ac diam rhoncus, congue lectus vitae, faucibus nunc. In arcu dolor, fringilla non consectetur et, accumsan eu est. Nullam elementum velit ut magna interdum, nec mollis dolor tristique. Nunc eu dictum elit. Nulla vel velit congue, venenatis arcu at, molestie mauris.', 5, 1, NULL, 'Aenean faucibus mollis dui malesuada commodo', 0),
(63, 63, 1, 'In viverra est et pretium finibus', '2021-11-15 18:46:45', '2021-11-15 18:46:45', 'dbdf4a7f-1000-4f1b-bf8b-73d6df8fe244', 'In viverra est et pretium finibus. Praesent quis dictum ante. Nullam molestie, orci id rutrum vestibulum, diam mauris venenatis ipsum, sit amet feugiat urna lectus eu magna. Praesent eleifend lorem nec tempus rhoncus. Pellentesque euismod auctor mi, sed feugiat felis consequat a. Vestibulum at ullamcorper tellus, et vestibulum sem. Nullam eget condimentum ex, non fermentum arcu. Maecenas at tincidunt diam. Donec pharetra lorem non orci egestas, eu auctor massa consectetur. Quisque vitae ullamcorper leo. In eleifend orci in nisl semper, id blandit nibh dignissim. Integer sodales posuere lectus ut sagittis. Etiam rhoncus nisl at ipsum convallis, eget interdum diam dapibus.', 12, 8, NULL, 'In viverra est et pretium finibus', 0),
(64, 64, 1, 'In viverra est et pretium finibus', '2021-11-15 18:46:45', '2021-11-15 18:46:45', 'adaa85cc-3816-4d7d-a309-149b8623ee30', 'In viverra est et pretium finibus. Praesent quis dictum ante. Nullam molestie, orci id rutrum vestibulum, diam mauris venenatis ipsum, sit amet feugiat urna lectus eu magna. Praesent eleifend lorem nec tempus rhoncus. Pellentesque euismod auctor mi, sed feugiat felis consequat a. Vestibulum at ullamcorper tellus, et vestibulum sem. Nullam eget condimentum ex, non fermentum arcu. Maecenas at tincidunt diam. Donec pharetra lorem non orci egestas, eu auctor massa consectetur. Quisque vitae ullamcorper leo. In eleifend orci in nisl semper, id blandit nibh dignissim. Integer sodales posuere lectus ut sagittis. Etiam rhoncus nisl at ipsum convallis, eget interdum diam dapibus.', 12, 8, NULL, 'In viverra est et pretium finibus', 0),
(65, 65, 1, 'Mauris vel odio dapibus metus ornare dignissim', '2021-11-15 18:50:07', '2021-11-15 18:51:18', '01d20a0b-13c7-4ccf-941e-9cf44a8a93ba', 'Mauris vel odio dapibus metus ornare dignissim. Maecenas dui lectus, congue eu mollis vel, venenatis sed velit. Fusce sagittis quis urna sed varius. Integer non ipsum et nisi tincidunt vulputate sed vitae ipsum. Proin tempor ultrices dignissim. Phasellus et velit sed erat semper porttitor sit amet blandit lorem. Etiam tempus odio non faucibus ornare. Vestibulum sed sem eu arcu sodales mattis a sed ex. Etiam tincidunt magna et purus placerat condimentum. Maecenas faucibus massa sed metus ultricies, in posuere orci semper. Nam finibus ante nec lacus molestie hendrerit. Praesent facilisis lacinia sapien vel tristique. Aliquam in neque orci.', 5, 1, NULL, 'Mauris vel odio dapibus metus ornare dignissim', 0),
(66, 66, 1, 'Man g9e6b97198 640', '2021-11-15 18:50:34', '2021-11-15 18:50:34', 'b784169b-897f-4c5e-bb55-7cbc565dcc50', NULL, NULL, NULL, NULL, NULL, NULL),
(67, 67, 1, 'Autumn gce5060330 640', '2021-11-15 18:50:36', '2021-11-15 18:50:36', '345b523f-6d91-4dcd-9bb2-25bfd8ea02dc', NULL, NULL, NULL, NULL, NULL, NULL),
(68, 68, 1, 'Mountain g5dd207968 640', '2021-11-15 18:50:37', '2021-11-15 18:50:37', '62d51e52-8cf8-4252-a5ca-d08cca1041ba', NULL, NULL, NULL, NULL, NULL, NULL),
(69, 69, 1, 'Nature', '2021-11-15 18:51:06', '2021-11-15 18:51:06', 'd879affe-1ebf-4c8c-abe3-4e0ddbb2fcb6', NULL, NULL, NULL, NULL, NULL, NULL),
(70, 70, 1, 'Mauris vel odio dapibus metus ornare dignissim', '2021-11-15 18:51:18', '2021-11-15 18:51:18', '843a12f8-2b00-44e0-8dad-21e05d80f00b', 'Mauris vel odio dapibus metus ornare dignissim. Maecenas dui lectus, congue eu mollis vel, venenatis sed velit. Fusce sagittis quis urna sed varius. Integer non ipsum et nisi tincidunt vulputate sed vitae ipsum. Proin tempor ultrices dignissim. Phasellus et velit sed erat semper porttitor sit amet blandit lorem. Etiam tempus odio non faucibus ornare. Vestibulum sed sem eu arcu sodales mattis a sed ex. Etiam tincidunt magna et purus placerat condimentum. Maecenas faucibus massa sed metus ultricies, in posuere orci semper. Nam finibus ante nec lacus molestie hendrerit. Praesent facilisis lacinia sapien vel tristique. Aliquam in neque orci.', 5, 1, NULL, 'Mauris vel odio dapibus metus ornare dignissim', 0),
(71, 71, 1, 'Aenean vel mauris odio', '2021-11-15 18:51:20', '2021-11-15 18:52:00', '5a66a675-c86c-4ef7-9586-fe5a65f917a2', 'Aenean vel mauris odio. In at fermentum est. Sed id metus eget quam ullamcorper sodales. Pellentesque aliquet blandit lorem, ac sagittis enim vestibulum eu. Praesent ut aliquam nulla, sit amet suscipit libero. Ut pulvinar erat eu augue sollicitudin dignissim. Nam eget ante vehicula augue imperdiet interdum nec tincidunt dolor. Suspendisse potenti. Nullam convallis, leo eget vulputate mollis, magna augue vehicula augue, vitae interdum arcu sapien nec arcu. Aenean in pretium felis. Quisque suscipit nisi quam, quis mattis urna volutpat in. Fusce a erat vitae metus aliquet facilisis.', 18, 5, NULL, 'Aenean vel mauris odio', 0),
(72, 72, 1, 'Season', '2021-11-15 18:51:51', '2021-11-15 18:51:51', '7eb26fb5-ee4d-4902-aa2b-16b6468cde99', NULL, NULL, NULL, NULL, NULL, NULL),
(73, 73, 1, 'Aenean vel mauris odio', '2021-11-15 18:52:00', '2021-11-15 18:52:00', '56444242-2087-4a64-a569-ba3df0df0d4f', 'Aenean vel mauris odio. In at fermentum est. Sed id metus eget quam ullamcorper sodales. Pellentesque aliquet blandit lorem, ac sagittis enim vestibulum eu. Praesent ut aliquam nulla, sit amet suscipit libero. Ut pulvinar erat eu augue sollicitudin dignissim. Nam eget ante vehicula augue imperdiet interdum nec tincidunt dolor. Suspendisse potenti. Nullam convallis, leo eget vulputate mollis, magna augue vehicula augue, vitae interdum arcu sapien nec arcu. Aenean in pretium felis. Quisque suscipit nisi quam, quis mattis urna volutpat in. Fusce a erat vitae metus aliquet facilisis.', 18, 5, NULL, 'Aenean vel mauris odio', 0),
(74, 74, 1, 'Phasellus ullamcorper pretium volutpat', '2021-11-15 18:52:02', '2021-11-15 18:52:40', '62cff517-1270-4e11-b4ca-0a897ae325e0', 'Phasellus ullamcorper pretium volutpat. Fusce cursus massa vel leo egestas commodo vulputate et erat. Quisque lectus dui, efficitur volutpat lorem a, sagittis sagittis urna. Sed gravida fringilla ligula, dictum tempor magna interdum non. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus suscipit nisi ut tristique posuere. Nam gravida nisl neque. Nunc gravida aliquam tortor, sit amet facilisis nunc imperdiet non. Sed quis mi laoreet magna vestibulum ultrices. Nullam accumsan, ex vitae aliquet interdum, arcu purus ultrices diam, sit amet egestas libero enim tincidunt massa. Phasellus vitae mauris sit amet felis porta placerat. Vestibulum sit amet ex a augue elementum aliquet. Vivamus posuere, velit vel euismod dapibus, massa dolor tincidunt mauris, nec blandit lacus quam quis ex. Nullam scelerisque, mi et accumsan tincidunt, purus sem sodales mi, eu bibendum nunc risus in magna. Cras posuere tempor est eu bibendum.', 6, 2, NULL, 'Phasellus ullamcorper pretium volutpat', 0),
(75, 75, 1, 'Mountain', '2021-11-15 18:52:27', '2021-11-15 18:52:27', '8169d2c2-0762-4d29-a6b5-5f67703687a9', NULL, NULL, NULL, NULL, NULL, NULL),
(76, 76, 1, 'Phasellus ullamcorper pretium volutpat', '2021-11-15 18:52:40', '2021-11-15 18:52:40', '2ef63923-2151-4da8-9f63-a234074289df', 'Phasellus ullamcorper pretium volutpat. Fusce cursus massa vel leo egestas commodo vulputate et erat. Quisque lectus dui, efficitur volutpat lorem a, sagittis sagittis urna. Sed gravida fringilla ligula, dictum tempor magna interdum non. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus suscipit nisi ut tristique posuere. Nam gravida nisl neque. Nunc gravida aliquam tortor, sit amet facilisis nunc imperdiet non. Sed quis mi laoreet magna vestibulum ultrices. Nullam accumsan, ex vitae aliquet interdum, arcu purus ultrices diam, sit amet egestas libero enim tincidunt massa. Phasellus vitae mauris sit amet felis porta placerat. Vestibulum sit amet ex a augue elementum aliquet. Vivamus posuere, velit vel euismod dapibus, massa dolor tincidunt mauris, nec blandit lacus quam quis ex. Nullam scelerisque, mi et accumsan tincidunt, purus sem sodales mi, eu bibendum nunc risus in magna. Cras posuere tempor est eu bibendum.', 6, 2, NULL, 'Phasellus ullamcorper pretium volutpat', 0),
(77, 77, 1, 'Mauris vel odio dapibus metus ornare dignissim', '2021-11-15 18:53:38', '2021-11-15 18:53:38', '69ac7484-2369-4165-b8ea-9647e1663c4c', 'Mauris vel odio dapibus metus ornare dignissim. Maecenas dui lectus, congue eu mollis vel, venenatis sed velit. Fusce sagittis quis urna sed varius. Integer non ipsum et nisi tincidunt vulputate sed vitae ipsum. Proin tempor ultrices dignissim. Phasellus et velit sed erat semper porttitor sit amet blandit lorem. Etiam tempus odio non faucibus ornare. Vestibulum sed sem eu arcu sodales mattis a sed ex. Etiam tincidunt magna et purus placerat condimentum. Maecenas faucibus massa sed metus ultricies, in posuere orci semper. Nam finibus ante nec lacus molestie hendrerit. Praesent facilisis lacinia sapien vel tristique. Aliquam in neque orci.', 5, 1, NULL, 'Mauris vel odio dapibus metus ornare dignissim', 0),
(78, 78, 1, 'Mauris vel odio dapibus metus ornare dignissim', '2021-11-15 18:53:38', '2021-11-15 18:53:38', 'f47e401a-4465-415d-bbf2-0ce22b82bd51', 'Mauris vel odio dapibus metus ornare dignissim. Maecenas dui lectus, congue eu mollis vel, venenatis sed velit. Fusce sagittis quis urna sed varius. Integer non ipsum et nisi tincidunt vulputate sed vitae ipsum. Proin tempor ultrices dignissim. Phasellus et velit sed erat semper porttitor sit amet blandit lorem. Etiam tempus odio non faucibus ornare. Vestibulum sed sem eu arcu sodales mattis a sed ex. Etiam tincidunt magna et purus placerat condimentum. Maecenas faucibus massa sed metus ultricies, in posuere orci semper. Nam finibus ante nec lacus molestie hendrerit. Praesent facilisis lacinia sapien vel tristique. Aliquam in neque orci.', 5, 1, NULL, 'Mauris vel odio dapibus metus ornare dignissim', 0),
(79, 79, 1, 'Aenean vel mauris odio', '2021-11-15 18:53:39', '2021-11-15 18:53:39', 'de4d31f2-9479-44ca-9a07-ef3363236157', 'Aenean vel mauris odio. In at fermentum est. Sed id metus eget quam ullamcorper sodales. Pellentesque aliquet blandit lorem, ac sagittis enim vestibulum eu. Praesent ut aliquam nulla, sit amet suscipit libero. Ut pulvinar erat eu augue sollicitudin dignissim. Nam eget ante vehicula augue imperdiet interdum nec tincidunt dolor. Suspendisse potenti. Nullam convallis, leo eget vulputate mollis, magna augue vehicula augue, vitae interdum arcu sapien nec arcu. Aenean in pretium felis. Quisque suscipit nisi quam, quis mattis urna volutpat in. Fusce a erat vitae metus aliquet facilisis.', 18, 5, NULL, 'Aenean vel mauris odio', 0),
(80, 80, 1, 'Aenean vel mauris odio', '2021-11-15 18:53:39', '2021-11-15 18:53:39', 'a1f45eb4-2857-448e-ad9e-16be6de1fd02', 'Aenean vel mauris odio. In at fermentum est. Sed id metus eget quam ullamcorper sodales. Pellentesque aliquet blandit lorem, ac sagittis enim vestibulum eu. Praesent ut aliquam nulla, sit amet suscipit libero. Ut pulvinar erat eu augue sollicitudin dignissim. Nam eget ante vehicula augue imperdiet interdum nec tincidunt dolor. Suspendisse potenti. Nullam convallis, leo eget vulputate mollis, magna augue vehicula augue, vitae interdum arcu sapien nec arcu. Aenean in pretium felis. Quisque suscipit nisi quam, quis mattis urna volutpat in. Fusce a erat vitae metus aliquet facilisis.', 18, 5, NULL, 'Aenean vel mauris odio', 0),
(81, 81, 1, 'Phasellus ullamcorper pretium volutpat', '2021-11-15 18:53:39', '2021-11-15 18:53:39', '6641b0e3-8e93-43b6-878d-9b278f13d83a', 'Phasellus ullamcorper pretium volutpat. Fusce cursus massa vel leo egestas commodo vulputate et erat. Quisque lectus dui, efficitur volutpat lorem a, sagittis sagittis urna. Sed gravida fringilla ligula, dictum tempor magna interdum non. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus suscipit nisi ut tristique posuere. Nam gravida nisl neque. Nunc gravida aliquam tortor, sit amet facilisis nunc imperdiet non. Sed quis mi laoreet magna vestibulum ultrices. Nullam accumsan, ex vitae aliquet interdum, arcu purus ultrices diam, sit amet egestas libero enim tincidunt massa. Phasellus vitae mauris sit amet felis porta placerat. Vestibulum sit amet ex a augue elementum aliquet. Vivamus posuere, velit vel euismod dapibus, massa dolor tincidunt mauris, nec blandit lacus quam quis ex. Nullam scelerisque, mi et accumsan tincidunt, purus sem sodales mi, eu bibendum nunc risus in magna. Cras posuere tempor est eu bibendum.', 6, 2, NULL, 'Phasellus ullamcorper pretium volutpat', 0),
(82, 82, 1, 'Phasellus ullamcorper pretium volutpat', '2021-11-15 18:53:39', '2021-11-15 18:53:39', '3f434d50-ac9f-44aa-8d81-4d8ef3898c01', 'Phasellus ullamcorper pretium volutpat. Fusce cursus massa vel leo egestas commodo vulputate et erat. Quisque lectus dui, efficitur volutpat lorem a, sagittis sagittis urna. Sed gravida fringilla ligula, dictum tempor magna interdum non. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus suscipit nisi ut tristique posuere. Nam gravida nisl neque. Nunc gravida aliquam tortor, sit amet facilisis nunc imperdiet non. Sed quis mi laoreet magna vestibulum ultrices. Nullam accumsan, ex vitae aliquet interdum, arcu purus ultrices diam, sit amet egestas libero enim tincidunt massa. Phasellus vitae mauris sit amet felis porta placerat. Vestibulum sit amet ex a augue elementum aliquet. Vivamus posuere, velit vel euismod dapibus, massa dolor tincidunt mauris, nec blandit lacus quam quis ex. Nullam scelerisque, mi et accumsan tincidunt, purus sem sodales mi, eu bibendum nunc risus in magna. Cras posuere tempor est eu bibendum.', 6, 2, NULL, 'Phasellus ullamcorper pretium volutpat', 0);

-- --------------------------------------------------------

--
-- Table structure for table `blg_craftidtokens`
--

CREATE TABLE `blg_craftidtokens` (
  `id` int NOT NULL,
  `userId` int NOT NULL,
  `accessToken` text COLLATE utf8_unicode_ci NOT NULL,
  `expiryDate` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `blg_deprecationerrors`
--

CREATE TABLE `blg_deprecationerrors` (
  `id` int NOT NULL,
  `key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fingerprint` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lastOccurrence` datetime NOT NULL,
  `file` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `line` smallint UNSIGNED DEFAULT NULL,
  `message` text COLLATE utf8_unicode_ci,
  `traces` text COLLATE utf8_unicode_ci,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `blg_deprecationerrors`
--

INSERT INTO `blg_deprecationerrors` (`id`, `key`, `fingerprint`, `lastOccurrence`, `file`, `line`, `message`, `traces`, `dateCreated`, `dateUpdated`, `uid`) VALUES
(1, 'enableGraphQlCaching', '/var/www/html/nicasource/back/config/general.php', '2021-11-15 13:39:39', '/var/www/html/nicasource/back/config/general.php', NULL, 'The `enableGraphQlCaching` config setting has been renamed to `enableGraphqlCaching`.', '[{\"objectClass\":\"craft\\\\services\\\\Deprecator\",\"file\":\"/var/www/html/nicasource/back/vendor/craftcms/cms/src/config/GeneralConfig.php\",\"line\":1772,\"class\":\"craft\\\\services\\\\Deprecator\",\"method\":\"log\",\"args\":\"\\\"enableGraphQlCaching\\\", \\\"The `enableGraphQlCaching` config setting has been renamed to `e...\\\", \\\"/var/www/html/nicasource/back/config/general.php\\\"\"},{\"objectClass\":\"craft\\\\config\\\\GeneralConfig\",\"file\":\"/var/www/html/nicasource/back/vendor/yiisoft/yii2/BaseYii.php\",\"line\":558,\"class\":\"craft\\\\config\\\\GeneralConfig\",\"method\":\"__set\",\"args\":\"\\\"enableGraphQlCaching\\\", true\"},{\"objectClass\":null,\"file\":\"/var/www/html/nicasource/back/vendor/yiisoft/yii2/base/BaseObject.php\",\"line\":107,\"class\":\"yii\\\\BaseYii\",\"method\":\"configure\",\"args\":\"craft\\\\config\\\\GeneralConfig, [\\\"defaultWeekStartDay\\\" => 1, \\\"omitScriptNameInUrls\\\" => true, \\\"cpTrigger\\\" => \\\"admin\\\", \\\"securityKey\\\" => \\\"YjEjjTN6nfNGEi3QFy3gmeC2_V-MECsC\\\", ...]\"},{\"objectClass\":\"craft\\\\config\\\\GeneralConfig\",\"file\":\"/var/www/html/nicasource/back/vendor/craftcms/cms/src/services/Config.php\",\"line\":100,\"class\":\"yii\\\\base\\\\BaseObject\",\"method\":\"__construct\",\"args\":\"[\\\"defaultWeekStartDay\\\" => 1, \\\"omitScriptNameInUrls\\\" => true, \\\"cpTrigger\\\" => \\\"admin\\\", \\\"securityKey\\\" => \\\"YjEjjTN6nfNGEi3QFy3gmeC2_V-MECsC\\\", ...]\"},{\"objectClass\":\"craft\\\\services\\\\Config\",\"file\":\"/var/www/html/nicasource/back/vendor/craftcms/cms/src/services/Config.php\",\"line\":168,\"class\":\"craft\\\\services\\\\Config\",\"method\":\"getConfigSettings\",\"args\":\"\\\"general\\\"\"},{\"objectClass\":\"craft\\\\services\\\\Config\",\"file\":\"/var/www/html/nicasource/back/vendor/craftcms/cms/src/helpers/App.php\",\"line\":767,\"class\":\"craft\\\\services\\\\Config\",\"method\":\"getGeneral\",\"args\":null},{\"objectClass\":null,\"file\":\"/var/www/html/nicasource/back/vendor/craftcms/cms/src/config/app.web.php\",\"line\":11,\"class\":\"craft\\\\helpers\\\\App\",\"method\":\"webRequestConfig\",\"args\":null},{\"objectClass\":null,\"file\":null,\"line\":null,\"class\":null,\"method\":\"{closure}\",\"args\":null},{\"objectClass\":null,\"file\":\"/var/www/html/nicasource/back/vendor/yiisoft/yii2/di/Container.php\",\"line\":634,\"class\":null,\"method\":\"call_user_func_array\",\"args\":\"Closure, []\"},{\"objectClass\":\"yii\\\\di\\\\Container\",\"file\":\"/var/www/html/nicasource/back/vendor/yiisoft/yii2/BaseYii.php\",\"line\":349,\"class\":\"yii\\\\di\\\\Container\",\"method\":\"invoke\",\"args\":\"Closure, []\"},{\"objectClass\":null,\"file\":\"/var/www/html/nicasource/back/vendor/yiisoft/yii2/di/ServiceLocator.php\",\"line\":137,\"class\":\"yii\\\\BaseYii\",\"method\":\"createObject\",\"args\":\"Closure\"},{\"objectClass\":\"craft\\\\web\\\\Application\",\"file\":\"/var/www/html/nicasource/back/vendor/yiisoft/yii2/base/Module.php\",\"line\":748,\"class\":\"yii\\\\di\\\\ServiceLocator\",\"method\":\"get\",\"args\":\"\\\"request\\\", true\"},{\"objectClass\":\"craft\\\\web\\\\Application\",\"file\":\"/var/www/html/nicasource/back/vendor/craftcms/cms/src/web/Application.php\",\"line\":337,\"class\":\"yii\\\\base\\\\Module\",\"method\":\"get\",\"args\":\"\\\"request\\\", true\"},{\"objectClass\":\"craft\\\\web\\\\Application\",\"file\":\"/var/www/html/nicasource/back/vendor/yiisoft/yii2/web/Application.php\",\"line\":161,\"class\":\"craft\\\\web\\\\Application\",\"method\":\"get\",\"args\":\"\\\"request\\\"\"},{\"objectClass\":\"craft\\\\web\\\\Application\",\"file\":\"/var/www/html/nicasource/back/vendor/craftcms/cms/src/base/ApplicationTrait.php\",\"line\":1439,\"class\":\"yii\\\\web\\\\Application\",\"method\":\"getRequest\",\"args\":null},{\"objectClass\":\"craft\\\\web\\\\Application\",\"file\":\"/var/www/html/nicasource/back/vendor/craftcms/cms/src/web/Application.php\",\"line\":90,\"class\":\"craft\\\\web\\\\Application\",\"method\":\"_preInit\",\"args\":null},{\"objectClass\":\"craft\\\\web\\\\Application\",\"file\":\"/var/www/html/nicasource/back/vendor/yiisoft/yii2/base/BaseObject.php\",\"line\":109,\"class\":\"craft\\\\web\\\\Application\",\"method\":\"init\",\"args\":null},{\"objectClass\":\"craft\\\\web\\\\Application\",\"file\":\"/var/www/html/nicasource/back/vendor/yiisoft/yii2/base/Application.php\",\"line\":212,\"class\":\"yii\\\\base\\\\BaseObject\",\"method\":\"__construct\",\"args\":\"[\\\"env\\\" => \\\"dev\\\", \\\"components\\\" => [\\\"config\\\" => craft\\\\services\\\\Config, \\\"announcements\\\" => [\\\"class\\\" => \\\"craft\\\\services\\\\Announcements\\\"], \\\"api\\\" => [\\\"class\\\" => \\\"craft\\\\services\\\\Api\\\"], \\\"assets\\\" => [\\\"class\\\" => \\\"craft\\\\services\\\\Assets\\\"], ...], \\\"id\\\" => \\\"CraftCMS--1ccc6265-970d-4f26-b693-4d3438ab04e4\\\", \\\"name\\\" => \\\"Craft CMS\\\", ...]\"},{\"objectClass\":\"craft\\\\web\\\\Application\",\"file\":null,\"line\":null,\"class\":\"yii\\\\base\\\\Application\",\"method\":\"__construct\",\"args\":\"[\\\"env\\\" => \\\"dev\\\", \\\"components\\\" => [\\\"config\\\" => craft\\\\services\\\\Config, \\\"announcements\\\" => [\\\"class\\\" => \\\"craft\\\\services\\\\Announcements\\\"], \\\"api\\\" => [\\\"class\\\" => \\\"craft\\\\services\\\\Api\\\"], \\\"assets\\\" => [\\\"class\\\" => \\\"craft\\\\services\\\\Assets\\\"], ...], \\\"id\\\" => \\\"CraftCMS--1ccc6265-970d-4f26-b693-4d3438ab04e4\\\", \\\"name\\\" => \\\"Craft CMS\\\", ...]\"},{\"objectClass\":\"ReflectionClass\",\"file\":\"/var/www/html/nicasource/back/vendor/yiisoft/yii2/di/Container.php\",\"line\":420,\"class\":\"ReflectionClass\",\"method\":\"newInstanceArgs\",\"args\":\"[[\\\"vendorPath\\\" => \\\"/var/www/html/nicasource/back/vendor\\\", \\\"env\\\" => \\\"dev\\\", \\\"components\\\" => [\\\"config\\\" => craft\\\\services\\\\Config, \\\"announcements\\\" => [\\\"class\\\" => \\\"craft\\\\services\\\\Announcements\\\"], \\\"api\\\" => [\\\"class\\\" => \\\"craft\\\\services\\\\Api\\\"], \\\"assets\\\" => [\\\"class\\\" => \\\"craft\\\\services\\\\Assets\\\"], ...], \\\"id\\\" => \\\"CraftCMS--1ccc6265-970d-4f26-b693-4d3438ab04e4\\\", ...]]\"},{\"objectClass\":\"yii\\\\di\\\\Container\",\"file\":\"/var/www/html/nicasource/back/vendor/yiisoft/yii2/di/Container.php\",\"line\":171,\"class\":\"yii\\\\di\\\\Container\",\"method\":\"build\",\"args\":\"\\\"craft\\\\web\\\\Application\\\", [], [\\\"vendorPath\\\" => \\\"/var/www/html/nicasource/back/vendor\\\", \\\"env\\\" => \\\"dev\\\", \\\"components\\\" => [\\\"config\\\" => craft\\\\services\\\\Config, \\\"announcements\\\" => [\\\"class\\\" => \\\"craft\\\\services\\\\Announcements\\\"], \\\"api\\\" => [\\\"class\\\" => \\\"craft\\\\services\\\\Api\\\"], \\\"assets\\\" => [\\\"class\\\" => \\\"craft\\\\services\\\\Assets\\\"], ...], \\\"id\\\" => \\\"CraftCMS--1ccc6265-970d-4f26-b693-4d3438ab04e4\\\", ...]\"},{\"objectClass\":\"yii\\\\di\\\\Container\",\"file\":\"/var/www/html/nicasource/back/vendor/yiisoft/yii2/BaseYii.php\",\"line\":365,\"class\":\"yii\\\\di\\\\Container\",\"method\":\"get\",\"args\":\"\\\"craft\\\\web\\\\Application\\\", [], [\\\"vendorPath\\\" => \\\"/var/www/html/nicasource/back/vendor\\\", \\\"env\\\" => \\\"dev\\\", \\\"components\\\" => [\\\"config\\\" => craft\\\\services\\\\Config, \\\"announcements\\\" => [\\\"class\\\" => \\\"craft\\\\services\\\\Announcements\\\"], \\\"api\\\" => [\\\"class\\\" => \\\"craft\\\\services\\\\Api\\\"], \\\"assets\\\" => [\\\"class\\\" => \\\"craft\\\\services\\\\Assets\\\"], ...], \\\"id\\\" => \\\"CraftCMS--1ccc6265-970d-4f26-b693-4d3438ab04e4\\\", ...]\"},{\"objectClass\":null,\"file\":\"/var/www/html/nicasource/back/vendor/craftcms/cms/bootstrap/bootstrap.php\",\"line\":242,\"class\":\"yii\\\\BaseYii\",\"method\":\"createObject\",\"args\":\"[\\\"vendorPath\\\" => \\\"/var/www/html/nicasource/back/vendor\\\", \\\"env\\\" => \\\"dev\\\", \\\"components\\\" => [\\\"config\\\" => craft\\\\services\\\\Config, \\\"announcements\\\" => [\\\"class\\\" => \\\"craft\\\\services\\\\Announcements\\\"], \\\"api\\\" => [\\\"class\\\" => \\\"craft\\\\services\\\\Api\\\"], \\\"assets\\\" => [\\\"class\\\" => \\\"craft\\\\services\\\\Assets\\\"], ...], \\\"id\\\" => \\\"CraftCMS--1ccc6265-970d-4f26-b693-4d3438ab04e4\\\", ...]\"},{\"objectClass\":null,\"file\":\"/var/www/html/nicasource/back/vendor/craftcms/cms/bootstrap/web.php\",\"line\":51,\"class\":null,\"method\":\"require\",\"args\":\"\\\"/var/www/html/nicasource/back/vendor/craftcms/cms/bootstrap/boot...\\\"\"},{\"objectClass\":null,\"file\":\"/var/www/html/nicasource/back/web/index.php\",\"line\":28,\"class\":null,\"method\":\"require\",\"args\":\"\\\"/var/www/html/nicasource/back/vendor/craftcms/cms/bootstrap/web....\\\"\"}]', '2021-11-15 13:39:39', '2021-11-15 13:39:39', '7aa1c73f-860c-4ee3-9076-cceb53dccb20');

-- --------------------------------------------------------

--
-- Table structure for table `blg_drafts`
--

CREATE TABLE `blg_drafts` (
  `id` int NOT NULL,
  `sourceId` int DEFAULT NULL,
  `creatorId` int DEFAULT NULL,
  `provisional` tinyint(1) NOT NULL DEFAULT '0',
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `notes` text COLLATE utf8_unicode_ci,
  `trackChanges` tinyint(1) NOT NULL DEFAULT '0',
  `dateLastMerged` datetime DEFAULT NULL,
  `saved` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `blg_drafts`
--

INSERT INTO `blg_drafts` (`id`, `sourceId`, `creatorId`, `provisional`, `name`, `notes`, `trackChanges`, `dateLastMerged`, `saved`) VALUES
(2, 2, 1, 1, 'Draft 1', '', 1, NULL, 1),
(3, NULL, 1, 0, 'First draft', NULL, 0, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `blg_elementindexsettings`
--

CREATE TABLE `blg_elementindexsettings` (
  `id` int NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `settings` text COLLATE utf8_unicode_ci,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `blg_elements`
--

CREATE TABLE `blg_elements` (
  `id` int NOT NULL,
  `canonicalId` int DEFAULT NULL,
  `draftId` int DEFAULT NULL,
  `revisionId` int DEFAULT NULL,
  `fieldLayoutId` int DEFAULT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `archived` tinyint(1) NOT NULL DEFAULT '0',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `dateLastMerged` datetime DEFAULT NULL,
  `dateDeleted` datetime DEFAULT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `blg_elements`
--

INSERT INTO `blg_elements` (`id`, `canonicalId`, `draftId`, `revisionId`, `fieldLayoutId`, `type`, `enabled`, `archived`, `dateCreated`, `dateUpdated`, `dateLastMerged`, `dateDeleted`, `uid`) VALUES
(1, NULL, NULL, NULL, NULL, 'craft\\elements\\User', 1, 0, '2021-11-14 23:59:06', '2021-11-15 03:59:37', NULL, NULL, '261b973d-529b-4456-a1a1-4b2bc9d7dbe5'),
(2, NULL, NULL, NULL, 1, 'craft\\elements\\Entry', 1, 0, '2021-11-15 01:06:17', '2021-11-15 03:28:50', NULL, '2021-11-15 03:36:12', '8d158c79-80e2-45f1-8ea6-fbe9d138abfa'),
(3, 2, NULL, 1, 1, 'craft\\elements\\Entry', 1, 0, '2021-11-15 01:06:17', '2021-11-15 01:06:18', NULL, '2021-11-15 03:36:12', 'cc71ceba-dee1-44da-80f1-203a1297bc6f'),
(4, 2, NULL, 2, 1, 'craft\\elements\\Entry', 1, 0, '2021-11-15 01:06:18', '2021-11-15 01:06:18', NULL, '2021-11-15 03:36:12', '8ad13d57-a718-4ffa-b0ac-5856b2f8f179'),
(5, 2, NULL, 3, 1, 'craft\\elements\\Entry', 1, 0, '2021-11-15 01:06:42', '2021-11-15 01:06:42', NULL, '2021-11-15 03:36:12', 'e42b228f-ac41-4035-8ec5-90081381815b'),
(6, 2, NULL, 4, 1, 'craft\\elements\\Entry', 1, 0, '2021-11-15 01:06:43', '2021-11-15 01:06:43', NULL, '2021-11-15 03:36:12', '6612f285-a7c1-44da-b2c3-990afa5dfe21'),
(7, NULL, NULL, NULL, 3, 'craft\\elements\\Tag', 1, 0, '2021-11-15 03:26:43', '2021-11-15 03:26:43', NULL, NULL, '3542f0fe-1384-4353-8886-f5ba630b7aad'),
(10, NULL, NULL, NULL, 2, 'craft\\elements\\Asset', 1, 0, '2021-11-15 03:28:32', '2021-11-15 03:28:32', NULL, NULL, '4f4e13c9-2338-4daf-9f1c-4ee4379ab5fb'),
(11, 2, NULL, 5, 1, 'craft\\elements\\Entry', 1, 0, '2021-11-15 03:28:50', '2021-11-15 03:28:50', NULL, '2021-11-15 03:36:12', 'd97edc29-207e-4d3e-ad1c-4ee3b703ac68'),
(12, 2, 2, NULL, 1, 'craft\\elements\\Entry', 1, 0, '2021-11-15 03:33:52', '2021-11-15 03:33:53', NULL, '2021-11-15 03:36:12', 'eda56a36-bb72-4f8f-bf88-3f802632a820'),
(13, NULL, 3, NULL, 1, 'craft\\elements\\Entry', 1, 0, '2021-11-15 03:35:45', '2021-11-15 03:35:45', NULL, NULL, '43141d87-999f-40de-a323-ec861741bc05'),
(14, NULL, NULL, NULL, 1, 'craft\\elements\\Entry', 1, 0, '2021-11-15 03:39:50', '2021-11-15 18:16:34', NULL, NULL, '3ad0ad1b-e85c-4150-994f-452fabe629b4'),
(15, 14, NULL, 6, 1, 'craft\\elements\\Entry', 1, 0, '2021-11-15 03:41:07', '2021-11-15 03:41:07', NULL, NULL, 'c0640583-f586-4673-9b45-450425c63999'),
(16, 14, NULL, 7, 1, 'craft\\elements\\Entry', 1, 0, '2021-11-15 03:42:07', '2021-11-15 03:42:07', NULL, NULL, '3ccb855e-536f-433c-b303-1501e6354fa7'),
(18, NULL, NULL, NULL, 1, 'craft\\elements\\Entry', 1, 0, '2021-11-15 03:42:24', '2021-11-15 18:16:52', NULL, NULL, 'ec148268-69fd-4722-abdb-407b84131f85'),
(19, NULL, NULL, NULL, 2, 'craft\\elements\\Asset', 1, 0, '2021-11-15 03:45:07', '2021-11-15 03:45:07', NULL, NULL, '67a68be1-cf82-477e-bb05-2478ac829b0a'),
(20, NULL, NULL, NULL, 2, 'craft\\elements\\Asset', 1, 0, '2021-11-15 03:45:08', '2021-11-15 03:45:08', NULL, NULL, 'b601c5f4-9e69-4c4f-bb19-53fbde2371a1'),
(21, NULL, NULL, NULL, 2, 'craft\\elements\\Asset', 1, 0, '2021-11-15 03:45:09', '2021-11-15 03:45:09', NULL, NULL, 'c1ade5ab-1b64-4b0a-b7ab-dd5d87e65808'),
(22, NULL, NULL, NULL, 2, 'craft\\elements\\Asset', 1, 0, '2021-11-15 03:45:11', '2021-11-15 03:45:11', NULL, NULL, '64ea8921-23d6-468a-9713-8c5b6e5499eb'),
(23, NULL, NULL, NULL, 2, 'craft\\elements\\Asset', 1, 0, '2021-11-15 03:45:12', '2021-11-15 03:45:12', NULL, NULL, '87d141a4-bc9d-402e-850d-68cb2b21625d'),
(24, NULL, NULL, NULL, 2, 'craft\\elements\\Asset', 1, 0, '2021-11-15 03:45:13', '2021-11-15 03:45:13', NULL, NULL, 'b80b36c8-3742-4d87-b58b-c49e0f2e8ade'),
(25, NULL, NULL, NULL, 2, 'craft\\elements\\Asset', 1, 0, '2021-11-15 03:45:14', '2021-11-15 03:45:14', NULL, NULL, '4bc96ab0-06a7-4bb6-9031-2fc48a961065'),
(26, NULL, NULL, NULL, 2, 'craft\\elements\\Asset', 1, 0, '2021-11-15 03:45:15', '2021-11-15 03:45:15', NULL, NULL, '06b35651-d7fe-4332-91d3-339fa4405ead'),
(27, NULL, NULL, NULL, 2, 'craft\\elements\\Asset', 1, 0, '2021-11-15 03:45:17', '2021-11-15 03:45:17', NULL, NULL, 'bdbe200a-e386-48ca-8e23-47910196d3e7'),
(28, 18, NULL, 8, 1, 'craft\\elements\\Entry', 0, 0, '2021-11-15 03:45:35', '2021-11-15 03:45:35', NULL, NULL, '560efe2f-16f2-4db3-810a-3596d23c3cd1'),
(30, 18, NULL, 9, 1, 'craft\\elements\\Entry', 0, 0, '2021-11-15 03:52:08', '2021-11-15 03:52:08', NULL, NULL, '137753fb-f6da-4fca-8227-3492b70e6131'),
(31, 14, NULL, 10, 1, 'craft\\elements\\Entry', 1, 0, '2021-11-15 03:52:36', '2021-11-15 03:52:36', NULL, NULL, 'fb2112b8-1f83-4d3a-afcb-46fda37019fc'),
(33, 18, NULL, 11, 1, 'craft\\elements\\Entry', 0, 0, '2021-11-15 03:52:42', '2021-11-15 03:52:42', NULL, NULL, '52a322fe-294a-4b99-8b1e-abbb96c1929a'),
(35, 18, NULL, 12, 1, 'craft\\elements\\Entry', 1, 0, '2021-11-15 03:53:02', '2021-11-15 03:53:02', NULL, NULL, '8e30ad87-bf56-4f91-b33a-af61bb303f33'),
(36, NULL, NULL, NULL, 2, 'craft\\elements\\Asset', 1, 0, '2021-11-15 03:59:37', '2021-11-15 03:59:37', NULL, NULL, '0ec44958-dc94-4566-b7a6-7c620bee318d'),
(38, 14, NULL, 13, 1, 'craft\\elements\\Entry', 1, 0, '2021-11-15 18:16:34', '2021-11-15 18:16:35', NULL, NULL, 'df6aa2a9-6762-4ed3-ac46-591c8e51f8a2'),
(40, 18, NULL, 14, 1, 'craft\\elements\\Entry', 1, 0, '2021-11-15 18:16:52', '2021-11-15 18:16:52', NULL, NULL, 'b6cfac71-4e90-4060-a2a7-3ab3e5688fc4'),
(41, NULL, NULL, NULL, 1, 'craft\\elements\\Entry', 1, 0, '2021-11-15 18:24:54', '2021-11-15 18:35:38', NULL, NULL, 'dee341ee-3e01-4681-bdd3-77e078e601c8'),
(42, 41, NULL, 15, 1, 'craft\\elements\\Entry', 1, 0, '2021-11-15 18:34:52', '2021-11-15 18:34:52', NULL, NULL, 'd2248700-d1c5-4eb8-ac8b-e81dec3c58fb'),
(44, 41, NULL, 16, 1, 'craft\\elements\\Entry', 1, 0, '2021-11-15 18:35:38', '2021-11-15 18:35:38', NULL, NULL, '91421ca0-dcb9-4183-8d27-99a41b8d448b'),
(45, NULL, NULL, NULL, 1, 'craft\\elements\\Entry', 1, 0, '2021-11-15 18:39:00', '2021-11-15 18:39:53', NULL, NULL, 'cd2279bc-e53f-46cf-91fe-898ace6f7e32'),
(46, 45, NULL, 17, 1, 'craft\\elements\\Entry', 1, 0, '2021-11-15 18:39:53', '2021-11-15 18:39:53', NULL, NULL, 'b60ec397-4720-40b0-907e-13ec225ea8c1'),
(47, NULL, NULL, NULL, 1, 'craft\\elements\\Entry', 1, 0, '2021-11-15 18:41:22', '2021-11-15 18:41:55', NULL, NULL, 'dbdb6c0b-484c-46c8-bba5-4c3240e05af3'),
(48, 47, NULL, 18, 1, 'craft\\elements\\Entry', 1, 0, '2021-11-15 18:41:55', '2021-11-15 18:41:55', NULL, NULL, 'da9710bf-b21a-4bd3-9aee-3139453da25b'),
(49, NULL, NULL, NULL, 1, 'craft\\elements\\Entry', 1, 0, '2021-11-15 18:42:21', '2021-11-15 18:43:00', NULL, NULL, '3212fdc4-a2d6-428b-9386-3c558b1aec1a'),
(50, 49, NULL, 19, 1, 'craft\\elements\\Entry', 1, 0, '2021-11-15 18:43:00', '2021-11-15 18:43:00', NULL, NULL, '0d2cdeeb-d7ff-4850-b555-54b5b4ab867a'),
(51, NULL, NULL, NULL, 1, 'craft\\elements\\Entry', 1, 0, '2021-11-15 18:43:01', '2021-11-15 18:43:39', NULL, NULL, '0aa1a736-46d3-4a10-a828-0ade1ef07060'),
(52, 51, NULL, 20, 1, 'craft\\elements\\Entry', 1, 0, '2021-11-15 18:43:39', '2021-11-15 18:43:39', NULL, NULL, 'cc14a011-9202-480b-b2b7-a075f92e5936'),
(53, NULL, NULL, NULL, 1, 'craft\\elements\\Entry', 1, 0, '2021-11-15 18:44:08', '2021-11-15 18:44:47', NULL, NULL, '6cca91d2-645b-484f-88ea-79d9b0f38ee1'),
(54, 53, NULL, 21, 1, 'craft\\elements\\Entry', 1, 0, '2021-11-15 18:44:47', '2021-11-15 18:44:47', NULL, NULL, 'a2efa40b-6fd6-47fd-9797-bbb026352507'),
(55, NULL, NULL, NULL, 1, 'craft\\elements\\Entry', 1, 0, '2021-11-15 18:44:48', '2021-11-15 18:45:24', NULL, NULL, '827848a1-351c-4f1a-a9b4-0dc1e4ec5fdd'),
(56, 55, NULL, 22, 1, 'craft\\elements\\Entry', 1, 0, '2021-11-15 18:45:24', '2021-11-15 18:45:24', NULL, NULL, '32ff238e-7b3e-4709-b5f5-58ca57c3a45e'),
(57, NULL, NULL, NULL, 1, 'craft\\elements\\Entry', 1, 0, '2021-11-15 18:45:58', '2021-11-15 18:46:31', NULL, NULL, '710d92ec-c6e9-409e-a3cf-1a6eab31df77'),
(58, 57, NULL, 23, 1, 'craft\\elements\\Entry', 1, 0, '2021-11-15 18:46:31', '2021-11-15 18:46:31', NULL, NULL, '5ea97fbb-6983-4d92-a076-bac6d11ad89a'),
(59, NULL, NULL, NULL, 1, 'craft\\elements\\Entry', 1, 0, '2021-11-15 18:46:44', '2021-11-15 18:46:44', NULL, NULL, '504558e0-4746-4f09-b339-d1283f4118ef'),
(60, 59, NULL, 24, 1, 'craft\\elements\\Entry', 1, 0, '2021-11-15 18:46:44', '2021-11-15 18:46:44', NULL, NULL, '6882c566-d354-4362-91e7-51ea73df19f3'),
(61, NULL, NULL, NULL, 1, 'craft\\elements\\Entry', 1, 0, '2021-11-15 18:46:44', '2021-11-15 18:46:44', NULL, NULL, '821b031a-e835-49f5-a54e-3441bdfa9b2f'),
(62, 61, NULL, 25, 1, 'craft\\elements\\Entry', 1, 0, '2021-11-15 18:46:44', '2021-11-15 18:46:44', NULL, NULL, '7419ce42-db2d-4b7c-8b88-4a00f5cfd4c0'),
(63, NULL, NULL, NULL, 1, 'craft\\elements\\Entry', 1, 0, '2021-11-15 18:46:45', '2021-11-15 18:46:45', NULL, NULL, '56571a32-193b-48c7-8a3e-bd097dabd680'),
(64, 63, NULL, 26, 1, 'craft\\elements\\Entry', 1, 0, '2021-11-15 18:46:45', '2021-11-15 18:46:45', NULL, NULL, '39bc38c0-ed93-4e0a-8ca8-9dd4a24bf438'),
(65, NULL, NULL, NULL, 1, 'craft\\elements\\Entry', 1, 0, '2021-11-15 18:50:07', '2021-11-15 18:51:18', NULL, NULL, '5a2cdd14-f12b-4351-a2f6-fe2d925a5a3d'),
(66, NULL, NULL, NULL, 2, 'craft\\elements\\Asset', 1, 0, '2021-11-15 18:50:34', '2021-11-15 18:50:34', NULL, NULL, '19388037-ee1a-461e-8475-1a7082115ad0'),
(67, NULL, NULL, NULL, 2, 'craft\\elements\\Asset', 1, 0, '2021-11-15 18:50:35', '2021-11-15 18:50:35', NULL, NULL, '02a9872d-9d3e-4f45-9bb5-ae10fe774823'),
(68, NULL, NULL, NULL, 2, 'craft\\elements\\Asset', 1, 0, '2021-11-15 18:50:37', '2021-11-15 18:50:37', NULL, NULL, '1229d308-4cae-4d9a-a478-2a7b11329ad6'),
(69, NULL, NULL, NULL, 3, 'craft\\elements\\Tag', 1, 0, '2021-11-15 18:51:06', '2021-11-15 18:51:06', NULL, NULL, 'c96e90f6-29a0-439d-aa82-e8b6238f613f'),
(70, 65, NULL, 27, 1, 'craft\\elements\\Entry', 1, 0, '2021-11-15 18:51:18', '2021-11-15 18:51:18', NULL, NULL, 'aa1cb841-e3d2-4fd5-8e60-6abe7b61d9d8'),
(71, NULL, NULL, NULL, 1, 'craft\\elements\\Entry', 1, 0, '2021-11-15 18:51:20', '2021-11-15 18:52:00', NULL, NULL, '41491dce-801e-4367-afeb-b24b57db69bc'),
(72, NULL, NULL, NULL, 3, 'craft\\elements\\Tag', 1, 0, '2021-11-15 18:51:51', '2021-11-15 18:51:51', NULL, NULL, 'f3089af7-e2f3-4e58-a50d-efa0eee11435'),
(73, 71, NULL, 28, 1, 'craft\\elements\\Entry', 1, 0, '2021-11-15 18:52:00', '2021-11-15 18:52:00', NULL, NULL, '857272dd-59a5-439a-8900-295006a490c9'),
(74, NULL, NULL, NULL, 1, 'craft\\elements\\Entry', 1, 0, '2021-11-15 18:52:02', '2021-11-15 18:52:40', NULL, NULL, '0792204e-d2d0-406f-ba01-c954d03722af'),
(75, NULL, NULL, NULL, 3, 'craft\\elements\\Tag', 1, 0, '2021-11-15 18:52:27', '2021-11-15 18:52:27', NULL, NULL, '2b735f32-7b10-4865-a30a-c05054490a90'),
(76, 74, NULL, 29, 1, 'craft\\elements\\Entry', 1, 0, '2021-11-15 18:52:40', '2021-11-15 18:52:40', NULL, NULL, '98f9dcef-dbb0-4ba8-a94b-55b616572f11'),
(77, NULL, NULL, NULL, 1, 'craft\\elements\\Entry', 1, 0, '2021-11-15 18:53:38', '2021-11-15 18:53:38', NULL, NULL, '0ca0b907-069a-4dd3-b8cd-2c1347b22f38'),
(78, 77, NULL, 30, 1, 'craft\\elements\\Entry', 1, 0, '2021-11-15 18:53:38', '2021-11-15 18:53:38', NULL, NULL, 'fdd2e6e1-fc57-4419-b757-8b3f3a3910fb'),
(79, NULL, NULL, NULL, 1, 'craft\\elements\\Entry', 1, 0, '2021-11-15 18:53:39', '2021-11-15 18:53:39', NULL, NULL, 'c17466d0-433f-46bf-b21c-e8cb96cb0297'),
(80, 79, NULL, 31, 1, 'craft\\elements\\Entry', 1, 0, '2021-11-15 18:53:39', '2021-11-15 18:53:39', NULL, NULL, '72651f30-72f0-4650-b121-9071fd0da811'),
(81, NULL, NULL, NULL, 1, 'craft\\elements\\Entry', 1, 0, '2021-11-15 18:53:39', '2021-11-15 18:53:39', NULL, NULL, 'b9083a72-7f14-4686-9dae-b0a5501d20c8'),
(82, 81, NULL, 32, 1, 'craft\\elements\\Entry', 1, 0, '2021-11-15 18:53:39', '2021-11-15 18:53:39', NULL, NULL, '49a517d0-5798-4335-91e8-e9c950c7ccc2');

-- --------------------------------------------------------

--
-- Table structure for table `blg_elements_sites`
--

CREATE TABLE `blg_elements_sites` (
  `id` int NOT NULL,
  `elementId` int NOT NULL,
  `siteId` int NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `uri` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `blg_elements_sites`
--

INSERT INTO `blg_elements_sites` (`id`, `elementId`, `siteId`, `slug`, `uri`, `enabled`, `dateCreated`, `dateUpdated`, `uid`) VALUES
(1, 1, 1, NULL, NULL, 1, '2021-11-14 23:59:06', '2021-11-14 23:59:06', 'ee50def3-7cef-41ef-ba95-f07e689d4325'),
(2, 2, 1, 'entries', 'articles', 1, '2021-11-15 01:06:17', '2021-11-15 01:07:36', 'ab101354-c06b-4039-bed7-5a56952e06eb'),
(3, 3, 1, 'entries', 'entries', 1, '2021-11-15 01:06:18', '2021-11-15 01:06:18', 'acf60641-ccf1-4ae3-ba5d-3f833ce192d6'),
(4, 4, 1, 'entries', 'entries', 1, '2021-11-15 01:06:18', '2021-11-15 01:06:18', '6058073a-ac58-49a3-af2f-acaed54224b8'),
(5, 5, 1, 'entries', 'entries', 1, '2021-11-15 01:06:42', '2021-11-15 01:06:42', '3ac1875a-f335-469a-81df-d212efefcd28'),
(6, 6, 1, 'entries', 'entries', 1, '2021-11-15 01:06:43', '2021-11-15 01:06:43', '586a9cb0-3b52-477e-aef3-ca69d1a68a44'),
(7, 7, 1, 'books', NULL, 1, '2021-11-15 03:26:43', '2021-11-15 03:26:43', 'bbd04e66-48cc-4c79-8bda-70a685e20b20'),
(10, 10, 1, NULL, NULL, 1, '2021-11-15 03:28:32', '2021-11-15 03:28:32', 'c20c6dac-efd7-4f64-8947-9cd857c611c4'),
(11, 11, 1, 'entries', 'articles', 1, '2021-11-15 03:28:50', '2021-11-15 03:28:50', '65dbd404-ee5c-48fb-90d4-499e8605a06b'),
(12, 12, 1, 'entries', 'articles', 1, '2021-11-15 03:33:52', '2021-11-15 03:33:52', '303db630-09f9-418e-b1c8-609c56ac65f9'),
(13, 13, 1, '__temp_riofzevtrmwqfseysqjvmkqqwqwnjlespzbv', NULL, 1, '2021-11-15 03:35:45', '2021-11-15 03:35:45', '7188e779-eb4d-46c8-94ac-b73efceca39f'),
(14, 14, 1, 'rebuild-trust-after-sexual-betrayal-2', 'articles/admin/rebuild-trust-after-sexual-betrayal-2', 1, '2021-11-15 03:39:50', '2021-11-15 03:52:36', 'a3f578b0-5569-48dc-8292-00b6b77ec60c'),
(15, 15, 1, 'articles', 'articles', 1, '2021-11-15 03:41:07', '2021-11-15 03:41:07', '94f07310-c396-4c97-a98c-a7afef137239'),
(16, 16, 1, 'article-1', 'articles', 1, '2021-11-15 03:42:07', '2021-11-15 03:42:07', 'f26b3fb7-306f-4648-8439-e44e9a8b24c7'),
(18, 18, 1, 'rebuild-trust-after-sexual-betrayal-2-2', 'articles/admin/rebuild-trust-after-sexual-betrayal-2-2', 1, '2021-11-15 03:42:24', '2021-11-15 03:53:02', '825ca542-985c-44e8-8eb0-a844f93f3d53'),
(19, 19, 1, NULL, NULL, 1, '2021-11-15 03:45:07', '2021-11-15 03:45:07', '787f1193-39b7-4f67-a948-a615ccb89a19'),
(20, 20, 1, NULL, NULL, 1, '2021-11-15 03:45:08', '2021-11-15 03:45:08', 'f262705d-5c18-422b-8a78-7472592ebace'),
(21, 21, 1, NULL, NULL, 1, '2021-11-15 03:45:09', '2021-11-15 03:45:09', '739b2abd-104d-4465-9c8a-c3a58d7d11cb'),
(22, 22, 1, NULL, NULL, 1, '2021-11-15 03:45:11', '2021-11-15 03:45:11', '73736278-db4d-45e9-834a-bdf68ba4c170'),
(23, 23, 1, NULL, NULL, 1, '2021-11-15 03:45:12', '2021-11-15 03:45:12', '4c30f142-8dfc-4e3c-b839-1b77172dcfea'),
(24, 24, 1, NULL, NULL, 1, '2021-11-15 03:45:13', '2021-11-15 03:45:13', 'a66ff4f9-9a66-40ff-8b42-c64c20d8a321'),
(25, 25, 1, NULL, NULL, 1, '2021-11-15 03:45:14', '2021-11-15 03:45:14', '641f3ced-ca0d-4506-b012-6967ec361db4'),
(26, 26, 1, NULL, NULL, 1, '2021-11-15 03:45:15', '2021-11-15 03:45:15', '9bac499f-3f36-49a5-81ae-c96b7fd5c9f3'),
(27, 27, 1, NULL, NULL, 1, '2021-11-15 03:45:17', '2021-11-15 03:45:17', 'c65c6504-babd-4640-9d12-821a9d87b8ee'),
(28, 28, 1, 'articles', NULL, 1, '2021-11-15 03:45:35', '2021-11-15 03:45:35', 'b6d34697-5d69-440f-b3c9-af7bcd73906b'),
(30, 30, 1, 'rebuild-trust-after-sexual-betrayal', 'articles/admin/rebuild-trust-after-sexual-betrayal', 1, '2021-11-15 03:52:08', '2021-11-15 03:52:08', '498c297c-b0ef-4e80-acc0-acc26d7ac5a6'),
(31, 31, 1, 'rebuild-trust-after-sexual-betrayal-2', 'articles/admin/rebuild-trust-after-sexual-betrayal-2', 1, '2021-11-15 03:52:36', '2021-11-15 03:52:36', '10fc40fc-e365-451a-8f6a-34ae374388f4'),
(33, 33, 1, 'rebuild-trust-after-sexual-betrayal', 'articles/admin/rebuild-trust-after-sexual-betrayal', 1, '2021-11-15 03:52:42', '2021-11-15 03:52:42', '5c30de1d-bdf4-47a2-9d34-ec82c37f68e2'),
(35, 35, 1, 'rebuild-trust-after-sexual-betrayal-2-2', 'articles/admin/rebuild-trust-after-sexual-betrayal-2-2', 1, '2021-11-15 03:53:02', '2021-11-15 03:53:02', '980de125-1536-4a80-9618-ee22c3c2ee45'),
(36, 36, 1, NULL, NULL, 1, '2021-11-15 03:59:37', '2021-11-15 03:59:37', 'f6ef9088-a012-4cd3-a87b-e42d0e3b027a'),
(38, 38, 1, 'rebuild-trust-after-sexual-betrayal-2', 'articles/admin/rebuild-trust-after-sexual-betrayal-2', 1, '2021-11-15 18:16:35', '2021-11-15 18:16:35', '4e51f0cf-9153-44d2-a4be-d3525f7ac12e'),
(40, 40, 1, 'rebuild-trust-after-sexual-betrayal-2-2', 'articles/admin/rebuild-trust-after-sexual-betrayal-2-2', 1, '2021-11-15 18:16:52', '2021-11-15 18:16:52', '465af5f1-1dbd-4d0d-a361-8fdbe42cfcb1'),
(41, 41, 1, 'lorem-ipsum-dolor-sit-amet-consectetur-adipiscing-elit', 'articles/admin/lorem-ipsum-dolor-sit-amet-consectetur-adipiscing-elit', 1, '2021-11-15 18:24:54', '2021-11-15 18:34:52', 'bce86114-860f-4172-a34e-561fcbafe0cd'),
(42, 42, 1, 'lorem-ipsum-dolor-sit-amet-consectetur-adipiscing-elit', 'articles/admin/lorem-ipsum-dolor-sit-amet-consectetur-adipiscing-elit', 1, '2021-11-15 18:34:52', '2021-11-15 18:34:52', '60aa5132-8af2-48c3-abe8-b67371a9bc22'),
(44, 44, 1, 'lorem-ipsum-dolor-sit-amet-consectetur-adipiscing-elit', 'articles/admin/lorem-ipsum-dolor-sit-amet-consectetur-adipiscing-elit', 1, '2021-11-15 18:35:38', '2021-11-15 18:35:38', 'a2fdf839-efa7-4c28-9337-0bfa6a4663d7'),
(45, 45, 1, 'quisque-ac-ligula-a-odio-condimentum-sagittis', 'articles/admin/quisque-ac-ligula-a-odio-condimentum-sagittis', 1, '2021-11-15 18:39:00', '2021-11-15 18:39:53', '6f88d056-10b2-4db7-9a15-0d7c93572173'),
(46, 46, 1, 'quisque-ac-ligula-a-odio-condimentum-sagittis', 'articles/admin/quisque-ac-ligula-a-odio-condimentum-sagittis', 1, '2021-11-15 18:39:53', '2021-11-15 18:39:53', '4d4126a7-e0ea-45b2-b8b4-a67828744140'),
(47, 47, 1, 'aliquam-erat-volutpat', 'articles/admin/aliquam-erat-volutpat', 1, '2021-11-15 18:41:22', '2021-11-15 18:41:55', '6b1b59e4-1f43-4423-8dbd-bb8d3ce8bee0'),
(48, 48, 1, 'aliquam-erat-volutpat', 'articles/admin/aliquam-erat-volutpat', 1, '2021-11-15 18:41:55', '2021-11-15 18:41:55', '2782fda4-9462-492f-817c-d1499e1c1b05'),
(49, 49, 1, 'vivamus-urna-lacus-posuere-quis-urna-eget-iaculis-suscipit-ex', 'articles/admin/vivamus-urna-lacus-posuere-quis-urna-eget-iaculis-suscipit-ex', 1, '2021-11-15 18:42:21', '2021-11-15 18:43:00', '7dad95b7-8f9d-4a0c-9ec4-e5606f5f3891'),
(50, 50, 1, 'vivamus-urna-lacus-posuere-quis-urna-eget-iaculis-suscipit-ex', 'articles/admin/vivamus-urna-lacus-posuere-quis-urna-eget-iaculis-suscipit-ex', 1, '2021-11-15 18:43:00', '2021-11-15 18:43:00', '93b33fe1-0eb5-448b-ab00-131d81a59ae1'),
(51, 51, 1, 'suspendisse-ultrices-maximus-massa-ac-aliquet-quam-pellentesque-ut', 'articles/admin/suspendisse-ultrices-maximus-massa-ac-aliquet-quam-pellentesque-ut', 1, '2021-11-15 18:43:01', '2021-11-15 18:43:39', '899c6c96-aab2-4bd4-a3fe-70a8f7e2f135'),
(52, 52, 1, 'suspendisse-ultrices-maximus-massa-ac-aliquet-quam-pellentesque-ut', 'articles/admin/suspendisse-ultrices-maximus-massa-ac-aliquet-quam-pellentesque-ut', 1, '2021-11-15 18:43:39', '2021-11-15 18:43:39', '07b6b38f-1ac0-4c32-b7c0-70c27aa3f133'),
(53, 53, 1, 'duis-sit-amet-facilisis-sapien', 'articles/admin/duis-sit-amet-facilisis-sapien', 1, '2021-11-15 18:44:08', '2021-11-15 18:44:47', 'dbc0faa4-e3ba-44f8-85a2-4676dc720949'),
(54, 54, 1, 'duis-sit-amet-facilisis-sapien', 'articles/admin/duis-sit-amet-facilisis-sapien', 1, '2021-11-15 18:44:47', '2021-11-15 18:44:47', '28a89d8b-e54e-44c7-ae04-9cf8662b14f4'),
(55, 55, 1, 'aenean-faucibus-mollis-dui-malesuada-commodo', 'articles/admin/aenean-faucibus-mollis-dui-malesuada-commodo', 1, '2021-11-15 18:44:48', '2021-11-15 18:45:24', 'b82c8f9a-891a-43bc-b8e7-ed856aa95f10'),
(56, 56, 1, 'aenean-faucibus-mollis-dui-malesuada-commodo', 'articles/admin/aenean-faucibus-mollis-dui-malesuada-commodo', 1, '2021-11-15 18:45:24', '2021-11-15 18:45:24', 'c6d54883-6f89-46ec-adc5-3024044e098c'),
(57, 57, 1, 'in-viverra-est-et-pretium-finibus', 'articles/admin/in-viverra-est-et-pretium-finibus', 1, '2021-11-15 18:45:58', '2021-11-15 18:46:31', '23f2e9a9-db4c-435e-8431-2fb7f8c7a4bc'),
(58, 58, 1, 'in-viverra-est-et-pretium-finibus', 'articles/admin/in-viverra-est-et-pretium-finibus', 1, '2021-11-15 18:46:31', '2021-11-15 18:46:31', 'b20567fa-e068-4157-b511-86d28cbee42d'),
(59, 59, 1, 'duis-sit-amet-facilisis-sapien-2', 'articles/admin/duis-sit-amet-facilisis-sapien-2', 1, '2021-11-15 18:46:44', '2021-11-15 18:46:44', '6cdce2b7-9f14-4aac-91d0-b0f18a8c532c'),
(60, 60, 1, 'duis-sit-amet-facilisis-sapien-2', 'articles/admin/duis-sit-amet-facilisis-sapien-2', 1, '2021-11-15 18:46:44', '2021-11-15 18:46:44', '9a94d79b-b99d-4057-add7-6e87479cd3e1'),
(61, 61, 1, 'aenean-faucibus-mollis-dui-malesuada-commodo-2', 'articles/admin/aenean-faucibus-mollis-dui-malesuada-commodo-2', 1, '2021-11-15 18:46:44', '2021-11-15 18:46:44', '7406c3e4-af0f-47be-b871-72ea3fb6bff0'),
(62, 62, 1, 'aenean-faucibus-mollis-dui-malesuada-commodo-2', 'articles/admin/aenean-faucibus-mollis-dui-malesuada-commodo-2', 1, '2021-11-15 18:46:44', '2021-11-15 18:46:44', '271763a8-ddc7-43d1-a0b0-574c3c117fd9'),
(63, 63, 1, 'in-viverra-est-et-pretium-finibus-2', 'articles/admin/in-viverra-est-et-pretium-finibus-2', 1, '2021-11-15 18:46:45', '2021-11-15 18:46:45', '466ce6c8-c34f-42d8-b0f8-187512471a66'),
(64, 64, 1, 'in-viverra-est-et-pretium-finibus-2', 'articles/admin/in-viverra-est-et-pretium-finibus-2', 1, '2021-11-15 18:46:45', '2021-11-15 18:46:45', 'af5b83e3-27db-4a11-bd5e-2b138a135397'),
(65, 65, 1, 'mauris-vel-odio-dapibus-metus-ornare-dignissim', 'articles/admin/mauris-vel-odio-dapibus-metus-ornare-dignissim', 1, '2021-11-15 18:50:07', '2021-11-15 18:51:18', '20d0d245-d352-43bf-b8f3-9be52d2a3914'),
(66, 66, 1, NULL, NULL, 1, '2021-11-15 18:50:34', '2021-11-15 18:50:34', '3873c84e-2fe4-457a-b4ad-3375ce85f39d'),
(67, 67, 1, NULL, NULL, 1, '2021-11-15 18:50:35', '2021-11-15 18:50:35', '587d500f-43bc-48ca-ba2a-a3a29fe294de'),
(68, 68, 1, NULL, NULL, 1, '2021-11-15 18:50:37', '2021-11-15 18:50:37', '77cafee8-d487-4043-b113-e3f4106c560d'),
(69, 69, 1, 'nature', NULL, 1, '2021-11-15 18:51:06', '2021-11-15 18:51:06', 'dd15354b-93f2-47e8-9055-b229943a1e29'),
(70, 70, 1, 'mauris-vel-odio-dapibus-metus-ornare-dignissim', 'articles/admin/mauris-vel-odio-dapibus-metus-ornare-dignissim', 1, '2021-11-15 18:51:18', '2021-11-15 18:51:18', 'c657f6d6-6354-4679-97e4-cdb6ae080fd8'),
(71, 71, 1, 'aenean-vel-mauris-odio', 'articles/admin/aenean-vel-mauris-odio', 1, '2021-11-15 18:51:20', '2021-11-15 18:52:00', '35a0d0c3-2581-48df-974e-1fe30d8ebeaf'),
(72, 72, 1, 'season', NULL, 1, '2021-11-15 18:51:51', '2021-11-15 18:51:51', 'a17fffb4-a194-4abc-a131-ae564c8e5303'),
(73, 73, 1, 'aenean-vel-mauris-odio', 'articles/admin/aenean-vel-mauris-odio', 1, '2021-11-15 18:52:00', '2021-11-15 18:52:00', 'ac9c0c78-6cf6-47d8-a37d-b96e9b31e9e8'),
(74, 74, 1, 'phasellus-ullamcorper-pretium-volutpat', 'articles/admin/phasellus-ullamcorper-pretium-volutpat', 1, '2021-11-15 18:52:02', '2021-11-15 18:52:40', '6530fc1c-b291-4f62-b275-a377b930efa7'),
(75, 75, 1, 'mountain', NULL, 1, '2021-11-15 18:52:27', '2021-11-15 18:52:27', '3d0a77d8-0855-44f1-8da6-280b1a08c7c7'),
(76, 76, 1, 'phasellus-ullamcorper-pretium-volutpat', 'articles/admin/phasellus-ullamcorper-pretium-volutpat', 1, '2021-11-15 18:52:40', '2021-11-15 18:52:40', 'c19d3851-78ee-48f5-9661-5e615a25552a'),
(77, 77, 1, 'mauris-vel-odio-dapibus-metus-ornare-dignissim-2', 'articles/admin/mauris-vel-odio-dapibus-metus-ornare-dignissim-2', 1, '2021-11-15 18:53:38', '2021-11-15 18:53:38', '5f8551e5-1eac-4cec-9c09-661276ee4d26'),
(78, 78, 1, 'mauris-vel-odio-dapibus-metus-ornare-dignissim-2', 'articles/admin/mauris-vel-odio-dapibus-metus-ornare-dignissim-2', 1, '2021-11-15 18:53:38', '2021-11-15 18:53:38', 'b3345e2d-8f0a-468e-83de-4194aff1bfba'),
(79, 79, 1, 'aenean-vel-mauris-odio-2', 'articles/admin/aenean-vel-mauris-odio-2', 1, '2021-11-15 18:53:39', '2021-11-15 18:53:39', 'b6b5246d-a9c1-4c61-ac20-09b353f5174f'),
(80, 80, 1, 'aenean-vel-mauris-odio-2', 'articles/admin/aenean-vel-mauris-odio-2', 1, '2021-11-15 18:53:39', '2021-11-15 18:53:39', '3fd0a9d2-4673-47e7-84e8-fdaaa8e39f8f'),
(81, 81, 1, 'phasellus-ullamcorper-pretium-volutpat-2', 'articles/admin/phasellus-ullamcorper-pretium-volutpat-2', 1, '2021-11-15 18:53:39', '2021-11-15 18:53:39', '965a279e-72d2-4ddc-a3e6-43986be776b5'),
(82, 82, 1, 'phasellus-ullamcorper-pretium-volutpat-2', 'articles/admin/phasellus-ullamcorper-pretium-volutpat-2', 1, '2021-11-15 18:53:39', '2021-11-15 18:53:39', 'c2ce8a71-3bbc-4ed1-b69c-a6ab2edfcdd8');

-- --------------------------------------------------------

--
-- Table structure for table `blg_entries`
--

CREATE TABLE `blg_entries` (
  `id` int NOT NULL,
  `sectionId` int NOT NULL,
  `parentId` int DEFAULT NULL,
  `typeId` int NOT NULL,
  `authorId` int DEFAULT NULL,
  `postDate` datetime DEFAULT NULL,
  `expiryDate` datetime DEFAULT NULL,
  `deletedWithEntryType` tinyint(1) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `blg_entries`
--

INSERT INTO `blg_entries` (`id`, `sectionId`, `parentId`, `typeId`, `authorId`, `postDate`, `expiryDate`, `deletedWithEntryType`, `dateCreated`, `dateUpdated`, `uid`) VALUES
(2, 1, NULL, 1, 1, '2021-11-15 01:06:00', NULL, 0, '2021-11-15 01:06:17', '2021-11-15 01:07:36', 'f5de77bb-3e9b-49e3-86ec-6cc1dfa76253'),
(3, 1, NULL, 1, NULL, '2021-11-15 01:06:00', NULL, NULL, '2021-11-15 01:06:18', '2021-11-15 01:06:18', 'b97d8b87-a96f-4137-b116-ee26aa722ff6'),
(4, 1, NULL, 1, NULL, '2021-11-15 01:06:00', NULL, NULL, '2021-11-15 01:06:18', '2021-11-15 01:06:18', '602b2091-89e5-4829-a359-775934be4935'),
(5, 1, NULL, 1, NULL, '2021-11-15 01:06:00', NULL, NULL, '2021-11-15 01:06:42', '2021-11-15 01:06:42', 'b02bf171-6cdf-4b62-8c1c-420a9f6b496b'),
(6, 1, NULL, 1, NULL, '2021-11-15 01:06:00', NULL, NULL, '2021-11-15 01:06:43', '2021-11-15 01:06:43', 'a9b3c1b7-62d3-40c3-9ee6-8fcb8b4a07a2'),
(11, 1, NULL, 1, 1, '2021-11-15 01:06:00', NULL, NULL, '2021-11-15 03:28:50', '2021-11-15 03:28:50', '5f11c717-7fc1-4af0-a539-a44f30cd19fc'),
(12, 1, NULL, 1, 1, '2021-11-15 01:06:00', NULL, NULL, '2021-11-15 03:33:52', '2021-11-15 03:33:52', 'da803afe-16e3-4cd6-a54c-0bacd70db738'),
(13, 1, NULL, 1, 1, '2021-11-15 03:35:00', NULL, NULL, '2021-11-15 03:35:46', '2021-11-15 03:35:46', '42a570b6-39b8-46f9-a84a-fca84de5461e'),
(14, 1, NULL, 1, 1, '2021-11-15 03:39:00', NULL, NULL, '2021-11-15 03:39:50', '2021-11-15 03:39:50', '896c056f-f984-41a9-a605-ef17401c4bbe'),
(15, 1, NULL, 1, 1, '2021-11-15 03:39:00', NULL, NULL, '2021-11-15 03:41:07', '2021-11-15 03:41:07', '632a0805-2600-4520-88c4-12332efd75c5'),
(16, 1, NULL, 1, 1, '2021-11-15 03:39:00', NULL, NULL, '2021-11-15 03:42:07', '2021-11-15 03:42:07', 'ccd762ee-ec25-4c5d-8fc2-f0c9a8c2048d'),
(18, 1, NULL, 1, 1, '2021-11-15 03:42:00', NULL, NULL, '2021-11-15 03:42:24', '2021-11-15 03:42:24', '377ac213-92a5-4981-b3ef-49f0ebe41168'),
(28, 1, NULL, 1, 1, '2021-11-15 03:42:00', NULL, NULL, '2021-11-15 03:45:35', '2021-11-15 03:45:35', '22b2a3ec-01b1-4949-8ca7-8974fa5d9ee3'),
(30, 1, NULL, 1, 1, '2021-11-15 03:42:00', NULL, NULL, '2021-11-15 03:52:08', '2021-11-15 03:52:08', 'b619397b-9d3f-4dae-b228-8dcccbdc9054'),
(31, 1, NULL, 1, 1, '2021-11-15 03:39:00', NULL, NULL, '2021-11-15 03:52:36', '2021-11-15 03:52:36', '7460d990-dfa4-476d-8acf-7abb07b8ac53'),
(33, 1, NULL, 1, 1, '2021-11-15 03:42:00', NULL, NULL, '2021-11-15 03:52:42', '2021-11-15 03:52:42', 'aeeee266-c67f-41fd-b640-9d696f31aaae'),
(35, 1, NULL, 1, 1, '2021-11-15 03:42:00', NULL, NULL, '2021-11-15 03:53:02', '2021-11-15 03:53:02', 'a6c21592-c1a0-46bf-a973-86b7e791236c'),
(38, 1, NULL, 1, 1, '2021-11-15 03:39:00', NULL, NULL, '2021-11-15 18:16:35', '2021-11-15 18:16:35', '4b8eaaee-6ede-4159-9085-652527507392'),
(40, 1, NULL, 1, 1, '2021-11-15 03:42:00', NULL, NULL, '2021-11-15 18:16:52', '2021-11-15 18:16:52', '5d3cab38-50f4-430d-9e48-bb698ba949a7'),
(41, 1, NULL, 1, 1, '2021-11-15 18:24:00', NULL, NULL, '2021-11-15 18:24:54', '2021-11-15 18:24:54', '7400066a-5913-4122-9275-250169dd3737'),
(42, 1, NULL, 1, 1, '2021-11-15 18:24:00', NULL, NULL, '2021-11-15 18:34:52', '2021-11-15 18:34:52', 'bd4aebdd-8b69-49f8-9afd-3919b8a72d8b'),
(44, 1, NULL, 1, 1, '2021-11-15 18:24:00', NULL, NULL, '2021-11-15 18:35:38', '2021-11-15 18:35:38', '36e4d2da-b7d3-40c8-9bf7-bacafe95c4c3'),
(45, 1, NULL, 1, 1, '2021-11-15 18:39:00', NULL, NULL, '2021-11-15 18:39:00', '2021-11-15 18:39:00', '79fefc60-a5e2-4b74-9cfd-78726b7d51a9'),
(46, 1, NULL, 1, 1, '2021-11-15 18:39:00', NULL, NULL, '2021-11-15 18:39:53', '2021-11-15 18:39:53', '7a290af6-788f-49a5-9946-59d9111ad207'),
(47, 1, NULL, 1, 1, '2021-11-15 18:41:00', NULL, NULL, '2021-11-15 18:41:22', '2021-11-15 18:41:22', 'a98e3c28-8dca-495e-9a44-3865459afe41'),
(48, 1, NULL, 1, 1, '2021-11-15 18:41:00', NULL, NULL, '2021-11-15 18:41:55', '2021-11-15 18:41:55', 'c3e75f83-d75a-479b-a49b-a6dc915503b0'),
(49, 1, NULL, 1, 1, '2021-11-15 18:42:00', NULL, NULL, '2021-11-15 18:42:21', '2021-11-15 18:42:21', 'd0d38bfa-3d6d-4ec4-8c6a-2eaf6f1a6d47'),
(50, 1, NULL, 1, 1, '2021-11-15 18:42:00', NULL, NULL, '2021-11-15 18:43:00', '2021-11-15 18:43:00', 'b9e8b0cc-a1cd-40fe-8d1b-2819e91593a3'),
(51, 1, NULL, 1, 1, '2021-11-15 18:43:00', NULL, NULL, '2021-11-15 18:43:02', '2021-11-15 18:43:02', '9c00528c-77fe-4cac-8f24-7ad075aba317'),
(52, 1, NULL, 1, 1, '2021-11-15 18:43:00', NULL, NULL, '2021-11-15 18:43:39', '2021-11-15 18:43:39', '2eff3ef6-e9ff-45d1-863d-e0c3b5d929ca'),
(53, 1, NULL, 1, 1, '2021-11-15 18:44:00', NULL, NULL, '2021-11-15 18:44:09', '2021-11-15 18:44:09', 'a156e889-8efe-4434-b6b1-15462f34aea3'),
(54, 1, NULL, 1, 1, '2021-11-15 18:44:00', NULL, NULL, '2021-11-15 18:44:47', '2021-11-15 18:44:47', '7a9c932c-858e-4d2d-a664-eb8ded384ac0'),
(55, 1, NULL, 1, 1, '2021-11-15 18:44:00', NULL, NULL, '2021-11-15 18:44:49', '2021-11-15 18:44:49', 'a0f82de0-19d2-4411-b9dd-50b3f0b11246'),
(56, 1, NULL, 1, 1, '2021-11-15 18:44:00', NULL, NULL, '2021-11-15 18:45:24', '2021-11-15 18:45:24', 'dac3acee-e4bc-4533-8835-1caf78d67d26'),
(57, 1, NULL, 1, 1, '2021-11-15 18:45:00', NULL, NULL, '2021-11-15 18:45:58', '2021-11-15 18:45:58', '594bf1ae-596e-4fbd-b7d2-003631251bce'),
(58, 1, NULL, 1, 1, '2021-11-15 18:45:00', NULL, NULL, '2021-11-15 18:46:32', '2021-11-15 18:46:32', 'dbe5e6a5-bec8-4cc8-96be-ec69fa7d311f'),
(59, 1, NULL, 1, 1, '2021-11-15 18:44:00', NULL, NULL, '2021-11-15 18:46:44', '2021-11-15 18:46:44', '6b07f608-8ac0-451a-a94a-0498192913ac'),
(60, 1, NULL, 1, 1, '2021-11-15 18:44:00', NULL, NULL, '2021-11-15 18:46:44', '2021-11-15 18:46:44', '30238c91-63e9-4ce1-8997-7cbc096dfad8'),
(61, 1, NULL, 1, 1, '2021-11-15 18:44:00', NULL, NULL, '2021-11-15 18:46:44', '2021-11-15 18:46:44', 'c94af87b-1e36-4858-9ad7-578d825be399'),
(62, 1, NULL, 1, 1, '2021-11-15 18:44:00', NULL, NULL, '2021-11-15 18:46:44', '2021-11-15 18:46:44', 'fb6834b1-ab58-42c2-ae8b-1bf31c535b50'),
(63, 1, NULL, 1, 1, '2021-11-15 18:45:00', NULL, NULL, '2021-11-15 18:46:45', '2021-11-15 18:46:45', 'ed9bcb7b-9fcf-44a8-8c83-bc52a8b7b2a8'),
(64, 1, NULL, 1, 1, '2021-11-15 18:45:00', NULL, NULL, '2021-11-15 18:46:45', '2021-11-15 18:46:45', '227dd49d-f719-4eea-a946-4d6072ebbbcc'),
(65, 1, NULL, 1, 1, '2021-11-15 18:50:00', NULL, NULL, '2021-11-15 18:50:07', '2021-11-15 18:50:07', '620f1eb7-00ca-498d-993e-f0ee8ef84a7d'),
(70, 1, NULL, 1, 1, '2021-11-15 18:50:00', NULL, NULL, '2021-11-15 18:51:18', '2021-11-15 18:51:18', '4317e006-363a-437a-b154-3cb3aff0b76a'),
(71, 1, NULL, 1, 1, '2021-11-15 18:51:00', NULL, NULL, '2021-11-15 18:51:20', '2021-11-15 18:51:20', '2950787d-3e0f-4f0b-bdb6-d7647df37757'),
(73, 1, NULL, 1, 1, '2021-11-15 18:51:00', NULL, NULL, '2021-11-15 18:52:00', '2021-11-15 18:52:00', '7eaa1cb9-11aa-4cf4-a520-d0e81abb5091'),
(74, 1, NULL, 1, 1, '2021-11-15 18:52:00', NULL, NULL, '2021-11-15 18:52:02', '2021-11-15 18:52:02', '8de155fe-6b25-4d43-a466-6e2c47403c29'),
(76, 1, NULL, 1, 1, '2021-11-15 18:52:00', NULL, NULL, '2021-11-15 18:52:40', '2021-11-15 18:52:40', 'cb48ce63-91aa-471c-aad7-6694a7dc53fb'),
(77, 1, NULL, 1, 1, '2021-11-15 18:50:00', NULL, NULL, '2021-11-15 18:53:38', '2021-11-15 18:53:38', '13d35fcc-837e-463c-9b88-871fec39369f'),
(78, 1, NULL, 1, 1, '2021-11-15 18:50:00', NULL, NULL, '2021-11-15 18:53:38', '2021-11-15 18:53:38', '94ee9162-efec-441c-ad5a-efe826f868d9'),
(79, 1, NULL, 1, 1, '2021-11-15 18:51:00', NULL, NULL, '2021-11-15 18:53:39', '2021-11-15 18:53:39', '45bcd5d2-0b16-4f47-81a4-b96d922126c4'),
(80, 1, NULL, 1, 1, '2021-11-15 18:51:00', NULL, NULL, '2021-11-15 18:53:39', '2021-11-15 18:53:39', 'f5a38147-ce22-4833-9089-3f9b874d6af2'),
(81, 1, NULL, 1, 1, '2021-11-15 18:52:00', NULL, NULL, '2021-11-15 18:53:39', '2021-11-15 18:53:39', '235b7263-9c04-4bd8-989f-f250231aa49a'),
(82, 1, NULL, 1, 1, '2021-11-15 18:52:00', NULL, NULL, '2021-11-15 18:53:39', '2021-11-15 18:53:39', '34e3f312-ce2a-4285-904a-0ee32e023077');

-- --------------------------------------------------------

--
-- Table structure for table `blg_entrytypes`
--

CREATE TABLE `blg_entrytypes` (
  `id` int NOT NULL,
  `sectionId` int NOT NULL,
  `fieldLayoutId` int DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `handle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `hasTitleField` tinyint(1) NOT NULL DEFAULT '1',
  `titleTranslationMethod` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'site',
  `titleTranslationKeyFormat` text COLLATE utf8_unicode_ci,
  `titleFormat` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sortOrder` smallint UNSIGNED DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `dateDeleted` datetime DEFAULT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `blg_entrytypes`
--

INSERT INTO `blg_entrytypes` (`id`, `sectionId`, `fieldLayoutId`, `name`, `handle`, `hasTitleField`, `titleTranslationMethod`, `titleTranslationKeyFormat`, `titleFormat`, `sortOrder`, `dateCreated`, `dateUpdated`, `dateDeleted`, `uid`) VALUES
(1, 1, 1, 'Articles', 'articles', 0, 'site', NULL, '{object.articleTitle|raw}', 1, '2021-11-15 01:06:17', '2021-11-15 03:51:31', NULL, 'ed9613bd-7583-4119-bfca-b7024788cf9d');

-- --------------------------------------------------------

--
-- Table structure for table `blg_fieldgroups`
--

CREATE TABLE `blg_fieldgroups` (
  `id` int NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `dateDeleted` datetime DEFAULT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `blg_fieldgroups`
--

INSERT INTO `blg_fieldgroups` (`id`, `name`, `dateCreated`, `dateUpdated`, `dateDeleted`, `uid`) VALUES
(1, 'Common', '2021-11-14 23:59:05', '2021-11-14 23:59:05', NULL, '54c2020e-34dd-4a49-a26c-02ad919637a9'),
(2, 'Articles', '2021-11-15 02:51:26', '2021-11-15 02:51:26', NULL, '4829e301-dddf-4d10-9dc9-a3161fcd74dc');

-- --------------------------------------------------------

--
-- Table structure for table `blg_fieldlayoutfields`
--

CREATE TABLE `blg_fieldlayoutfields` (
  `id` int NOT NULL,
  `layoutId` int NOT NULL,
  `tabId` int NOT NULL,
  `fieldId` int NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `sortOrder` smallint UNSIGNED DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `blg_fieldlayoutfields`
--

INSERT INTO `blg_fieldlayoutfields` (`id`, `layoutId`, `tabId`, `fieldId`, `required`, `sortOrder`, `dateCreated`, `dateUpdated`, `uid`) VALUES
(44, 1, 12, 8, 1, 1, '2021-11-15 18:16:09', '2021-11-15 18:16:09', 'a185e281-9bab-40d0-b4f3-97f3dc45b02b'),
(45, 1, 12, 1, 1, 2, '2021-11-15 18:16:09', '2021-11-15 18:16:09', 'f3d7991d-29c1-49b0-b856-58472b2cac87'),
(46, 1, 12, 2, 0, 3, '2021-11-15 18:16:09', '2021-11-15 18:16:09', '9335e8ae-4495-40be-8f17-e9abc9c81acd'),
(47, 1, 12, 3, 0, 4, '2021-11-15 18:16:09', '2021-11-15 18:16:09', '5bda931b-54a3-4f23-9932-ec1fe29f9b7f'),
(48, 1, 12, 7, 0, 5, '2021-11-15 18:16:09', '2021-11-15 18:16:09', '2c0b14f4-3738-4655-b5a7-474a6ef037d8'),
(49, 1, 12, 5, 0, 6, '2021-11-15 18:16:09', '2021-11-15 18:16:09', '19c49347-bc9c-4c7b-86c4-84edb5e1c049'),
(50, 1, 12, 4, 0, 7, '2021-11-15 18:16:09', '2021-11-15 18:16:09', '4fcaae79-a529-449b-8772-7263deb6a943');

-- --------------------------------------------------------

--
-- Table structure for table `blg_fieldlayouts`
--

CREATE TABLE `blg_fieldlayouts` (
  `id` int NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `dateDeleted` datetime DEFAULT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `blg_fieldlayouts`
--

INSERT INTO `blg_fieldlayouts` (`id`, `type`, `dateCreated`, `dateUpdated`, `dateDeleted`, `uid`) VALUES
(1, 'craft\\elements\\Entry', '2021-11-15 01:06:17', '2021-11-15 01:06:17', NULL, '519aa450-ab54-4f79-ac62-c3e17c2e5329'),
(2, 'craft\\elements\\Asset', '2021-11-15 03:22:44', '2021-11-15 03:22:44', NULL, 'ef04c9bf-d2da-4b35-aa69-d8b16f2bb443'),
(3, 'craft\\elements\\Tag', '2021-11-15 03:25:36', '2021-11-15 03:25:36', NULL, '66fb93af-fcea-4881-8438-f3251a78c7f5');

-- --------------------------------------------------------

--
-- Table structure for table `blg_fieldlayouttabs`
--

CREATE TABLE `blg_fieldlayouttabs` (
  `id` int NOT NULL,
  `layoutId` int NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `elements` text COLLATE utf8_unicode_ci,
  `sortOrder` smallint UNSIGNED DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `blg_fieldlayouttabs`
--

INSERT INTO `blg_fieldlayouttabs` (`id`, `layoutId`, `name`, `elements`, `sortOrder`, `dateCreated`, `dateUpdated`, `uid`) VALUES
(5, 2, 'Content', '[{\"type\":\"craft\\\\fieldlayoutelements\\\\AssetTitleField\",\"autocomplete\":false,\"class\":null,\"size\":null,\"name\":null,\"autocorrect\":true,\"autocapitalize\":true,\"disabled\":false,\"readonly\":false,\"title\":null,\"placeholder\":null,\"step\":null,\"min\":null,\"max\":null,\"requirable\":false,\"id\":null,\"containerAttributes\":[],\"inputContainerAttributes\":[],\"labelAttributes\":[],\"orientation\":null,\"label\":null,\"instructions\":null,\"tip\":null,\"warning\":null,\"width\":100}]', 1, '2021-11-15 03:22:44', '2021-11-15 03:22:44', 'e89299bf-2d6e-4f1e-a98a-dc94840e9dad'),
(6, 3, 'Content', '[{\"type\":\"craft\\\\fieldlayoutelements\\\\TitleField\",\"autocomplete\":false,\"class\":null,\"size\":null,\"name\":null,\"autocorrect\":true,\"autocapitalize\":true,\"disabled\":false,\"readonly\":false,\"title\":null,\"placeholder\":null,\"step\":null,\"min\":null,\"max\":null,\"requirable\":false,\"id\":null,\"containerAttributes\":[],\"inputContainerAttributes\":[],\"labelAttributes\":[],\"orientation\":null,\"label\":\"Tag Name\",\"instructions\":\"\",\"tip\":null,\"warning\":null,\"width\":100}]', 1, '2021-11-15 03:25:36', '2021-11-15 03:25:36', '2c12db17-b68e-432b-9f86-ca7aea29bf76'),
(12, 1, 'Content', '[{\"type\":\"craft\\\\fieldlayoutelements\\\\EntryTitleField\",\"autocomplete\":false,\"class\":null,\"size\":null,\"name\":null,\"autocorrect\":true,\"autocapitalize\":true,\"disabled\":false,\"readonly\":false,\"title\":null,\"placeholder\":null,\"step\":null,\"min\":null,\"max\":null,\"requirable\":false,\"id\":null,\"containerAttributes\":[],\"inputContainerAttributes\":[],\"labelAttributes\":[],\"orientation\":null,\"label\":\"\",\"instructions\":\"\",\"tip\":null,\"warning\":null,\"width\":100},{\"type\":\"craft\\\\fieldlayoutelements\\\\CustomField\",\"label\":\"Title\",\"instructions\":\"\",\"tip\":null,\"warning\":null,\"required\":\"1\",\"width\":100,\"fieldUid\":\"a9263f26-bca2-4ef1-b137-70ca86db0b2b\"},{\"type\":\"craft\\\\fieldlayoutelements\\\\CustomField\",\"label\":\"\",\"instructions\":\"\",\"tip\":null,\"warning\":null,\"required\":\"1\",\"width\":100,\"fieldUid\":\"0e36f6df-114e-4c36-996f-183787e38f90\"},{\"type\":\"craft\\\\fieldlayoutelements\\\\CustomField\",\"label\":null,\"instructions\":null,\"tip\":null,\"warning\":null,\"required\":false,\"width\":100,\"fieldUid\":\"313863d8-5395-4fa8-9bad-eee5018c0ae9\"},{\"type\":\"craft\\\\fieldlayoutelements\\\\CustomField\",\"label\":null,\"instructions\":null,\"tip\":null,\"warning\":null,\"required\":false,\"width\":100,\"fieldUid\":\"245f7576-c80c-414b-bc21-6ad15807d7a2\"},{\"type\":\"craft\\\\fieldlayoutelements\\\\CustomField\",\"label\":\"Set as featured post?\",\"instructions\":\"\",\"tip\":null,\"warning\":null,\"required\":\"\",\"width\":100,\"fieldUid\":\"a67d916f-adcc-4c77-ab98-3c4f66ad26e8\"},{\"type\":\"craft\\\\fieldlayoutelements\\\\CustomField\",\"label\":\"\",\"instructions\":\"\",\"tip\":null,\"warning\":null,\"required\":\"\",\"width\":100,\"fieldUid\":\"485bf76e-a338-41b7-b86c-9000a6c4ffcc\"},{\"type\":\"craft\\\\fieldlayoutelements\\\\CustomField\",\"label\":\"\",\"instructions\":\"\",\"tip\":null,\"warning\":null,\"required\":\"\",\"width\":100,\"fieldUid\":\"5a9d08f1-24ab-431e-bbed-f882d42ee70e\"}]', 1, '2021-11-15 18:16:09', '2021-11-15 18:16:09', '709e7137-7ded-4d7c-bdfd-462875660d20');

-- --------------------------------------------------------

--
-- Table structure for table `blg_fields`
--

CREATE TABLE `blg_fields` (
  `id` int NOT NULL,
  `groupId` int DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `handle` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `context` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'global',
  `columnSuffix` char(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `instructions` text COLLATE utf8_unicode_ci,
  `searchable` tinyint(1) NOT NULL DEFAULT '1',
  `translationMethod` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'none',
  `translationKeyFormat` text COLLATE utf8_unicode_ci,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `settings` text COLLATE utf8_unicode_ci,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `blg_fields`
--

INSERT INTO `blg_fields` (`id`, `groupId`, `name`, `handle`, `context`, `columnSuffix`, `instructions`, `searchable`, `translationMethod`, `translationKeyFormat`, `type`, `settings`, `dateCreated`, `dateUpdated`, `uid`) VALUES
(1, 2, 'Excerpt', 'excerpt', 'global', 'oebprhdp', '', 0, 'none', NULL, 'craft\\fields\\PlainText', '{\"byteLimit\":null,\"charLimit\":null,\"code\":\"\",\"columnType\":\"text\",\"initialRows\":\"4\",\"multiline\":\"1\",\"placeholder\":null,\"uiMode\":\"enlarged\"}', '2021-11-15 02:53:31', '2021-11-15 02:58:17', '0e36f6df-114e-4c36-996f-183787e38f90'),
(2, 2, 'Image', 'image', 'global', NULL, '', 0, 'site', NULL, 'craft\\fields\\Assets', '{\"allowSelfRelations\":false,\"allowUploads\":true,\"allowedKinds\":[\"image\"],\"defaultUploadLocationSource\":\"volume:f38e29ae-f631-4ce5-a112-5d7cd773e4fa\",\"defaultUploadLocationSubpath\":\"\",\"limit\":\"1\",\"localizeRelations\":false,\"previewMode\":\"full\",\"restrictFiles\":\"1\",\"selectionLabel\":\"\",\"showSiteMenu\":true,\"showUnpermittedFiles\":false,\"showUnpermittedVolumes\":false,\"singleUploadLocationSource\":\"volume:f38e29ae-f631-4ce5-a112-5d7cd773e4fa\",\"singleUploadLocationSubpath\":\"\",\"source\":null,\"sources\":\"*\",\"targetSiteId\":null,\"useSingleFolder\":false,\"validateRelatedElements\":false,\"viewMode\":\"large\"}', '2021-11-15 02:54:52', '2021-11-15 03:24:07', '313863d8-5395-4fa8-9bad-eee5018c0ae9'),
(3, 2, 'Tag', 'tag', 'global', NULL, '', 0, 'site', NULL, 'craft\\fields\\Tags', '{\"allowLimit\":false,\"allowMultipleSources\":false,\"allowSelfRelations\":false,\"limit\":null,\"localizeRelations\":false,\"selectionLabel\":\"\",\"showSiteMenu\":true,\"source\":\"taggroup:4335f424-4db1-4fc1-b173-3a3d2cad684e\",\"sources\":\"*\",\"targetSiteId\":null,\"validateRelatedElements\":false,\"viewMode\":null}', '2021-11-15 02:55:55', '2021-11-15 03:26:00', '245f7576-c80c-414b-bc21-6ad15807d7a2'),
(4, 2, 'Views', 'views', 'global', 'ccmcxfrr', '', 0, 'none', NULL, 'craft\\fields\\Number', '{\"decimals\":0,\"defaultValue\":null,\"max\":null,\"min\":\"0\",\"prefix\":null,\"previewCurrency\":\"\",\"previewFormat\":\"decimal\",\"size\":null,\"suffix\":null}', '2021-11-15 02:56:40', '2021-11-15 02:56:40', '5a9d08f1-24ab-431e-bbed-f882d42ee70e'),
(5, 2, 'Likes', 'likes', 'global', 'mksqzuki', '', 0, 'none', NULL, 'craft\\fields\\Number', '{\"decimals\":0,\"defaultValue\":null,\"max\":null,\"min\":\"0\",\"prefix\":null,\"previewCurrency\":\"\",\"previewFormat\":\"decimal\",\"size\":null,\"suffix\":null}', '2021-11-15 02:57:09', '2021-11-15 02:57:09', '485bf76e-a338-41b7-b86c-9000a6c4ffcc'),
(7, 2, 'Featured', 'featured', 'global', 'qjffftaa', '', 0, 'none', NULL, 'craft\\fields\\Lightswitch', '{\"default\":false,\"offLabel\":null,\"onLabel\":null}', '2021-11-15 03:30:23', '2021-11-15 18:15:26', 'a67d916f-adcc-4c77-ab98-3c4f66ad26e8'),
(8, 2, 'Article Title', 'articleTitle', 'global', 'gaxmcqla', '', 1, 'none', NULL, 'craft\\fields\\PlainText', '{\"byteLimit\":null,\"charLimit\":null,\"code\":\"\",\"columnType\":\"string\",\"initialRows\":\"4\",\"multiline\":\"\",\"placeholder\":null,\"uiMode\":\"normal\"}', '2021-11-15 03:38:38', '2021-11-15 03:38:38', 'a9263f26-bca2-4ef1-b137-70ca86db0b2b');

-- --------------------------------------------------------

--
-- Table structure for table `blg_globalsets`
--

CREATE TABLE `blg_globalsets` (
  `id` int NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `handle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fieldLayoutId` int DEFAULT NULL,
  `sortOrder` smallint UNSIGNED DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `blg_gqlschemas`
--

CREATE TABLE `blg_gqlschemas` (
  `id` int NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `scope` text COLLATE utf8_unicode_ci,
  `isPublic` tinyint(1) NOT NULL DEFAULT '0',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `blg_gqlschemas`
--

INSERT INTO `blg_gqlschemas` (`id`, `name`, `scope`, `isPublic`, `dateCreated`, `dateUpdated`, `uid`) VALUES
(1, 'Public Schema', '[\"sections.7f3a3d16-e578-421a-991a-91842208b384:read\"]', 1, '2021-11-15 04:39:24', '2021-11-15 13:05:03', '54051863-8de7-40aa-ab6b-4c40ca533c78'),
(2, 'articles', '[\"sections.7f3a3d16-e578-421a-991a-91842208b384:read\",\"entrytypes.ed9613bd-7583-4119-bfca-b7024788cf9d:read\",\"volumes.f38e29ae-f631-4ce5-a112-5d7cd773e4fa:read\",\"usergroups.everyone:read\",\"taggroups.4335f424-4db1-4fc1-b173-3a3d2cad684e:read\"]', 0, '2021-11-15 13:06:16', '2021-11-15 18:23:13', 'db041b37-83e1-4382-8091-ffc915fef807');

-- --------------------------------------------------------

--
-- Table structure for table `blg_gqltokens`
--

CREATE TABLE `blg_gqltokens` (
  `id` int NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `accessToken` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `expiryDate` datetime DEFAULT NULL,
  `lastUsed` datetime DEFAULT NULL,
  `schemaId` int DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `blg_gqltokens`
--

INSERT INTO `blg_gqltokens` (`id`, `name`, `accessToken`, `enabled`, `expiryDate`, `lastUsed`, `schemaId`, `dateCreated`, `dateUpdated`, `uid`) VALUES
(1, 'Public Token', '__PUBLIC__', 1, NULL, NULL, 1, '2021-11-15 04:39:25', '2021-11-15 04:39:25', '1b4625a5-9468-4a40-bb9f-f8a3fed1553d'),
(2, 'blog-front-local', '42t42yQns-pEuxIBnuBhyIJK24I6K849', 1, NULL, NULL, 2, '2021-11-15 13:06:58', '2021-11-15 13:06:58', 'ab8e33b1-d410-4946-a25e-ff466721171d');

-- --------------------------------------------------------

--
-- Table structure for table `blg_info`
--

CREATE TABLE `blg_info` (
  `id` int NOT NULL,
  `version` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `schemaVersion` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `maintenance` tinyint(1) NOT NULL DEFAULT '0',
  `configVersion` char(12) COLLATE utf8_unicode_ci NOT NULL DEFAULT '000000000000',
  `fieldVersion` char(12) COLLATE utf8_unicode_ci NOT NULL DEFAULT '000000000000',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `blg_info`
--

INSERT INTO `blg_info` (`id`, `version`, `schemaVersion`, `maintenance`, `configVersion`, `fieldVersion`, `dateCreated`, `dateUpdated`, `uid`) VALUES
(1, '3.7.20', '3.7.8', 0, 'niymgwyndurx', 'ncvxfmkwvaih', '2021-11-14 23:59:05', '2021-11-15 18:49:28', 'd3c51393-8821-4258-8ba7-bef3391e29a4');

-- --------------------------------------------------------

--
-- Table structure for table `blg_matrixblocks`
--

CREATE TABLE `blg_matrixblocks` (
  `id` int NOT NULL,
  `ownerId` int NOT NULL,
  `fieldId` int NOT NULL,
  `typeId` int NOT NULL,
  `sortOrder` smallint UNSIGNED DEFAULT NULL,
  `deletedWithOwner` tinyint(1) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `blg_matrixblocktypes`
--

CREATE TABLE `blg_matrixblocktypes` (
  `id` int NOT NULL,
  `fieldId` int NOT NULL,
  `fieldLayoutId` int DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `handle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sortOrder` smallint UNSIGNED DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `blg_migrations`
--

CREATE TABLE `blg_migrations` (
  `id` int NOT NULL,
  `track` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `applyTime` datetime NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `blg_migrations`
--

INSERT INTO `blg_migrations` (`id`, `track`, `name`, `applyTime`, `dateCreated`, `dateUpdated`, `uid`) VALUES
(1, 'craft', 'Install', '2021-11-14 23:59:10', '2021-11-14 23:59:10', '2021-11-14 23:59:10', '503659db-912a-433f-ac7a-dbca32ec684c'),
(2, 'craft', 'm150403_183908_migrations_table_changes', '2021-11-14 23:59:11', '2021-11-14 23:59:11', '2021-11-14 23:59:11', '6cbe9874-a38a-48b6-ab76-7cf8b087404c'),
(3, 'craft', 'm150403_184247_plugins_table_changes', '2021-11-14 23:59:11', '2021-11-14 23:59:11', '2021-11-14 23:59:11', 'b8ea3912-47c5-4385-bf14-6d7346a7a6f2'),
(4, 'craft', 'm150403_184533_field_version', '2021-11-14 23:59:11', '2021-11-14 23:59:11', '2021-11-14 23:59:11', 'f1c14394-359f-46b0-beb3-04514bb1f97a'),
(5, 'craft', 'm150403_184729_type_columns', '2021-11-14 23:59:12', '2021-11-14 23:59:12', '2021-11-14 23:59:12', 'dd298ad0-578c-4a04-ae68-aaba5e9056ef'),
(6, 'craft', 'm150403_185142_volumes', '2021-11-14 23:59:12', '2021-11-14 23:59:12', '2021-11-14 23:59:12', 'ddbfd3be-c330-4d5a-b393-04f962e82364'),
(7, 'craft', 'm150428_231346_userpreferences', '2021-11-14 23:59:12', '2021-11-14 23:59:12', '2021-11-14 23:59:12', '2a33a422-abe3-4776-96b5-47e31021a33e'),
(8, 'craft', 'm150519_150900_fieldversion_conversion', '2021-11-14 23:59:12', '2021-11-14 23:59:12', '2021-11-14 23:59:12', 'c217571b-fe3f-42bb-ac14-4fc9dc7c1242'),
(9, 'craft', 'm150617_213829_update_email_settings', '2021-11-14 23:59:13', '2021-11-14 23:59:13', '2021-11-14 23:59:13', 'a9bfdaa4-08a9-4f7b-a0db-5ec0da64bc00'),
(10, 'craft', 'm150721_124739_templatecachequeries', '2021-11-14 23:59:13', '2021-11-14 23:59:13', '2021-11-14 23:59:13', '006e4171-1173-4081-9472-8bb8db14ac8a'),
(11, 'craft', 'm150724_140822_adjust_quality_settings', '2021-11-14 23:59:13', '2021-11-14 23:59:13', '2021-11-14 23:59:13', '52e34125-b362-4ef7-aea2-30e7ba43c9b8'),
(12, 'craft', 'm150815_133521_last_login_attempt_ip', '2021-11-14 23:59:14', '2021-11-14 23:59:14', '2021-11-14 23:59:14', '7d2e5307-7dcd-4036-9cae-8dc07838adec'),
(13, 'craft', 'm151002_095935_volume_cache_settings', '2021-11-14 23:59:14', '2021-11-14 23:59:14', '2021-11-14 23:59:14', '3ad9524c-0493-46cd-9155-8a5f452b46c5'),
(14, 'craft', 'm151005_142750_volume_s3_storage_settings', '2021-11-14 23:59:14', '2021-11-14 23:59:14', '2021-11-14 23:59:14', '5b5e7839-a638-421f-95b2-bdd4fdbe3c42'),
(15, 'craft', 'm151016_133600_delete_asset_thumbnails', '2021-11-14 23:59:15', '2021-11-14 23:59:15', '2021-11-14 23:59:15', 'f22196ec-6272-4cf7-85c5-5263ff4d9a67'),
(16, 'craft', 'm151209_000000_move_logo', '2021-11-14 23:59:15', '2021-11-14 23:59:15', '2021-11-14 23:59:15', '48558829-2cbf-4e0d-8ec0-873b2922deb2'),
(17, 'craft', 'm151211_000000_rename_fileId_to_assetId', '2021-11-14 23:59:15', '2021-11-14 23:59:15', '2021-11-14 23:59:15', '8f8aedc2-bb9f-4cc2-9a1c-ef31a560b433'),
(18, 'craft', 'm151215_000000_rename_asset_permissions', '2021-11-14 23:59:15', '2021-11-14 23:59:15', '2021-11-14 23:59:15', '7a544274-a113-43d2-b406-8f6634949aa4'),
(19, 'craft', 'm160707_000001_rename_richtext_assetsource_setting', '2021-11-14 23:59:16', '2021-11-14 23:59:16', '2021-11-14 23:59:16', 'c052cfaa-2ee2-429c-b978-3378d557d90a'),
(20, 'craft', 'm160708_185142_volume_hasUrls_setting', '2021-11-14 23:59:16', '2021-11-14 23:59:16', '2021-11-14 23:59:16', 'd1aad4f1-970b-4435-a5f0-1c22d1a0ef9f'),
(21, 'craft', 'm160714_000000_increase_max_asset_filesize', '2021-11-14 23:59:16', '2021-11-14 23:59:16', '2021-11-14 23:59:16', 'ba7f3e0c-cbd6-4386-b41b-6b10926f9f79'),
(22, 'craft', 'm160727_194637_column_cleanup', '2021-11-14 23:59:17', '2021-11-14 23:59:17', '2021-11-14 23:59:17', 'b2adbd7b-83e6-41bf-a0e7-12a0a09f6072'),
(23, 'craft', 'm160804_110002_userphotos_to_assets', '2021-11-14 23:59:17', '2021-11-14 23:59:17', '2021-11-14 23:59:17', '98d4a338-7715-4785-809d-c5adbc286d95'),
(24, 'craft', 'm160807_144858_sites', '2021-11-14 23:59:18', '2021-11-14 23:59:18', '2021-11-14 23:59:18', '002ef8e5-996d-4561-bb43-d277cbbf6d62'),
(25, 'craft', 'm160829_000000_pending_user_content_cleanup', '2021-11-14 23:59:18', '2021-11-14 23:59:18', '2021-11-14 23:59:18', '4e102d21-4576-4820-b601-4642d267b3b6'),
(26, 'craft', 'm160830_000000_asset_index_uri_increase', '2021-11-14 23:59:18', '2021-11-14 23:59:18', '2021-11-14 23:59:18', '2ab6976b-8399-4962-bc0d-7f9b6e3a7a06'),
(27, 'craft', 'm160912_230520_require_entry_type_id', '2021-11-14 23:59:19', '2021-11-14 23:59:19', '2021-11-14 23:59:19', 'd485d63c-fe2a-4c08-9170-ffcf71dce69b'),
(28, 'craft', 'm160913_134730_require_matrix_block_type_id', '2021-11-14 23:59:19', '2021-11-14 23:59:19', '2021-11-14 23:59:19', '6871467e-5a7d-455e-bebe-92550053535d'),
(29, 'craft', 'm160920_174553_matrixblocks_owner_site_id_nullable', '2021-11-14 23:59:19', '2021-11-14 23:59:19', '2021-11-14 23:59:19', 'ae3e3fbc-9503-4741-8fec-351e5da6aee8'),
(30, 'craft', 'm160920_231045_usergroup_handle_title_unique', '2021-11-14 23:59:19', '2021-11-14 23:59:19', '2021-11-14 23:59:19', '7fb08707-560e-4d97-a20e-b00e4413a4f6'),
(31, 'craft', 'm160925_113941_route_uri_parts', '2021-11-14 23:59:20', '2021-11-14 23:59:20', '2021-11-14 23:59:20', 'eb2949b2-bed3-4d41-8084-83f5d8e83b06'),
(32, 'craft', 'm161006_205918_schemaVersion_not_null', '2021-11-14 23:59:20', '2021-11-14 23:59:20', '2021-11-14 23:59:20', 'd17fe2db-9910-4d02-9505-038668fba3ee'),
(33, 'craft', 'm161007_130653_update_email_settings', '2021-11-14 23:59:20', '2021-11-14 23:59:20', '2021-11-14 23:59:20', '1c905d5a-8d77-4adc-a946-13d7324bd159'),
(34, 'craft', 'm161013_175052_newParentId', '2021-11-14 23:59:20', '2021-11-14 23:59:20', '2021-11-14 23:59:20', '30dd881d-cb7a-4d28-8066-8f948d6e657a'),
(35, 'craft', 'm161021_102916_fix_recent_entries_widgets', '2021-11-14 23:59:20', '2021-11-14 23:59:20', '2021-11-14 23:59:20', '61365b25-a773-4c3e-8293-76b11aba81d3'),
(36, 'craft', 'm161021_182140_rename_get_help_widget', '2021-11-14 23:59:20', '2021-11-14 23:59:20', '2021-11-14 23:59:20', '66523715-09ed-41e0-a3ed-ce0e2dc0dee8'),
(37, 'craft', 'm161025_000000_fix_char_columns', '2021-11-14 23:59:21', '2021-11-14 23:59:21', '2021-11-14 23:59:21', '8ef78e5f-ff56-4635-ae87-7d8d547c28b0'),
(38, 'craft', 'm161029_124145_email_message_languages', '2021-11-14 23:59:21', '2021-11-14 23:59:21', '2021-11-14 23:59:21', '33db786b-0e42-4edd-97ba-dd2894a034c4'),
(39, 'craft', 'm161108_000000_new_version_format', '2021-11-14 23:59:21', '2021-11-14 23:59:21', '2021-11-14 23:59:21', '75446af6-46da-43ff-8cb2-a5bcd0ff1cb2'),
(40, 'craft', 'm161109_000000_index_shuffle', '2021-11-14 23:59:21', '2021-11-14 23:59:21', '2021-11-14 23:59:21', '2b65931b-a1c6-4ec1-ac5a-6fb812de5e56'),
(41, 'craft', 'm161122_185500_no_craft_app', '2021-11-14 23:59:21', '2021-11-14 23:59:21', '2021-11-14 23:59:21', '83dd74f2-5742-4f59-803d-b0761509f9fe'),
(42, 'craft', 'm161125_150752_clear_urlmanager_cache', '2021-11-14 23:59:22', '2021-11-14 23:59:22', '2021-11-14 23:59:22', 'cf7ed4f3-937a-4eb7-887c-35d190ec3d93'),
(43, 'craft', 'm161220_000000_volumes_hasurl_notnull', '2021-11-14 23:59:22', '2021-11-14 23:59:22', '2021-11-14 23:59:22', '883070e1-9e13-47c4-afbd-5f02403d9c38'),
(44, 'craft', 'm170114_161144_udates_permission', '2021-11-14 23:59:22', '2021-11-14 23:59:22', '2021-11-14 23:59:22', '88b84273-aa5e-465c-b5ae-a1177e8bb838'),
(45, 'craft', 'm170120_000000_schema_cleanup', '2021-11-14 23:59:22', '2021-11-14 23:59:22', '2021-11-14 23:59:22', 'e2278b97-6535-41a7-97cc-c14012c68143'),
(46, 'craft', 'm170126_000000_assets_focal_point', '2021-11-14 23:59:22', '2021-11-14 23:59:22', '2021-11-14 23:59:22', '19b83181-d4a6-4601-8966-b07bfc4fc104'),
(47, 'craft', 'm170206_142126_system_name', '2021-11-14 23:59:22', '2021-11-14 23:59:22', '2021-11-14 23:59:22', '8281def3-49f8-466f-89ba-7d8e03f60423'),
(48, 'craft', 'm170217_044740_category_branch_limits', '2021-11-14 23:59:23', '2021-11-14 23:59:23', '2021-11-14 23:59:23', '548c6638-d4f2-4aaf-b109-70af7378dcc2'),
(49, 'craft', 'm170217_120224_asset_indexing_columns', '2021-11-14 23:59:23', '2021-11-14 23:59:23', '2021-11-14 23:59:23', '4d4396bd-dd79-4a2a-9b11-340df8818eb6'),
(50, 'craft', 'm170223_224012_plain_text_settings', '2021-11-14 23:59:23', '2021-11-14 23:59:23', '2021-11-14 23:59:23', '939b1355-d301-44a8-869e-a33e5ea93f1d'),
(51, 'craft', 'm170227_120814_focal_point_percentage', '2021-11-14 23:59:23', '2021-11-14 23:59:23', '2021-11-14 23:59:23', 'c4ab4ad8-010d-4cbb-aa1d-6eba022f35d7'),
(52, 'craft', 'm170228_171113_system_messages', '2021-11-14 23:59:23', '2021-11-14 23:59:23', '2021-11-14 23:59:23', '8844dc8d-0630-4f8a-8bbd-b45e15311d64'),
(53, 'craft', 'm170303_140500_asset_field_source_settings', '2021-11-14 23:59:23', '2021-11-14 23:59:23', '2021-11-14 23:59:23', '55aa197c-bf9a-44fb-8fa8-94a538a3d149'),
(54, 'craft', 'm170306_150500_asset_temporary_uploads', '2021-11-14 23:59:24', '2021-11-14 23:59:24', '2021-11-14 23:59:24', '234a72f9-afe2-4700-9948-6b720456caa0'),
(55, 'craft', 'm170523_190652_element_field_layout_ids', '2021-11-14 23:59:24', '2021-11-14 23:59:24', '2021-11-14 23:59:24', 'fe3e9e8f-03b2-4611-bb08-75fd4d820448'),
(56, 'craft', 'm170621_195237_format_plugin_handles', '2021-11-14 23:59:25', '2021-11-14 23:59:25', '2021-11-14 23:59:25', '30c344d6-1557-49a0-9664-7a8f5503104f'),
(57, 'craft', 'm170630_161027_deprecation_line_nullable', '2021-11-14 23:59:25', '2021-11-14 23:59:25', '2021-11-14 23:59:25', '9f73ab3e-9c0c-40e4-8c0f-3d8f2d38ffc0'),
(58, 'craft', 'm170630_161028_deprecation_changes', '2021-11-14 23:59:25', '2021-11-14 23:59:25', '2021-11-14 23:59:25', 'e4c13a76-5cde-4c92-865c-8c746df0f79a'),
(59, 'craft', 'm170703_181539_plugins_table_tweaks', '2021-11-14 23:59:25', '2021-11-14 23:59:25', '2021-11-14 23:59:25', 'ee798bdd-6357-43ba-8f79-bfa6c983041d'),
(60, 'craft', 'm170704_134916_sites_tables', '2021-11-14 23:59:26', '2021-11-14 23:59:26', '2021-11-14 23:59:26', 'f5149027-be9c-471b-850a-6a9a7bae0259'),
(61, 'craft', 'm170706_183216_rename_sequences', '2021-11-14 23:59:26', '2021-11-14 23:59:26', '2021-11-14 23:59:26', '988e565e-bb2e-43ba-abf0-2350109d9039'),
(62, 'craft', 'm170707_094758_delete_compiled_traits', '2021-11-14 23:59:26', '2021-11-14 23:59:26', '2021-11-14 23:59:26', 'b2d85564-ed6e-4410-bfa7-5053f4a7ecd3'),
(63, 'craft', 'm170731_190138_drop_asset_packagist', '2021-11-14 23:59:26', '2021-11-14 23:59:26', '2021-11-14 23:59:26', '1d81eacf-59e8-4ed0-b515-3b64654704ad'),
(64, 'craft', 'm170810_201318_create_queue_table', '2021-11-14 23:59:26', '2021-11-14 23:59:26', '2021-11-14 23:59:26', '1cbc5852-fbb0-43fc-9040-b1b9b99c8eac'),
(65, 'craft', 'm170903_192801_longblob_for_queue_jobs', '2021-11-14 23:59:26', '2021-11-14 23:59:26', '2021-11-14 23:59:26', '71b5f4db-401c-4950-a246-8ee88751ae88'),
(66, 'craft', 'm170914_204621_asset_cache_shuffle', '2021-11-14 23:59:27', '2021-11-14 23:59:27', '2021-11-14 23:59:27', '4fd24bc0-94ce-4b29-ab05-3f9e9e029cd6'),
(67, 'craft', 'm171011_214115_site_groups', '2021-11-14 23:59:27', '2021-11-14 23:59:27', '2021-11-14 23:59:27', '95d3571b-dbe3-4251-9506-5ab393c7588e'),
(68, 'craft', 'm171012_151440_primary_site', '2021-11-14 23:59:27', '2021-11-14 23:59:27', '2021-11-14 23:59:27', '21d52f6f-5107-49b5-aa22-a92b866cb756'),
(69, 'craft', 'm171013_142500_transform_interlace', '2021-11-14 23:59:27', '2021-11-14 23:59:27', '2021-11-14 23:59:27', '5c47a7bc-d22a-4da9-acf4-5dbbbed578e5'),
(70, 'craft', 'm171016_092553_drop_position_select', '2021-11-14 23:59:28', '2021-11-14 23:59:28', '2021-11-14 23:59:28', '3a286ecf-3f3f-4d95-9649-ea868fd5437c'),
(71, 'craft', 'm171016_221244_less_strict_translation_method', '2021-11-14 23:59:28', '2021-11-14 23:59:28', '2021-11-14 23:59:28', '30dc626c-7ac2-4c5f-97ed-a1b9d4c1dfa1'),
(72, 'craft', 'm171107_000000_assign_group_permissions', '2021-11-14 23:59:28', '2021-11-14 23:59:28', '2021-11-14 23:59:28', '70fe76f0-43fd-4771-bd7b-c9599298172b'),
(73, 'craft', 'm171117_000001_templatecache_index_tune', '2021-11-14 23:59:29', '2021-11-14 23:59:29', '2021-11-14 23:59:29', '4fc098b7-b01a-4819-8d72-1b25ec198550'),
(74, 'craft', 'm171126_105927_disabled_plugins', '2021-11-14 23:59:29', '2021-11-14 23:59:29', '2021-11-14 23:59:29', '835b7363-82c5-432d-b3ee-b321297d6d06'),
(75, 'craft', 'm171130_214407_craftidtokens_table', '2021-11-14 23:59:29', '2021-11-14 23:59:29', '2021-11-14 23:59:29', '77059013-6cbb-4590-bf44-721ab17a9edb'),
(76, 'craft', 'm171202_004225_update_email_settings', '2021-11-14 23:59:30', '2021-11-14 23:59:30', '2021-11-14 23:59:30', 'b3c3a758-18e6-4ce0-85a3-f3dfc3d7bc43'),
(77, 'craft', 'm171204_000001_templatecache_index_tune_deux', '2021-11-14 23:59:30', '2021-11-14 23:59:30', '2021-11-14 23:59:30', '6626bd03-4c4e-43dd-a2d3-8d7825d96cb5'),
(78, 'craft', 'm171205_130908_remove_craftidtokens_refreshtoken_column', '2021-11-14 23:59:31', '2021-11-14 23:59:31', '2021-11-14 23:59:31', '2c58db80-f935-4c2e-abd2-2e232d9ec91f'),
(79, 'craft', 'm171218_143135_longtext_query_column', '2021-11-14 23:59:31', '2021-11-14 23:59:31', '2021-11-14 23:59:31', '37e8ccbc-1921-4701-a7d8-ee6653549c65'),
(80, 'craft', 'm171231_055546_environment_variables_to_aliases', '2021-11-14 23:59:31', '2021-11-14 23:59:31', '2021-11-14 23:59:31', 'cf1b588b-c4cd-4c90-9cc1-21b668b6b1b2'),
(81, 'craft', 'm180113_153740_drop_users_archived_column', '2021-11-14 23:59:32', '2021-11-14 23:59:32', '2021-11-14 23:59:32', '4630dff2-095f-4ff4-a085-898bdeee9b42'),
(82, 'craft', 'm180122_213433_propagate_entries_setting', '2021-11-14 23:59:32', '2021-11-14 23:59:32', '2021-11-14 23:59:32', '07726eb6-475c-4fe1-b817-c1c08072c5ee'),
(83, 'craft', 'm180124_230459_fix_propagate_entries_values', '2021-11-14 23:59:32', '2021-11-14 23:59:32', '2021-11-14 23:59:32', 'c6143742-1fa3-4ef9-b376-c198666c4523'),
(84, 'craft', 'm180128_235202_set_tag_slugs', '2021-11-14 23:59:32', '2021-11-14 23:59:32', '2021-11-14 23:59:32', 'e1adc1de-9df6-4e0e-9188-e199872cb496'),
(85, 'craft', 'm180202_185551_fix_focal_points', '2021-11-14 23:59:32', '2021-11-14 23:59:32', '2021-11-14 23:59:32', '4234a240-eb63-43fc-a420-e05b0ec6e9c6'),
(86, 'craft', 'm180217_172123_tiny_ints', '2021-11-14 23:59:33', '2021-11-14 23:59:33', '2021-11-14 23:59:33', '585d39a5-e302-42db-aa3c-07e15a31be6a'),
(87, 'craft', 'm180321_233505_small_ints', '2021-11-14 23:59:33', '2021-11-14 23:59:33', '2021-11-14 23:59:33', '1d9f4d30-0f8d-4fca-8c71-9ba8d53bd1d3'),
(88, 'craft', 'm180404_182320_edition_changes', '2021-11-14 23:59:33', '2021-11-14 23:59:33', '2021-11-14 23:59:33', 'e59feca5-811f-4ad2-b539-e9a477008f0f'),
(89, 'craft', 'm180411_102218_fix_db_routes', '2021-11-14 23:59:33', '2021-11-14 23:59:33', '2021-11-14 23:59:33', 'cd2a75f6-18c2-4827-bd3e-2f535996c418'),
(90, 'craft', 'm180416_205628_resourcepaths_table', '2021-11-14 23:59:34', '2021-11-14 23:59:34', '2021-11-14 23:59:34', '83452448-2c0d-451c-80e5-9e52f7a9d957'),
(91, 'craft', 'm180418_205713_widget_cleanup', '2021-11-14 23:59:34', '2021-11-14 23:59:34', '2021-11-14 23:59:34', 'fbce3fc5-bc3d-4153-83d9-46fadc079bc3'),
(92, 'craft', 'm180425_203349_searchable_fields', '2021-11-14 23:59:34', '2021-11-14 23:59:34', '2021-11-14 23:59:34', '0452324e-7f1d-4f86-b559-910a6ade1b99'),
(93, 'craft', 'm180516_153000_uids_in_field_settings', '2021-11-14 23:59:34', '2021-11-14 23:59:34', '2021-11-14 23:59:34', '3bce7c4f-799b-49ca-a019-180f45755f97'),
(94, 'craft', 'm180517_173000_user_photo_volume_to_uid', '2021-11-14 23:59:34', '2021-11-14 23:59:34', '2021-11-14 23:59:34', '039ef9d0-992e-4045-a127-14e2c880773f'),
(95, 'craft', 'm180518_173000_permissions_to_uid', '2021-11-14 23:59:35', '2021-11-14 23:59:35', '2021-11-14 23:59:35', '42fe1405-649b-46de-ade1-c076168c18a9'),
(96, 'craft', 'm180520_173000_matrix_context_to_uids', '2021-11-14 23:59:35', '2021-11-14 23:59:35', '2021-11-14 23:59:35', 'a23524ec-48d1-4e5c-8461-1f9a6ade7caf'),
(97, 'craft', 'm180521_172900_project_config_table', '2021-11-14 23:59:35', '2021-11-14 23:59:35', '2021-11-14 23:59:35', '57e1a57a-c156-47bd-8d52-774e478b8de7'),
(98, 'craft', 'm180521_173000_initial_yml_and_snapshot', '2021-11-14 23:59:35', '2021-11-14 23:59:35', '2021-11-14 23:59:35', 'bf61ab02-e46a-4801-acca-dfba49e54b8a'),
(99, 'craft', 'm180731_162030_soft_delete_sites', '2021-11-14 23:59:35', '2021-11-14 23:59:35', '2021-11-14 23:59:35', '5c836d6d-ce92-42c4-87e1-7232cbbc60af'),
(100, 'craft', 'm180810_214427_soft_delete_field_layouts', '2021-11-14 23:59:36', '2021-11-14 23:59:36', '2021-11-14 23:59:36', 'e17d08a8-c868-4953-8c9f-5e4f064c5cc0'),
(101, 'craft', 'm180810_214439_soft_delete_elements', '2021-11-14 23:59:36', '2021-11-14 23:59:36', '2021-11-14 23:59:36', '0757f651-ac2a-49c5-92c4-9c5166b83c95'),
(102, 'craft', 'm180824_193422_case_sensitivity_fixes', '2021-11-14 23:59:36', '2021-11-14 23:59:36', '2021-11-14 23:59:36', '7dc2ddff-fdd2-4464-a1a0-08d5cbd7e977'),
(103, 'craft', 'm180901_151639_fix_matrixcontent_tables', '2021-11-14 23:59:36', '2021-11-14 23:59:36', '2021-11-14 23:59:36', 'fce8ebab-1230-459f-a151-d579f9a48f77'),
(104, 'craft', 'm180904_112109_permission_changes', '2021-11-14 23:59:36', '2021-11-14 23:59:36', '2021-11-14 23:59:36', '2c094937-ded4-4330-bf4d-4e59840b1d4b'),
(105, 'craft', 'm180910_142030_soft_delete_sitegroups', '2021-11-14 23:59:36', '2021-11-14 23:59:36', '2021-11-14 23:59:36', 'af6c222b-50cc-4adf-9867-d0bff5c07b6b'),
(106, 'craft', 'm181011_160000_soft_delete_asset_support', '2021-11-14 23:59:37', '2021-11-14 23:59:37', '2021-11-14 23:59:37', '698ef902-742b-445b-bfb6-82eb5686cfd9'),
(107, 'craft', 'm181016_183648_set_default_user_settings', '2021-11-14 23:59:37', '2021-11-14 23:59:37', '2021-11-14 23:59:37', 'ce172c7f-db8a-4041-978b-4ddf00a3adcb'),
(108, 'craft', 'm181017_225222_system_config_settings', '2021-11-14 23:59:37', '2021-11-14 23:59:37', '2021-11-14 23:59:37', '772ae458-7d3d-4845-b701-1cc9bf6dd544'),
(109, 'craft', 'm181018_222343_drop_userpermissions_from_config', '2021-11-14 23:59:37', '2021-11-14 23:59:37', '2021-11-14 23:59:37', 'e51de274-385f-4d0a-84ce-d1a0a7f50932'),
(110, 'craft', 'm181029_130000_add_transforms_routes_to_config', '2021-11-14 23:59:37', '2021-11-14 23:59:37', '2021-11-14 23:59:37', '51f01014-fa48-4f07-9371-233212eac13e'),
(111, 'craft', 'm181112_203955_sequences_table', '2021-11-14 23:59:38', '2021-11-14 23:59:38', '2021-11-14 23:59:38', '5e945437-72e6-44e6-bac3-c663097da818'),
(112, 'craft', 'm181121_001712_cleanup_field_configs', '2021-11-14 23:59:38', '2021-11-14 23:59:38', '2021-11-14 23:59:38', 'aaba2fd3-bd87-4130-bad7-788e84ed798e'),
(113, 'craft', 'm181128_193942_fix_project_config', '2021-11-14 23:59:38', '2021-11-14 23:59:38', '2021-11-14 23:59:38', 'd5a5e5df-85f7-4f59-88bc-0b3c0a4cf10e'),
(114, 'craft', 'm181130_143040_fix_schema_version', '2021-11-14 23:59:38', '2021-11-14 23:59:38', '2021-11-14 23:59:38', '2e91eaee-2bce-4821-9667-4fc4a3a860b9'),
(115, 'craft', 'm181211_143040_fix_entry_type_uids', '2021-11-14 23:59:38', '2021-11-14 23:59:38', '2021-11-14 23:59:38', 'd52319f3-9923-446d-b45a-fc0a38011589'),
(116, 'craft', 'm181217_153000_fix_structure_uids', '2021-11-14 23:59:38', '2021-11-14 23:59:38', '2021-11-14 23:59:38', 'bce5f712-1e73-44a6-a697-478e3dc2cbda'),
(117, 'craft', 'm190104_152725_store_licensed_plugin_editions', '2021-11-14 23:59:39', '2021-11-14 23:59:39', '2021-11-14 23:59:39', '1f7eac77-0594-4039-afa6-9dd8c48a06c7'),
(118, 'craft', 'm190108_110000_cleanup_project_config', '2021-11-14 23:59:39', '2021-11-14 23:59:39', '2021-11-14 23:59:39', '227d5a1b-e801-49bf-b2ac-f83ff3299dd9'),
(119, 'craft', 'm190108_113000_asset_field_setting_change', '2021-11-14 23:59:39', '2021-11-14 23:59:39', '2021-11-14 23:59:39', '5cb63946-b959-47d2-9dc8-8ce3b6befe6e'),
(120, 'craft', 'm190109_172845_fix_colspan', '2021-11-14 23:59:39', '2021-11-14 23:59:39', '2021-11-14 23:59:39', 'c8d32529-5396-47ef-8111-4b280af95c12'),
(121, 'craft', 'm190110_150000_prune_nonexisting_sites', '2021-11-14 23:59:40', '2021-11-14 23:59:40', '2021-11-14 23:59:40', 'fb3b23bd-a46d-411f-afeb-98593d7abe8a'),
(122, 'craft', 'm190110_214819_soft_delete_volumes', '2021-11-14 23:59:40', '2021-11-14 23:59:40', '2021-11-14 23:59:40', '927ecd4d-9c1c-497c-8266-794283c5a2ba'),
(123, 'craft', 'm190112_124737_fix_user_settings', '2021-11-14 23:59:40', '2021-11-14 23:59:40', '2021-11-14 23:59:40', '6aa0c945-bc85-4d1e-a840-a658cf816b20'),
(124, 'craft', 'm190112_131225_fix_field_layouts', '2021-11-14 23:59:40', '2021-11-14 23:59:40', '2021-11-14 23:59:40', 'df201eb0-ca58-4c44-8090-94bc7346fe67'),
(125, 'craft', 'm190112_201010_more_soft_deletes', '2021-11-14 23:59:40', '2021-11-14 23:59:40', '2021-11-14 23:59:40', '74bc0ea8-49a1-420e-a2ac-4d95e74b1aca'),
(126, 'craft', 'm190114_143000_more_asset_field_setting_changes', '2021-11-14 23:59:40', '2021-11-14 23:59:40', '2021-11-14 23:59:40', '3b2baf81-d4b4-4704-86de-6d8124edaa7f'),
(127, 'craft', 'm190121_120000_rich_text_config_setting', '2021-11-14 23:59:41', '2021-11-14 23:59:41', '2021-11-14 23:59:41', 'd97348ab-091b-4b3d-8067-d72788bc47ca'),
(128, 'craft', 'm190125_191628_fix_email_transport_password', '2021-11-14 23:59:41', '2021-11-14 23:59:41', '2021-11-14 23:59:41', 'b8ec4a2d-5984-48cc-bd36-4e36b18efd29'),
(129, 'craft', 'm190128_181422_cleanup_volume_folders', '2021-11-14 23:59:41', '2021-11-14 23:59:41', '2021-11-14 23:59:41', '1ce37bbc-ea17-4e9c-86af-2a8060eb6688'),
(130, 'craft', 'm190205_140000_fix_asset_soft_delete_index', '2021-11-14 23:59:41', '2021-11-14 23:59:41', '2021-11-14 23:59:41', '12b5a61e-599e-4447-b478-56d56e3cf4fa'),
(131, 'craft', 'm190218_143000_element_index_settings_uid', '2021-11-14 23:59:41', '2021-11-14 23:59:41', '2021-11-14 23:59:41', '5f8a169d-c785-4179-8f22-2245fe5f981f'),
(132, 'craft', 'm190312_152740_element_revisions', '2021-11-14 23:59:42', '2021-11-14 23:59:42', '2021-11-14 23:59:42', 'aaf920d7-420d-4903-9125-cdae74c8478d'),
(133, 'craft', 'm190327_235137_propagation_method', '2021-11-14 23:59:42', '2021-11-14 23:59:42', '2021-11-14 23:59:42', 'fdca6d03-8eb0-4c92-b52a-84bd2d8d8eb8'),
(134, 'craft', 'm190401_223843_drop_old_indexes', '2021-11-14 23:59:42', '2021-11-14 23:59:42', '2021-11-14 23:59:42', '46da9a92-3712-4cbe-be61-323a06cc4d18'),
(135, 'craft', 'm190416_014525_drop_unique_global_indexes', '2021-11-14 23:59:42', '2021-11-14 23:59:42', '2021-11-14 23:59:42', '6193d2da-877c-4e0b-a3c8-63fb619aae02'),
(136, 'craft', 'm190417_085010_add_image_editor_permissions', '2021-11-14 23:59:42', '2021-11-14 23:59:42', '2021-11-14 23:59:42', 'dfa26ea2-0d0c-4450-aebd-06f35ffb352b'),
(137, 'craft', 'm190502_122019_store_default_user_group_uid', '2021-11-14 23:59:43', '2021-11-14 23:59:43', '2021-11-14 23:59:43', '749a47c0-90e8-463b-af8d-245fdd473e0e'),
(138, 'craft', 'm190504_150349_preview_targets', '2021-11-14 23:59:43', '2021-11-14 23:59:43', '2021-11-14 23:59:43', '0503b4ac-33d2-4394-beb0-9f5dfa426c52'),
(139, 'craft', 'm190516_184711_job_progress_label', '2021-11-14 23:59:43', '2021-11-14 23:59:43', '2021-11-14 23:59:43', 'e939d75b-61f9-4aa8-8ff2-eb7652570a3f'),
(140, 'craft', 'm190523_190303_optional_revision_creators', '2021-11-14 23:59:43', '2021-11-14 23:59:43', '2021-11-14 23:59:43', '49286297-c91d-4ef1-b405-f6187a7a58ff'),
(141, 'craft', 'm190529_204501_fix_duplicate_uids', '2021-11-14 23:59:43', '2021-11-14 23:59:43', '2021-11-14 23:59:43', '5e94dce7-fac0-4724-8068-c8e125298c57'),
(142, 'craft', 'm190605_223807_unsaved_drafts', '2021-11-14 23:59:43', '2021-11-14 23:59:43', '2021-11-14 23:59:43', 'cbf6c9b3-f624-4a51-a4ec-06e62d0db56d'),
(143, 'craft', 'm190607_230042_entry_revision_error_tables', '2021-11-14 23:59:44', '2021-11-14 23:59:44', '2021-11-14 23:59:44', 'cf7fed40-188f-4e27-aa28-4f2677da0013'),
(144, 'craft', 'm190608_033429_drop_elements_uid_idx', '2021-11-14 23:59:44', '2021-11-14 23:59:44', '2021-11-14 23:59:44', '2abb2b67-147e-4821-a812-762bf1bac494'),
(145, 'craft', 'm190617_164400_add_gqlschemas_table', '2021-11-14 23:59:44', '2021-11-14 23:59:44', '2021-11-14 23:59:44', '80b489e0-9f0d-4253-8f63-201b87ee2168'),
(146, 'craft', 'm190624_234204_matrix_propagation_method', '2021-11-14 23:59:44', '2021-11-14 23:59:44', '2021-11-14 23:59:44', '99452784-bb9c-46f9-86d1-91945e30be9b'),
(147, 'craft', 'm190711_153020_drop_snapshots', '2021-11-14 23:59:45', '2021-11-14 23:59:45', '2021-11-14 23:59:45', '908b0fb0-a092-45b4-8251-f9eba29e3405'),
(148, 'craft', 'm190712_195914_no_draft_revisions', '2021-11-14 23:59:45', '2021-11-14 23:59:45', '2021-11-14 23:59:45', '685d87c9-7439-43d4-bb2e-5efe4b2d771e'),
(149, 'craft', 'm190723_140314_fix_preview_targets_column', '2021-11-14 23:59:45', '2021-11-14 23:59:45', '2021-11-14 23:59:45', 'c74e43a6-d504-413c-8d9a-24db3ec78ce9'),
(150, 'craft', 'm190820_003519_flush_compiled_templates', '2021-11-14 23:59:45', '2021-11-14 23:59:45', '2021-11-14 23:59:45', 'c2d9fe9c-84d9-4928-a4e5-52a118db624b'),
(151, 'craft', 'm190823_020339_optional_draft_creators', '2021-11-14 23:59:45', '2021-11-14 23:59:45', '2021-11-14 23:59:45', 'c23a254f-df9a-4736-97c0-4d41153dfea5'),
(152, 'craft', 'm190913_152146_update_preview_targets', '2021-11-14 23:59:45', '2021-11-14 23:59:45', '2021-11-14 23:59:45', 'cd1ad683-f3c3-42f1-a48c-f0e3a51b77cc'),
(153, 'craft', 'm191107_122000_add_gql_project_config_support', '2021-11-14 23:59:46', '2021-11-14 23:59:46', '2021-11-14 23:59:46', '1177ab99-2765-4384-ac2d-2befd114f427'),
(154, 'craft', 'm191204_085100_pack_savable_component_settings', '2021-11-14 23:59:46', '2021-11-14 23:59:46', '2021-11-14 23:59:46', '952b2a67-9644-4472-bba1-d23f4677b2d1'),
(155, 'craft', 'm191206_001148_change_tracking', '2021-11-14 23:59:46', '2021-11-14 23:59:46', '2021-11-14 23:59:46', 'cea25212-03de-46c5-81ac-f93054aae5dc'),
(156, 'craft', 'm191216_191635_asset_upload_tracking', '2021-11-14 23:59:46', '2021-11-14 23:59:46', '2021-11-14 23:59:46', 'b55e21d9-770f-42c8-a206-eacae0398ec2'),
(157, 'craft', 'm191222_002848_peer_asset_permissions', '2021-11-14 23:59:46', '2021-11-14 23:59:46', '2021-11-14 23:59:46', 'c74a13e1-12de-4c26-a8e2-c1a12d08ff5b'),
(158, 'craft', 'm200127_172522_queue_channels', '2021-11-14 23:59:46', '2021-11-14 23:59:46', '2021-11-14 23:59:46', '1f488b76-d497-405e-b4be-6b4e7e35a8f5'),
(159, 'craft', 'm200211_175048_truncate_element_query_cache', '2021-11-14 23:59:47', '2021-11-14 23:59:47', '2021-11-14 23:59:47', '6180cc7d-3a3a-4f90-838a-863752ccbe23'),
(160, 'craft', 'm200213_172522_new_elements_index', '2021-11-14 23:59:47', '2021-11-14 23:59:47', '2021-11-14 23:59:47', 'ba9ecb2a-2342-40b4-a3ed-05b6f76a8fa2'),
(161, 'craft', 'm200228_195211_long_deprecation_messages', '2021-11-14 23:59:47', '2021-11-14 23:59:47', '2021-11-14 23:59:47', 'ffdaac22-d64b-40cb-97ae-16dff9748c67'),
(162, 'craft', 'm200306_054652_disabled_sites', '2021-11-14 23:59:47', '2021-11-14 23:59:47', '2021-11-14 23:59:47', 'cee175a4-57b7-4962-8c33-b50a8ae006fe'),
(163, 'craft', 'm200522_191453_clear_template_caches', '2021-11-14 23:59:47', '2021-11-14 23:59:47', '2021-11-14 23:59:47', 'ac30430d-eaf3-42a3-a4b4-5acdb5cb4c7e'),
(164, 'craft', 'm200606_231117_migration_tracks', '2021-11-14 23:59:47', '2021-11-14 23:59:47', '2021-11-14 23:59:47', '85c6b9e5-a540-416f-9ff5-7df1560ec0b6'),
(165, 'craft', 'm200619_215137_title_translation_method', '2021-11-14 23:59:47', '2021-11-14 23:59:47', '2021-11-14 23:59:47', '671d6542-4d53-4d65-9a2a-c61e327c2048'),
(166, 'craft', 'm200620_005028_user_group_descriptions', '2021-11-14 23:59:48', '2021-11-14 23:59:48', '2021-11-14 23:59:48', 'b37eaa42-3111-4cff-b4a7-c71eabbffb6b'),
(167, 'craft', 'm200620_230205_field_layout_changes', '2021-11-14 23:59:48', '2021-11-14 23:59:48', '2021-11-14 23:59:48', '079563e7-c40b-4fc9-b1c2-b24b58013b04'),
(168, 'craft', 'm200625_131100_move_entrytypes_to_top_project_config', '2021-11-14 23:59:49', '2021-11-14 23:59:49', '2021-11-14 23:59:49', 'e6af08ae-0a84-4479-abb5-2deba01ce7d0'),
(169, 'craft', 'm200629_112700_remove_project_config_legacy_files', '2021-11-14 23:59:49', '2021-11-14 23:59:49', '2021-11-14 23:59:49', '021695a2-ccba-4443-b15d-419396a9be14'),
(170, 'craft', 'm200630_183000_drop_configmap', '2021-11-14 23:59:49', '2021-11-14 23:59:49', '2021-11-14 23:59:49', 'bb53117b-c189-45e0-837d-f299ca29ea04'),
(171, 'craft', 'm200715_113400_transform_index_error_flag', '2021-11-14 23:59:49', '2021-11-14 23:59:49', '2021-11-14 23:59:49', '1020d9bb-f293-4d25-bf5c-a966df476139'),
(172, 'craft', 'm200716_110900_replace_file_asset_permissions', '2021-11-14 23:59:49', '2021-11-14 23:59:49', '2021-11-14 23:59:49', 'a5b5263f-5b4e-426f-8a71-c8208175408f'),
(173, 'craft', 'm200716_153800_public_token_settings_in_project_config', '2021-11-14 23:59:50', '2021-11-14 23:59:50', '2021-11-14 23:59:50', 'bb2ab9d5-a9d6-49f0-8c8b-b51e9e7f42c9'),
(174, 'craft', 'm200720_175543_drop_unique_constraints', '2021-11-14 23:59:50', '2021-11-14 23:59:50', '2021-11-14 23:59:50', 'dd513362-918e-4ad5-996b-df1c3ae9a181'),
(175, 'craft', 'm200825_051217_project_config_version', '2021-11-14 23:59:50', '2021-11-14 23:59:50', '2021-11-14 23:59:50', '677b3a8f-0e1c-43ce-a562-ef0d7dc14bbd'),
(176, 'craft', 'm201116_190500_asset_title_translation_method', '2021-11-14 23:59:50', '2021-11-14 23:59:50', '2021-11-14 23:59:50', '6878cff6-3ce8-4f1f-960a-cbb36fbac21e'),
(177, 'craft', 'm201124_003555_plugin_trials', '2021-11-14 23:59:50', '2021-11-14 23:59:50', '2021-11-14 23:59:50', '956f190c-3e98-4243-874d-6b6b2043dae4'),
(178, 'craft', 'm210209_135503_soft_delete_field_groups', '2021-11-14 23:59:50', '2021-11-14 23:59:50', '2021-11-14 23:59:50', '59a03f70-acb7-4806-97b7-b531d982d558'),
(179, 'craft', 'm210212_223539_delete_invalid_drafts', '2021-11-14 23:59:51', '2021-11-14 23:59:51', '2021-11-14 23:59:51', 'e332b6bb-e6ac-4c09-96e7-e1c42da241e8'),
(180, 'craft', 'm210214_202731_track_saved_drafts', '2021-11-14 23:59:51', '2021-11-14 23:59:51', '2021-11-14 23:59:51', '158bd03b-6a8c-4e0f-a372-df6ad5c2753f'),
(181, 'craft', 'm210223_150900_add_new_element_gql_schema_components', '2021-11-14 23:59:51', '2021-11-14 23:59:51', '2021-11-14 23:59:51', '312d8a80-3098-4f3d-829a-93a21edacba2'),
(182, 'craft', 'm210302_212318_canonical_elements', '2021-11-14 23:59:51', '2021-11-14 23:59:51', '2021-11-14 23:59:51', 'fd65d1cd-2203-44b4-a5c6-840fe5266f47'),
(183, 'craft', 'm210326_132000_invalidate_projectconfig_cache', '2021-11-14 23:59:51', '2021-11-14 23:59:51', '2021-11-14 23:59:51', '0cff82b5-9fc5-42fa-9de8-dd8bcac12d1a'),
(184, 'craft', 'm210329_214847_field_column_suffixes', '2021-11-14 23:59:51', '2021-11-14 23:59:51', '2021-11-14 23:59:51', 'e632342c-a5c7-4a37-8a96-f4a9431fdc5b'),
(185, 'craft', 'm210331_220322_null_author', '2021-11-14 23:59:52', '2021-11-14 23:59:52', '2021-11-14 23:59:52', 'd4d25cc7-5909-4fb1-905d-6fa16cd024c1'),
(186, 'craft', 'm210405_231315_provisional_drafts', '2021-11-14 23:59:52', '2021-11-14 23:59:52', '2021-11-14 23:59:52', '1ad8d592-afbe-4bdb-a8dc-56d106ccaca2'),
(187, 'craft', 'm210602_111300_project_config_names_in_config', '2021-11-14 23:59:52', '2021-11-14 23:59:52', '2021-11-14 23:59:52', '7cfb3cc6-2ab1-4a6d-bdd0-ce8a43203c52'),
(188, 'craft', 'm210611_233510_default_placement_settings', '2021-11-14 23:59:52', '2021-11-14 23:59:52', '2021-11-14 23:59:52', '4102f636-b1b5-4c6b-873e-d9147a047a0d'),
(189, 'craft', 'm210613_145522_sortable_global_sets', '2021-11-14 23:59:52', '2021-11-14 23:59:52', '2021-11-14 23:59:52', '2eac98d6-1adc-4363-8974-d45ebc15a431'),
(190, 'craft', 'm210613_184103_announcements', '2021-11-14 23:59:52', '2021-11-14 23:59:52', '2021-11-14 23:59:52', 'e14debef-fe03-479f-a4dc-c474a40eb009'),
(191, 'craft', 'm210829_000000_element_index_tweak', '2021-11-14 23:59:53', '2021-11-14 23:59:53', '2021-11-14 23:59:53', 'd0819f1d-a3c6-4b8f-959e-6f2b6d618cac');

-- --------------------------------------------------------

--
-- Table structure for table `blg_plugins`
--

CREATE TABLE `blg_plugins` (
  `id` int NOT NULL,
  `handle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `version` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `schemaVersion` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `licenseKeyStatus` enum('valid','trial','invalid','mismatched','astray','unknown') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'unknown',
  `licensedEdition` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `installDate` datetime NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `blg_projectconfig`
--

CREATE TABLE `blg_projectconfig` (
  `path` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `blg_projectconfig`
--

INSERT INTO `blg_projectconfig` (`path`, `value`) VALUES
('dateModified', '1637002168'),
('email.fromEmail', '\"laviniamanzanares@gmail.com\"'),
('email.fromName', '\"blogcraft.test\"'),
('email.transportType', '\"craft\\\\mail\\\\transportadapters\\\\Sendmail\"'),
('entryTypes.ed9613bd-7583-4119-bfca-b7024788cf9d.fieldLayouts.519aa450-ab54-4f79-ac62-c3e17c2e5329.tabs.0.elements.0.autocapitalize', 'true'),
('entryTypes.ed9613bd-7583-4119-bfca-b7024788cf9d.fieldLayouts.519aa450-ab54-4f79-ac62-c3e17c2e5329.tabs.0.elements.0.autocomplete', 'false'),
('entryTypes.ed9613bd-7583-4119-bfca-b7024788cf9d.fieldLayouts.519aa450-ab54-4f79-ac62-c3e17c2e5329.tabs.0.elements.0.autocorrect', 'true'),
('entryTypes.ed9613bd-7583-4119-bfca-b7024788cf9d.fieldLayouts.519aa450-ab54-4f79-ac62-c3e17c2e5329.tabs.0.elements.0.class', 'null'),
('entryTypes.ed9613bd-7583-4119-bfca-b7024788cf9d.fieldLayouts.519aa450-ab54-4f79-ac62-c3e17c2e5329.tabs.0.elements.0.disabled', 'false'),
('entryTypes.ed9613bd-7583-4119-bfca-b7024788cf9d.fieldLayouts.519aa450-ab54-4f79-ac62-c3e17c2e5329.tabs.0.elements.0.id', 'null'),
('entryTypes.ed9613bd-7583-4119-bfca-b7024788cf9d.fieldLayouts.519aa450-ab54-4f79-ac62-c3e17c2e5329.tabs.0.elements.0.instructions', '\"\"'),
('entryTypes.ed9613bd-7583-4119-bfca-b7024788cf9d.fieldLayouts.519aa450-ab54-4f79-ac62-c3e17c2e5329.tabs.0.elements.0.label', '\"\"'),
('entryTypes.ed9613bd-7583-4119-bfca-b7024788cf9d.fieldLayouts.519aa450-ab54-4f79-ac62-c3e17c2e5329.tabs.0.elements.0.max', 'null'),
('entryTypes.ed9613bd-7583-4119-bfca-b7024788cf9d.fieldLayouts.519aa450-ab54-4f79-ac62-c3e17c2e5329.tabs.0.elements.0.min', 'null'),
('entryTypes.ed9613bd-7583-4119-bfca-b7024788cf9d.fieldLayouts.519aa450-ab54-4f79-ac62-c3e17c2e5329.tabs.0.elements.0.name', 'null'),
('entryTypes.ed9613bd-7583-4119-bfca-b7024788cf9d.fieldLayouts.519aa450-ab54-4f79-ac62-c3e17c2e5329.tabs.0.elements.0.orientation', 'null'),
('entryTypes.ed9613bd-7583-4119-bfca-b7024788cf9d.fieldLayouts.519aa450-ab54-4f79-ac62-c3e17c2e5329.tabs.0.elements.0.placeholder', 'null'),
('entryTypes.ed9613bd-7583-4119-bfca-b7024788cf9d.fieldLayouts.519aa450-ab54-4f79-ac62-c3e17c2e5329.tabs.0.elements.0.readonly', 'false'),
('entryTypes.ed9613bd-7583-4119-bfca-b7024788cf9d.fieldLayouts.519aa450-ab54-4f79-ac62-c3e17c2e5329.tabs.0.elements.0.requirable', 'false'),
('entryTypes.ed9613bd-7583-4119-bfca-b7024788cf9d.fieldLayouts.519aa450-ab54-4f79-ac62-c3e17c2e5329.tabs.0.elements.0.size', 'null'),
('entryTypes.ed9613bd-7583-4119-bfca-b7024788cf9d.fieldLayouts.519aa450-ab54-4f79-ac62-c3e17c2e5329.tabs.0.elements.0.step', 'null'),
('entryTypes.ed9613bd-7583-4119-bfca-b7024788cf9d.fieldLayouts.519aa450-ab54-4f79-ac62-c3e17c2e5329.tabs.0.elements.0.tip', 'null'),
('entryTypes.ed9613bd-7583-4119-bfca-b7024788cf9d.fieldLayouts.519aa450-ab54-4f79-ac62-c3e17c2e5329.tabs.0.elements.0.title', 'null'),
('entryTypes.ed9613bd-7583-4119-bfca-b7024788cf9d.fieldLayouts.519aa450-ab54-4f79-ac62-c3e17c2e5329.tabs.0.elements.0.type', '\"craft\\\\fieldlayoutelements\\\\EntryTitleField\"'),
('entryTypes.ed9613bd-7583-4119-bfca-b7024788cf9d.fieldLayouts.519aa450-ab54-4f79-ac62-c3e17c2e5329.tabs.0.elements.0.warning', 'null'),
('entryTypes.ed9613bd-7583-4119-bfca-b7024788cf9d.fieldLayouts.519aa450-ab54-4f79-ac62-c3e17c2e5329.tabs.0.elements.0.width', '100'),
('entryTypes.ed9613bd-7583-4119-bfca-b7024788cf9d.fieldLayouts.519aa450-ab54-4f79-ac62-c3e17c2e5329.tabs.0.elements.1.fieldUid', '\"a9263f26-bca2-4ef1-b137-70ca86db0b2b\"'),
('entryTypes.ed9613bd-7583-4119-bfca-b7024788cf9d.fieldLayouts.519aa450-ab54-4f79-ac62-c3e17c2e5329.tabs.0.elements.1.instructions', '\"\"'),
('entryTypes.ed9613bd-7583-4119-bfca-b7024788cf9d.fieldLayouts.519aa450-ab54-4f79-ac62-c3e17c2e5329.tabs.0.elements.1.label', '\"Title\"'),
('entryTypes.ed9613bd-7583-4119-bfca-b7024788cf9d.fieldLayouts.519aa450-ab54-4f79-ac62-c3e17c2e5329.tabs.0.elements.1.required', '\"1\"'),
('entryTypes.ed9613bd-7583-4119-bfca-b7024788cf9d.fieldLayouts.519aa450-ab54-4f79-ac62-c3e17c2e5329.tabs.0.elements.1.tip', 'null'),
('entryTypes.ed9613bd-7583-4119-bfca-b7024788cf9d.fieldLayouts.519aa450-ab54-4f79-ac62-c3e17c2e5329.tabs.0.elements.1.type', '\"craft\\\\fieldlayoutelements\\\\CustomField\"'),
('entryTypes.ed9613bd-7583-4119-bfca-b7024788cf9d.fieldLayouts.519aa450-ab54-4f79-ac62-c3e17c2e5329.tabs.0.elements.1.warning', 'null'),
('entryTypes.ed9613bd-7583-4119-bfca-b7024788cf9d.fieldLayouts.519aa450-ab54-4f79-ac62-c3e17c2e5329.tabs.0.elements.1.width', '100'),
('entryTypes.ed9613bd-7583-4119-bfca-b7024788cf9d.fieldLayouts.519aa450-ab54-4f79-ac62-c3e17c2e5329.tabs.0.elements.2.fieldUid', '\"0e36f6df-114e-4c36-996f-183787e38f90\"'),
('entryTypes.ed9613bd-7583-4119-bfca-b7024788cf9d.fieldLayouts.519aa450-ab54-4f79-ac62-c3e17c2e5329.tabs.0.elements.2.instructions', '\"\"'),
('entryTypes.ed9613bd-7583-4119-bfca-b7024788cf9d.fieldLayouts.519aa450-ab54-4f79-ac62-c3e17c2e5329.tabs.0.elements.2.label', '\"\"'),
('entryTypes.ed9613bd-7583-4119-bfca-b7024788cf9d.fieldLayouts.519aa450-ab54-4f79-ac62-c3e17c2e5329.tabs.0.elements.2.required', '\"1\"'),
('entryTypes.ed9613bd-7583-4119-bfca-b7024788cf9d.fieldLayouts.519aa450-ab54-4f79-ac62-c3e17c2e5329.tabs.0.elements.2.tip', 'null'),
('entryTypes.ed9613bd-7583-4119-bfca-b7024788cf9d.fieldLayouts.519aa450-ab54-4f79-ac62-c3e17c2e5329.tabs.0.elements.2.type', '\"craft\\\\fieldlayoutelements\\\\CustomField\"'),
('entryTypes.ed9613bd-7583-4119-bfca-b7024788cf9d.fieldLayouts.519aa450-ab54-4f79-ac62-c3e17c2e5329.tabs.0.elements.2.warning', 'null'),
('entryTypes.ed9613bd-7583-4119-bfca-b7024788cf9d.fieldLayouts.519aa450-ab54-4f79-ac62-c3e17c2e5329.tabs.0.elements.2.width', '100'),
('entryTypes.ed9613bd-7583-4119-bfca-b7024788cf9d.fieldLayouts.519aa450-ab54-4f79-ac62-c3e17c2e5329.tabs.0.elements.3.fieldUid', '\"313863d8-5395-4fa8-9bad-eee5018c0ae9\"'),
('entryTypes.ed9613bd-7583-4119-bfca-b7024788cf9d.fieldLayouts.519aa450-ab54-4f79-ac62-c3e17c2e5329.tabs.0.elements.3.instructions', 'null'),
('entryTypes.ed9613bd-7583-4119-bfca-b7024788cf9d.fieldLayouts.519aa450-ab54-4f79-ac62-c3e17c2e5329.tabs.0.elements.3.label', 'null'),
('entryTypes.ed9613bd-7583-4119-bfca-b7024788cf9d.fieldLayouts.519aa450-ab54-4f79-ac62-c3e17c2e5329.tabs.0.elements.3.required', 'false'),
('entryTypes.ed9613bd-7583-4119-bfca-b7024788cf9d.fieldLayouts.519aa450-ab54-4f79-ac62-c3e17c2e5329.tabs.0.elements.3.tip', 'null'),
('entryTypes.ed9613bd-7583-4119-bfca-b7024788cf9d.fieldLayouts.519aa450-ab54-4f79-ac62-c3e17c2e5329.tabs.0.elements.3.type', '\"craft\\\\fieldlayoutelements\\\\CustomField\"'),
('entryTypes.ed9613bd-7583-4119-bfca-b7024788cf9d.fieldLayouts.519aa450-ab54-4f79-ac62-c3e17c2e5329.tabs.0.elements.3.warning', 'null'),
('entryTypes.ed9613bd-7583-4119-bfca-b7024788cf9d.fieldLayouts.519aa450-ab54-4f79-ac62-c3e17c2e5329.tabs.0.elements.3.width', '100'),
('entryTypes.ed9613bd-7583-4119-bfca-b7024788cf9d.fieldLayouts.519aa450-ab54-4f79-ac62-c3e17c2e5329.tabs.0.elements.4.fieldUid', '\"245f7576-c80c-414b-bc21-6ad15807d7a2\"'),
('entryTypes.ed9613bd-7583-4119-bfca-b7024788cf9d.fieldLayouts.519aa450-ab54-4f79-ac62-c3e17c2e5329.tabs.0.elements.4.instructions', 'null'),
('entryTypes.ed9613bd-7583-4119-bfca-b7024788cf9d.fieldLayouts.519aa450-ab54-4f79-ac62-c3e17c2e5329.tabs.0.elements.4.label', 'null'),
('entryTypes.ed9613bd-7583-4119-bfca-b7024788cf9d.fieldLayouts.519aa450-ab54-4f79-ac62-c3e17c2e5329.tabs.0.elements.4.required', 'false'),
('entryTypes.ed9613bd-7583-4119-bfca-b7024788cf9d.fieldLayouts.519aa450-ab54-4f79-ac62-c3e17c2e5329.tabs.0.elements.4.tip', 'null'),
('entryTypes.ed9613bd-7583-4119-bfca-b7024788cf9d.fieldLayouts.519aa450-ab54-4f79-ac62-c3e17c2e5329.tabs.0.elements.4.type', '\"craft\\\\fieldlayoutelements\\\\CustomField\"'),
('entryTypes.ed9613bd-7583-4119-bfca-b7024788cf9d.fieldLayouts.519aa450-ab54-4f79-ac62-c3e17c2e5329.tabs.0.elements.4.warning', 'null'),
('entryTypes.ed9613bd-7583-4119-bfca-b7024788cf9d.fieldLayouts.519aa450-ab54-4f79-ac62-c3e17c2e5329.tabs.0.elements.4.width', '100'),
('entryTypes.ed9613bd-7583-4119-bfca-b7024788cf9d.fieldLayouts.519aa450-ab54-4f79-ac62-c3e17c2e5329.tabs.0.elements.5.fieldUid', '\"a67d916f-adcc-4c77-ab98-3c4f66ad26e8\"'),
('entryTypes.ed9613bd-7583-4119-bfca-b7024788cf9d.fieldLayouts.519aa450-ab54-4f79-ac62-c3e17c2e5329.tabs.0.elements.5.instructions', '\"\"'),
('entryTypes.ed9613bd-7583-4119-bfca-b7024788cf9d.fieldLayouts.519aa450-ab54-4f79-ac62-c3e17c2e5329.tabs.0.elements.5.label', '\"Set as featured post?\"'),
('entryTypes.ed9613bd-7583-4119-bfca-b7024788cf9d.fieldLayouts.519aa450-ab54-4f79-ac62-c3e17c2e5329.tabs.0.elements.5.required', '\"\"'),
('entryTypes.ed9613bd-7583-4119-bfca-b7024788cf9d.fieldLayouts.519aa450-ab54-4f79-ac62-c3e17c2e5329.tabs.0.elements.5.tip', 'null'),
('entryTypes.ed9613bd-7583-4119-bfca-b7024788cf9d.fieldLayouts.519aa450-ab54-4f79-ac62-c3e17c2e5329.tabs.0.elements.5.type', '\"craft\\\\fieldlayoutelements\\\\CustomField\"'),
('entryTypes.ed9613bd-7583-4119-bfca-b7024788cf9d.fieldLayouts.519aa450-ab54-4f79-ac62-c3e17c2e5329.tabs.0.elements.5.warning', 'null'),
('entryTypes.ed9613bd-7583-4119-bfca-b7024788cf9d.fieldLayouts.519aa450-ab54-4f79-ac62-c3e17c2e5329.tabs.0.elements.5.width', '100'),
('entryTypes.ed9613bd-7583-4119-bfca-b7024788cf9d.fieldLayouts.519aa450-ab54-4f79-ac62-c3e17c2e5329.tabs.0.elements.6.fieldUid', '\"485bf76e-a338-41b7-b86c-9000a6c4ffcc\"'),
('entryTypes.ed9613bd-7583-4119-bfca-b7024788cf9d.fieldLayouts.519aa450-ab54-4f79-ac62-c3e17c2e5329.tabs.0.elements.6.instructions', '\"\"'),
('entryTypes.ed9613bd-7583-4119-bfca-b7024788cf9d.fieldLayouts.519aa450-ab54-4f79-ac62-c3e17c2e5329.tabs.0.elements.6.label', '\"\"'),
('entryTypes.ed9613bd-7583-4119-bfca-b7024788cf9d.fieldLayouts.519aa450-ab54-4f79-ac62-c3e17c2e5329.tabs.0.elements.6.required', '\"\"'),
('entryTypes.ed9613bd-7583-4119-bfca-b7024788cf9d.fieldLayouts.519aa450-ab54-4f79-ac62-c3e17c2e5329.tabs.0.elements.6.tip', 'null'),
('entryTypes.ed9613bd-7583-4119-bfca-b7024788cf9d.fieldLayouts.519aa450-ab54-4f79-ac62-c3e17c2e5329.tabs.0.elements.6.type', '\"craft\\\\fieldlayoutelements\\\\CustomField\"'),
('entryTypes.ed9613bd-7583-4119-bfca-b7024788cf9d.fieldLayouts.519aa450-ab54-4f79-ac62-c3e17c2e5329.tabs.0.elements.6.warning', 'null'),
('entryTypes.ed9613bd-7583-4119-bfca-b7024788cf9d.fieldLayouts.519aa450-ab54-4f79-ac62-c3e17c2e5329.tabs.0.elements.6.width', '100'),
('entryTypes.ed9613bd-7583-4119-bfca-b7024788cf9d.fieldLayouts.519aa450-ab54-4f79-ac62-c3e17c2e5329.tabs.0.elements.7.fieldUid', '\"5a9d08f1-24ab-431e-bbed-f882d42ee70e\"'),
('entryTypes.ed9613bd-7583-4119-bfca-b7024788cf9d.fieldLayouts.519aa450-ab54-4f79-ac62-c3e17c2e5329.tabs.0.elements.7.instructions', '\"\"'),
('entryTypes.ed9613bd-7583-4119-bfca-b7024788cf9d.fieldLayouts.519aa450-ab54-4f79-ac62-c3e17c2e5329.tabs.0.elements.7.label', '\"\"'),
('entryTypes.ed9613bd-7583-4119-bfca-b7024788cf9d.fieldLayouts.519aa450-ab54-4f79-ac62-c3e17c2e5329.tabs.0.elements.7.required', '\"\"'),
('entryTypes.ed9613bd-7583-4119-bfca-b7024788cf9d.fieldLayouts.519aa450-ab54-4f79-ac62-c3e17c2e5329.tabs.0.elements.7.tip', 'null'),
('entryTypes.ed9613bd-7583-4119-bfca-b7024788cf9d.fieldLayouts.519aa450-ab54-4f79-ac62-c3e17c2e5329.tabs.0.elements.7.type', '\"craft\\\\fieldlayoutelements\\\\CustomField\"'),
('entryTypes.ed9613bd-7583-4119-bfca-b7024788cf9d.fieldLayouts.519aa450-ab54-4f79-ac62-c3e17c2e5329.tabs.0.elements.7.warning', 'null'),
('entryTypes.ed9613bd-7583-4119-bfca-b7024788cf9d.fieldLayouts.519aa450-ab54-4f79-ac62-c3e17c2e5329.tabs.0.elements.7.width', '100'),
('entryTypes.ed9613bd-7583-4119-bfca-b7024788cf9d.fieldLayouts.519aa450-ab54-4f79-ac62-c3e17c2e5329.tabs.0.name', '\"Content\"'),
('entryTypes.ed9613bd-7583-4119-bfca-b7024788cf9d.fieldLayouts.519aa450-ab54-4f79-ac62-c3e17c2e5329.tabs.0.sortOrder', '1'),
('entryTypes.ed9613bd-7583-4119-bfca-b7024788cf9d.handle', '\"articles\"'),
('entryTypes.ed9613bd-7583-4119-bfca-b7024788cf9d.hasTitleField', 'false'),
('entryTypes.ed9613bd-7583-4119-bfca-b7024788cf9d.name', '\"Articles\"'),
('entryTypes.ed9613bd-7583-4119-bfca-b7024788cf9d.section', '\"7f3a3d16-e578-421a-991a-91842208b384\"'),
('entryTypes.ed9613bd-7583-4119-bfca-b7024788cf9d.sortOrder', '1'),
('entryTypes.ed9613bd-7583-4119-bfca-b7024788cf9d.titleFormat', '\"{object.articleTitle|raw}\"'),
('entryTypes.ed9613bd-7583-4119-bfca-b7024788cf9d.titleTranslationKeyFormat', 'null'),
('entryTypes.ed9613bd-7583-4119-bfca-b7024788cf9d.titleTranslationMethod', '\"site\"'),
('fieldGroups.4829e301-dddf-4d10-9dc9-a3161fcd74dc.name', '\"Articles\"'),
('fieldGroups.54c2020e-34dd-4a49-a26c-02ad919637a9.name', '\"Common\"'),
('fields.0e36f6df-114e-4c36-996f-183787e38f90.columnSuffix', '\"oebprhdp\"'),
('fields.0e36f6df-114e-4c36-996f-183787e38f90.contentColumnType', '\"text\"'),
('fields.0e36f6df-114e-4c36-996f-183787e38f90.fieldGroup', '\"4829e301-dddf-4d10-9dc9-a3161fcd74dc\"'),
('fields.0e36f6df-114e-4c36-996f-183787e38f90.handle', '\"excerpt\"'),
('fields.0e36f6df-114e-4c36-996f-183787e38f90.instructions', '\"\"'),
('fields.0e36f6df-114e-4c36-996f-183787e38f90.name', '\"Excerpt\"'),
('fields.0e36f6df-114e-4c36-996f-183787e38f90.searchable', 'false'),
('fields.0e36f6df-114e-4c36-996f-183787e38f90.settings.byteLimit', 'null'),
('fields.0e36f6df-114e-4c36-996f-183787e38f90.settings.charLimit', 'null'),
('fields.0e36f6df-114e-4c36-996f-183787e38f90.settings.code', '\"\"'),
('fields.0e36f6df-114e-4c36-996f-183787e38f90.settings.columnType', '\"text\"'),
('fields.0e36f6df-114e-4c36-996f-183787e38f90.settings.initialRows', '\"4\"'),
('fields.0e36f6df-114e-4c36-996f-183787e38f90.settings.multiline', '\"1\"'),
('fields.0e36f6df-114e-4c36-996f-183787e38f90.settings.placeholder', 'null'),
('fields.0e36f6df-114e-4c36-996f-183787e38f90.settings.uiMode', '\"enlarged\"'),
('fields.0e36f6df-114e-4c36-996f-183787e38f90.translationKeyFormat', 'null'),
('fields.0e36f6df-114e-4c36-996f-183787e38f90.translationMethod', '\"none\"'),
('fields.0e36f6df-114e-4c36-996f-183787e38f90.type', '\"craft\\\\fields\\\\PlainText\"'),
('fields.245f7576-c80c-414b-bc21-6ad15807d7a2.columnSuffix', 'null'),
('fields.245f7576-c80c-414b-bc21-6ad15807d7a2.contentColumnType', '\"string\"'),
('fields.245f7576-c80c-414b-bc21-6ad15807d7a2.fieldGroup', '\"4829e301-dddf-4d10-9dc9-a3161fcd74dc\"'),
('fields.245f7576-c80c-414b-bc21-6ad15807d7a2.handle', '\"tag\"'),
('fields.245f7576-c80c-414b-bc21-6ad15807d7a2.instructions', '\"\"'),
('fields.245f7576-c80c-414b-bc21-6ad15807d7a2.name', '\"Tag\"'),
('fields.245f7576-c80c-414b-bc21-6ad15807d7a2.searchable', 'false'),
('fields.245f7576-c80c-414b-bc21-6ad15807d7a2.settings.allowLimit', 'false'),
('fields.245f7576-c80c-414b-bc21-6ad15807d7a2.settings.allowMultipleSources', 'false'),
('fields.245f7576-c80c-414b-bc21-6ad15807d7a2.settings.allowSelfRelations', 'false'),
('fields.245f7576-c80c-414b-bc21-6ad15807d7a2.settings.limit', 'null'),
('fields.245f7576-c80c-414b-bc21-6ad15807d7a2.settings.localizeRelations', 'false'),
('fields.245f7576-c80c-414b-bc21-6ad15807d7a2.settings.selectionLabel', '\"\"'),
('fields.245f7576-c80c-414b-bc21-6ad15807d7a2.settings.showSiteMenu', 'true'),
('fields.245f7576-c80c-414b-bc21-6ad15807d7a2.settings.source', '\"taggroup:4335f424-4db1-4fc1-b173-3a3d2cad684e\"'),
('fields.245f7576-c80c-414b-bc21-6ad15807d7a2.settings.sources', '\"*\"'),
('fields.245f7576-c80c-414b-bc21-6ad15807d7a2.settings.targetSiteId', 'null'),
('fields.245f7576-c80c-414b-bc21-6ad15807d7a2.settings.validateRelatedElements', 'false'),
('fields.245f7576-c80c-414b-bc21-6ad15807d7a2.settings.viewMode', 'null'),
('fields.245f7576-c80c-414b-bc21-6ad15807d7a2.translationKeyFormat', 'null'),
('fields.245f7576-c80c-414b-bc21-6ad15807d7a2.translationMethod', '\"site\"'),
('fields.245f7576-c80c-414b-bc21-6ad15807d7a2.type', '\"craft\\\\fields\\\\Tags\"'),
('fields.313863d8-5395-4fa8-9bad-eee5018c0ae9.columnSuffix', 'null'),
('fields.313863d8-5395-4fa8-9bad-eee5018c0ae9.contentColumnType', '\"string\"'),
('fields.313863d8-5395-4fa8-9bad-eee5018c0ae9.fieldGroup', '\"4829e301-dddf-4d10-9dc9-a3161fcd74dc\"'),
('fields.313863d8-5395-4fa8-9bad-eee5018c0ae9.handle', '\"image\"'),
('fields.313863d8-5395-4fa8-9bad-eee5018c0ae9.instructions', '\"\"'),
('fields.313863d8-5395-4fa8-9bad-eee5018c0ae9.name', '\"Image\"'),
('fields.313863d8-5395-4fa8-9bad-eee5018c0ae9.searchable', 'false'),
('fields.313863d8-5395-4fa8-9bad-eee5018c0ae9.settings.allowedKinds.0', '\"image\"'),
('fields.313863d8-5395-4fa8-9bad-eee5018c0ae9.settings.allowSelfRelations', 'false'),
('fields.313863d8-5395-4fa8-9bad-eee5018c0ae9.settings.allowUploads', 'true'),
('fields.313863d8-5395-4fa8-9bad-eee5018c0ae9.settings.defaultUploadLocationSource', '\"volume:f38e29ae-f631-4ce5-a112-5d7cd773e4fa\"'),
('fields.313863d8-5395-4fa8-9bad-eee5018c0ae9.settings.defaultUploadLocationSubpath', '\"\"'),
('fields.313863d8-5395-4fa8-9bad-eee5018c0ae9.settings.limit', '\"1\"'),
('fields.313863d8-5395-4fa8-9bad-eee5018c0ae9.settings.localizeRelations', 'false'),
('fields.313863d8-5395-4fa8-9bad-eee5018c0ae9.settings.previewMode', '\"full\"'),
('fields.313863d8-5395-4fa8-9bad-eee5018c0ae9.settings.restrictFiles', '\"1\"'),
('fields.313863d8-5395-4fa8-9bad-eee5018c0ae9.settings.selectionLabel', '\"\"'),
('fields.313863d8-5395-4fa8-9bad-eee5018c0ae9.settings.showSiteMenu', 'true'),
('fields.313863d8-5395-4fa8-9bad-eee5018c0ae9.settings.showUnpermittedFiles', 'false'),
('fields.313863d8-5395-4fa8-9bad-eee5018c0ae9.settings.showUnpermittedVolumes', 'false'),
('fields.313863d8-5395-4fa8-9bad-eee5018c0ae9.settings.singleUploadLocationSource', '\"volume:f38e29ae-f631-4ce5-a112-5d7cd773e4fa\"'),
('fields.313863d8-5395-4fa8-9bad-eee5018c0ae9.settings.singleUploadLocationSubpath', '\"\"'),
('fields.313863d8-5395-4fa8-9bad-eee5018c0ae9.settings.source', 'null'),
('fields.313863d8-5395-4fa8-9bad-eee5018c0ae9.settings.sources', '\"*\"'),
('fields.313863d8-5395-4fa8-9bad-eee5018c0ae9.settings.targetSiteId', 'null'),
('fields.313863d8-5395-4fa8-9bad-eee5018c0ae9.settings.useSingleFolder', 'false'),
('fields.313863d8-5395-4fa8-9bad-eee5018c0ae9.settings.validateRelatedElements', 'false'),
('fields.313863d8-5395-4fa8-9bad-eee5018c0ae9.settings.viewMode', '\"large\"'),
('fields.313863d8-5395-4fa8-9bad-eee5018c0ae9.translationKeyFormat', 'null'),
('fields.313863d8-5395-4fa8-9bad-eee5018c0ae9.translationMethod', '\"site\"'),
('fields.313863d8-5395-4fa8-9bad-eee5018c0ae9.type', '\"craft\\\\fields\\\\Assets\"'),
('fields.485bf76e-a338-41b7-b86c-9000a6c4ffcc.columnSuffix', '\"mksqzuki\"'),
('fields.485bf76e-a338-41b7-b86c-9000a6c4ffcc.contentColumnType', '\"integer(10)\"'),
('fields.485bf76e-a338-41b7-b86c-9000a6c4ffcc.fieldGroup', '\"4829e301-dddf-4d10-9dc9-a3161fcd74dc\"'),
('fields.485bf76e-a338-41b7-b86c-9000a6c4ffcc.handle', '\"likes\"'),
('fields.485bf76e-a338-41b7-b86c-9000a6c4ffcc.instructions', '\"\"'),
('fields.485bf76e-a338-41b7-b86c-9000a6c4ffcc.name', '\"Likes\"'),
('fields.485bf76e-a338-41b7-b86c-9000a6c4ffcc.searchable', 'false'),
('fields.485bf76e-a338-41b7-b86c-9000a6c4ffcc.settings.decimals', '0'),
('fields.485bf76e-a338-41b7-b86c-9000a6c4ffcc.settings.defaultValue', 'null'),
('fields.485bf76e-a338-41b7-b86c-9000a6c4ffcc.settings.max', 'null'),
('fields.485bf76e-a338-41b7-b86c-9000a6c4ffcc.settings.min', '\"0\"'),
('fields.485bf76e-a338-41b7-b86c-9000a6c4ffcc.settings.prefix', 'null'),
('fields.485bf76e-a338-41b7-b86c-9000a6c4ffcc.settings.previewCurrency', '\"\"'),
('fields.485bf76e-a338-41b7-b86c-9000a6c4ffcc.settings.previewFormat', '\"decimal\"'),
('fields.485bf76e-a338-41b7-b86c-9000a6c4ffcc.settings.size', 'null'),
('fields.485bf76e-a338-41b7-b86c-9000a6c4ffcc.settings.suffix', 'null'),
('fields.485bf76e-a338-41b7-b86c-9000a6c4ffcc.translationKeyFormat', 'null'),
('fields.485bf76e-a338-41b7-b86c-9000a6c4ffcc.translationMethod', '\"none\"'),
('fields.485bf76e-a338-41b7-b86c-9000a6c4ffcc.type', '\"craft\\\\fields\\\\Number\"'),
('fields.5a9d08f1-24ab-431e-bbed-f882d42ee70e.columnSuffix', '\"ccmcxfrr\"'),
('fields.5a9d08f1-24ab-431e-bbed-f882d42ee70e.contentColumnType', '\"integer(10)\"'),
('fields.5a9d08f1-24ab-431e-bbed-f882d42ee70e.fieldGroup', '\"4829e301-dddf-4d10-9dc9-a3161fcd74dc\"'),
('fields.5a9d08f1-24ab-431e-bbed-f882d42ee70e.handle', '\"views\"'),
('fields.5a9d08f1-24ab-431e-bbed-f882d42ee70e.instructions', '\"\"'),
('fields.5a9d08f1-24ab-431e-bbed-f882d42ee70e.name', '\"Views\"'),
('fields.5a9d08f1-24ab-431e-bbed-f882d42ee70e.searchable', 'false'),
('fields.5a9d08f1-24ab-431e-bbed-f882d42ee70e.settings.decimals', '0'),
('fields.5a9d08f1-24ab-431e-bbed-f882d42ee70e.settings.defaultValue', 'null'),
('fields.5a9d08f1-24ab-431e-bbed-f882d42ee70e.settings.max', 'null'),
('fields.5a9d08f1-24ab-431e-bbed-f882d42ee70e.settings.min', '\"0\"'),
('fields.5a9d08f1-24ab-431e-bbed-f882d42ee70e.settings.prefix', 'null'),
('fields.5a9d08f1-24ab-431e-bbed-f882d42ee70e.settings.previewCurrency', '\"\"'),
('fields.5a9d08f1-24ab-431e-bbed-f882d42ee70e.settings.previewFormat', '\"decimal\"'),
('fields.5a9d08f1-24ab-431e-bbed-f882d42ee70e.settings.size', 'null'),
('fields.5a9d08f1-24ab-431e-bbed-f882d42ee70e.settings.suffix', 'null'),
('fields.5a9d08f1-24ab-431e-bbed-f882d42ee70e.translationKeyFormat', 'null'),
('fields.5a9d08f1-24ab-431e-bbed-f882d42ee70e.translationMethod', '\"none\"'),
('fields.5a9d08f1-24ab-431e-bbed-f882d42ee70e.type', '\"craft\\\\fields\\\\Number\"'),
('fields.a67d916f-adcc-4c77-ab98-3c4f66ad26e8.columnSuffix', '\"qjffftaa\"'),
('fields.a67d916f-adcc-4c77-ab98-3c4f66ad26e8.contentColumnType', '\"boolean\"'),
('fields.a67d916f-adcc-4c77-ab98-3c4f66ad26e8.fieldGroup', '\"4829e301-dddf-4d10-9dc9-a3161fcd74dc\"'),
('fields.a67d916f-adcc-4c77-ab98-3c4f66ad26e8.handle', '\"featured\"'),
('fields.a67d916f-adcc-4c77-ab98-3c4f66ad26e8.instructions', '\"\"'),
('fields.a67d916f-adcc-4c77-ab98-3c4f66ad26e8.name', '\"Featured\"'),
('fields.a67d916f-adcc-4c77-ab98-3c4f66ad26e8.searchable', 'false'),
('fields.a67d916f-adcc-4c77-ab98-3c4f66ad26e8.settings.default', 'false'),
('fields.a67d916f-adcc-4c77-ab98-3c4f66ad26e8.settings.offLabel', 'null'),
('fields.a67d916f-adcc-4c77-ab98-3c4f66ad26e8.settings.onLabel', 'null'),
('fields.a67d916f-adcc-4c77-ab98-3c4f66ad26e8.translationKeyFormat', 'null'),
('fields.a67d916f-adcc-4c77-ab98-3c4f66ad26e8.translationMethod', '\"none\"'),
('fields.a67d916f-adcc-4c77-ab98-3c4f66ad26e8.type', '\"craft\\\\fields\\\\Lightswitch\"'),
('fields.a9263f26-bca2-4ef1-b137-70ca86db0b2b.columnSuffix', '\"gaxmcqla\"'),
('fields.a9263f26-bca2-4ef1-b137-70ca86db0b2b.contentColumnType', '\"string\"'),
('fields.a9263f26-bca2-4ef1-b137-70ca86db0b2b.fieldGroup', '\"4829e301-dddf-4d10-9dc9-a3161fcd74dc\"'),
('fields.a9263f26-bca2-4ef1-b137-70ca86db0b2b.handle', '\"articleTitle\"'),
('fields.a9263f26-bca2-4ef1-b137-70ca86db0b2b.instructions', '\"\"'),
('fields.a9263f26-bca2-4ef1-b137-70ca86db0b2b.name', '\"Article Title\"'),
('fields.a9263f26-bca2-4ef1-b137-70ca86db0b2b.searchable', 'true'),
('fields.a9263f26-bca2-4ef1-b137-70ca86db0b2b.settings.byteLimit', 'null'),
('fields.a9263f26-bca2-4ef1-b137-70ca86db0b2b.settings.charLimit', 'null'),
('fields.a9263f26-bca2-4ef1-b137-70ca86db0b2b.settings.code', '\"\"'),
('fields.a9263f26-bca2-4ef1-b137-70ca86db0b2b.settings.columnType', '\"string\"'),
('fields.a9263f26-bca2-4ef1-b137-70ca86db0b2b.settings.initialRows', '\"4\"'),
('fields.a9263f26-bca2-4ef1-b137-70ca86db0b2b.settings.multiline', '\"\"'),
('fields.a9263f26-bca2-4ef1-b137-70ca86db0b2b.settings.placeholder', 'null'),
('fields.a9263f26-bca2-4ef1-b137-70ca86db0b2b.settings.uiMode', '\"normal\"'),
('fields.a9263f26-bca2-4ef1-b137-70ca86db0b2b.translationKeyFormat', 'null'),
('fields.a9263f26-bca2-4ef1-b137-70ca86db0b2b.translationMethod', '\"none\"'),
('fields.a9263f26-bca2-4ef1-b137-70ca86db0b2b.type', '\"craft\\\\fields\\\\PlainText\"'),
('graphql.publicToken.enabled', 'true'),
('graphql.publicToken.expiryDate', 'null'),
('graphql.schemas.54051863-8de7-40aa-ab6b-4c40ca533c78.isPublic', 'true'),
('graphql.schemas.54051863-8de7-40aa-ab6b-4c40ca533c78.name', '\"Public Schema\"'),
('graphql.schemas.54051863-8de7-40aa-ab6b-4c40ca533c78.scope.0', '\"sections.7f3a3d16-e578-421a-991a-91842208b384:read\"'),
('graphql.schemas.db041b37-83e1-4382-8091-ffc915fef807.isPublic', 'false'),
('graphql.schemas.db041b37-83e1-4382-8091-ffc915fef807.name', '\"articles\"'),
('graphql.schemas.db041b37-83e1-4382-8091-ffc915fef807.scope.0', '\"sections.7f3a3d16-e578-421a-991a-91842208b384:read\"'),
('graphql.schemas.db041b37-83e1-4382-8091-ffc915fef807.scope.1', '\"entrytypes.ed9613bd-7583-4119-bfca-b7024788cf9d:read\"'),
('graphql.schemas.db041b37-83e1-4382-8091-ffc915fef807.scope.2', '\"volumes.f38e29ae-f631-4ce5-a112-5d7cd773e4fa:read\"'),
('graphql.schemas.db041b37-83e1-4382-8091-ffc915fef807.scope.3', '\"usergroups.everyone:read\"'),
('graphql.schemas.db041b37-83e1-4382-8091-ffc915fef807.scope.4', '\"taggroups.4335f424-4db1-4fc1-b173-3a3d2cad684e:read\"'),
('imageTransforms.5cbe3726-c50c-4c54-bb09-3f83d0b51b46.format', '\"jpg\"'),
('imageTransforms.5cbe3726-c50c-4c54-bb09-3f83d0b51b46.handle', '\"postsList\"'),
('imageTransforms.5cbe3726-c50c-4c54-bb09-3f83d0b51b46.height', '379'),
('imageTransforms.5cbe3726-c50c-4c54-bb09-3f83d0b51b46.interlace', '\"none\"'),
('imageTransforms.5cbe3726-c50c-4c54-bb09-3f83d0b51b46.mode', '\"crop\"'),
('imageTransforms.5cbe3726-c50c-4c54-bb09-3f83d0b51b46.name', '\"Posts List\"'),
('imageTransforms.5cbe3726-c50c-4c54-bb09-3f83d0b51b46.position', '\"center-center\"'),
('imageTransforms.5cbe3726-c50c-4c54-bb09-3f83d0b51b46.quality', '60'),
('imageTransforms.5cbe3726-c50c-4c54-bb09-3f83d0b51b46.width', '568'),
('meta.__names__.0e36f6df-114e-4c36-996f-183787e38f90', '\"Excerpt\"'),
('meta.__names__.245f7576-c80c-414b-bc21-6ad15807d7a2', '\"Tag\"'),
('meta.__names__.313863d8-5395-4fa8-9bad-eee5018c0ae9', '\"Image\"'),
('meta.__names__.4335f424-4db1-4fc1-b173-3a3d2cad684e', '\"Article Tags\"'),
('meta.__names__.4829e301-dddf-4d10-9dc9-a3161fcd74dc', '\"Articles\"'),
('meta.__names__.485bf76e-a338-41b7-b86c-9000a6c4ffcc', '\"Likes\"'),
('meta.__names__.54051863-8de7-40aa-ab6b-4c40ca533c78', '\"Public Schema\"'),
('meta.__names__.54c2020e-34dd-4a49-a26c-02ad919637a9', '\"Common\"'),
('meta.__names__.5a9d08f1-24ab-431e-bbed-f882d42ee70e', '\"Views\"'),
('meta.__names__.5cbe3726-c50c-4c54-bb09-3f83d0b51b46', '\"Posts List\"'),
('meta.__names__.7f3a3d16-e578-421a-991a-91842208b384', '\"Articles\"'),
('meta.__names__.a67d916f-adcc-4c77-ab98-3c4f66ad26e8', '\"Featured\"'),
('meta.__names__.a9263f26-bca2-4ef1-b137-70ca86db0b2b', '\"Article Title\"'),
('meta.__names__.c4128e2f-c490-4b54-a451-731ad120bd0c', '\"blogcraft.test\"'),
('meta.__names__.db041b37-83e1-4382-8091-ffc915fef807', '\"articles\"'),
('meta.__names__.ed9613bd-7583-4119-bfca-b7024788cf9d', '\"Articles\"'),
('meta.__names__.f38e29ae-f631-4ce5-a112-5d7cd773e4fa', '\"Uploads\"'),
('meta.__names__.fe3b0bf5-0ec5-4aa7-a9bd-7f56f878127c', '\"blogcraft.test\"'),
('sections.7f3a3d16-e578-421a-991a-91842208b384.defaultPlacement', '\"end\"'),
('sections.7f3a3d16-e578-421a-991a-91842208b384.enableVersioning', 'true'),
('sections.7f3a3d16-e578-421a-991a-91842208b384.handle', '\"articles\"'),
('sections.7f3a3d16-e578-421a-991a-91842208b384.name', '\"Articles\"'),
('sections.7f3a3d16-e578-421a-991a-91842208b384.propagationMethod', '\"all\"'),
('sections.7f3a3d16-e578-421a-991a-91842208b384.siteSettings.fe3b0bf5-0ec5-4aa7-a9bd-7f56f878127c.enabledByDefault', 'true'),
('sections.7f3a3d16-e578-421a-991a-91842208b384.siteSettings.fe3b0bf5-0ec5-4aa7-a9bd-7f56f878127c.hasUrls', 'true'),
('sections.7f3a3d16-e578-421a-991a-91842208b384.siteSettings.fe3b0bf5-0ec5-4aa7-a9bd-7f56f878127c.template', '\"articles/_entry\"'),
('sections.7f3a3d16-e578-421a-991a-91842208b384.siteSettings.fe3b0bf5-0ec5-4aa7-a9bd-7f56f878127c.uriFormat', '\"articles/{{ author.username }}/{{ slug }}\"'),
('sections.7f3a3d16-e578-421a-991a-91842208b384.type', '\"channel\"'),
('siteGroups.c4128e2f-c490-4b54-a451-731ad120bd0c.name', '\"blogcraft.test\"'),
('sites.fe3b0bf5-0ec5-4aa7-a9bd-7f56f878127c.baseUrl', '\"$PRIMARY_SITE_URL\"'),
('sites.fe3b0bf5-0ec5-4aa7-a9bd-7f56f878127c.handle', '\"default\"'),
('sites.fe3b0bf5-0ec5-4aa7-a9bd-7f56f878127c.hasUrls', 'true'),
('sites.fe3b0bf5-0ec5-4aa7-a9bd-7f56f878127c.language', '\"en-US\"'),
('sites.fe3b0bf5-0ec5-4aa7-a9bd-7f56f878127c.name', '\"blogcraft.test\"'),
('sites.fe3b0bf5-0ec5-4aa7-a9bd-7f56f878127c.primary', 'true'),
('sites.fe3b0bf5-0ec5-4aa7-a9bd-7f56f878127c.siteGroup', '\"c4128e2f-c490-4b54-a451-731ad120bd0c\"'),
('sites.fe3b0bf5-0ec5-4aa7-a9bd-7f56f878127c.sortOrder', '1'),
('system.edition', '\"pro\"'),
('system.live', 'true'),
('system.name', '\"My Blog\"'),
('system.retryDuration', 'null'),
('system.schemaVersion', '\"3.7.8\"'),
('system.timeZone', '\"America/Managua\"'),
('tagGroups.4335f424-4db1-4fc1-b173-3a3d2cad684e.fieldLayouts.66fb93af-fcea-4881-8438-f3251a78c7f5.tabs.0.elements.0.autocapitalize', 'true'),
('tagGroups.4335f424-4db1-4fc1-b173-3a3d2cad684e.fieldLayouts.66fb93af-fcea-4881-8438-f3251a78c7f5.tabs.0.elements.0.autocomplete', 'false'),
('tagGroups.4335f424-4db1-4fc1-b173-3a3d2cad684e.fieldLayouts.66fb93af-fcea-4881-8438-f3251a78c7f5.tabs.0.elements.0.autocorrect', 'true'),
('tagGroups.4335f424-4db1-4fc1-b173-3a3d2cad684e.fieldLayouts.66fb93af-fcea-4881-8438-f3251a78c7f5.tabs.0.elements.0.class', 'null'),
('tagGroups.4335f424-4db1-4fc1-b173-3a3d2cad684e.fieldLayouts.66fb93af-fcea-4881-8438-f3251a78c7f5.tabs.0.elements.0.disabled', 'false'),
('tagGroups.4335f424-4db1-4fc1-b173-3a3d2cad684e.fieldLayouts.66fb93af-fcea-4881-8438-f3251a78c7f5.tabs.0.elements.0.id', 'null'),
('tagGroups.4335f424-4db1-4fc1-b173-3a3d2cad684e.fieldLayouts.66fb93af-fcea-4881-8438-f3251a78c7f5.tabs.0.elements.0.instructions', '\"\"'),
('tagGroups.4335f424-4db1-4fc1-b173-3a3d2cad684e.fieldLayouts.66fb93af-fcea-4881-8438-f3251a78c7f5.tabs.0.elements.0.label', '\"Tag Name\"'),
('tagGroups.4335f424-4db1-4fc1-b173-3a3d2cad684e.fieldLayouts.66fb93af-fcea-4881-8438-f3251a78c7f5.tabs.0.elements.0.max', 'null'),
('tagGroups.4335f424-4db1-4fc1-b173-3a3d2cad684e.fieldLayouts.66fb93af-fcea-4881-8438-f3251a78c7f5.tabs.0.elements.0.min', 'null'),
('tagGroups.4335f424-4db1-4fc1-b173-3a3d2cad684e.fieldLayouts.66fb93af-fcea-4881-8438-f3251a78c7f5.tabs.0.elements.0.name', 'null'),
('tagGroups.4335f424-4db1-4fc1-b173-3a3d2cad684e.fieldLayouts.66fb93af-fcea-4881-8438-f3251a78c7f5.tabs.0.elements.0.orientation', 'null'),
('tagGroups.4335f424-4db1-4fc1-b173-3a3d2cad684e.fieldLayouts.66fb93af-fcea-4881-8438-f3251a78c7f5.tabs.0.elements.0.placeholder', 'null'),
('tagGroups.4335f424-4db1-4fc1-b173-3a3d2cad684e.fieldLayouts.66fb93af-fcea-4881-8438-f3251a78c7f5.tabs.0.elements.0.readonly', 'false'),
('tagGroups.4335f424-4db1-4fc1-b173-3a3d2cad684e.fieldLayouts.66fb93af-fcea-4881-8438-f3251a78c7f5.tabs.0.elements.0.requirable', 'false'),
('tagGroups.4335f424-4db1-4fc1-b173-3a3d2cad684e.fieldLayouts.66fb93af-fcea-4881-8438-f3251a78c7f5.tabs.0.elements.0.size', 'null'),
('tagGroups.4335f424-4db1-4fc1-b173-3a3d2cad684e.fieldLayouts.66fb93af-fcea-4881-8438-f3251a78c7f5.tabs.0.elements.0.step', 'null'),
('tagGroups.4335f424-4db1-4fc1-b173-3a3d2cad684e.fieldLayouts.66fb93af-fcea-4881-8438-f3251a78c7f5.tabs.0.elements.0.tip', 'null'),
('tagGroups.4335f424-4db1-4fc1-b173-3a3d2cad684e.fieldLayouts.66fb93af-fcea-4881-8438-f3251a78c7f5.tabs.0.elements.0.title', 'null'),
('tagGroups.4335f424-4db1-4fc1-b173-3a3d2cad684e.fieldLayouts.66fb93af-fcea-4881-8438-f3251a78c7f5.tabs.0.elements.0.type', '\"craft\\\\fieldlayoutelements\\\\TitleField\"'),
('tagGroups.4335f424-4db1-4fc1-b173-3a3d2cad684e.fieldLayouts.66fb93af-fcea-4881-8438-f3251a78c7f5.tabs.0.elements.0.warning', 'null'),
('tagGroups.4335f424-4db1-4fc1-b173-3a3d2cad684e.fieldLayouts.66fb93af-fcea-4881-8438-f3251a78c7f5.tabs.0.elements.0.width', '100'),
('tagGroups.4335f424-4db1-4fc1-b173-3a3d2cad684e.fieldLayouts.66fb93af-fcea-4881-8438-f3251a78c7f5.tabs.0.name', '\"Content\"'),
('tagGroups.4335f424-4db1-4fc1-b173-3a3d2cad684e.fieldLayouts.66fb93af-fcea-4881-8438-f3251a78c7f5.tabs.0.sortOrder', '1'),
('tagGroups.4335f424-4db1-4fc1-b173-3a3d2cad684e.handle', '\"articleTags\"'),
('tagGroups.4335f424-4db1-4fc1-b173-3a3d2cad684e.name', '\"Article Tags\"'),
('users.allowPublicRegistration', 'false'),
('users.defaultGroup', 'null'),
('users.photoSubpath', 'null'),
('users.photoVolumeUid', '\"f38e29ae-f631-4ce5-a112-5d7cd773e4fa\"'),
('users.requireEmailVerification', 'true'),
('volumes.f38e29ae-f631-4ce5-a112-5d7cd773e4fa.fieldLayouts.ef04c9bf-d2da-4b35-aa69-d8b16f2bb443.tabs.0.elements.0.autocapitalize', 'true'),
('volumes.f38e29ae-f631-4ce5-a112-5d7cd773e4fa.fieldLayouts.ef04c9bf-d2da-4b35-aa69-d8b16f2bb443.tabs.0.elements.0.autocomplete', 'false'),
('volumes.f38e29ae-f631-4ce5-a112-5d7cd773e4fa.fieldLayouts.ef04c9bf-d2da-4b35-aa69-d8b16f2bb443.tabs.0.elements.0.autocorrect', 'true'),
('volumes.f38e29ae-f631-4ce5-a112-5d7cd773e4fa.fieldLayouts.ef04c9bf-d2da-4b35-aa69-d8b16f2bb443.tabs.0.elements.0.class', 'null'),
('volumes.f38e29ae-f631-4ce5-a112-5d7cd773e4fa.fieldLayouts.ef04c9bf-d2da-4b35-aa69-d8b16f2bb443.tabs.0.elements.0.disabled', 'false'),
('volumes.f38e29ae-f631-4ce5-a112-5d7cd773e4fa.fieldLayouts.ef04c9bf-d2da-4b35-aa69-d8b16f2bb443.tabs.0.elements.0.id', 'null'),
('volumes.f38e29ae-f631-4ce5-a112-5d7cd773e4fa.fieldLayouts.ef04c9bf-d2da-4b35-aa69-d8b16f2bb443.tabs.0.elements.0.instructions', 'null'),
('volumes.f38e29ae-f631-4ce5-a112-5d7cd773e4fa.fieldLayouts.ef04c9bf-d2da-4b35-aa69-d8b16f2bb443.tabs.0.elements.0.label', 'null'),
('volumes.f38e29ae-f631-4ce5-a112-5d7cd773e4fa.fieldLayouts.ef04c9bf-d2da-4b35-aa69-d8b16f2bb443.tabs.0.elements.0.max', 'null'),
('volumes.f38e29ae-f631-4ce5-a112-5d7cd773e4fa.fieldLayouts.ef04c9bf-d2da-4b35-aa69-d8b16f2bb443.tabs.0.elements.0.min', 'null'),
('volumes.f38e29ae-f631-4ce5-a112-5d7cd773e4fa.fieldLayouts.ef04c9bf-d2da-4b35-aa69-d8b16f2bb443.tabs.0.elements.0.name', 'null'),
('volumes.f38e29ae-f631-4ce5-a112-5d7cd773e4fa.fieldLayouts.ef04c9bf-d2da-4b35-aa69-d8b16f2bb443.tabs.0.elements.0.orientation', 'null'),
('volumes.f38e29ae-f631-4ce5-a112-5d7cd773e4fa.fieldLayouts.ef04c9bf-d2da-4b35-aa69-d8b16f2bb443.tabs.0.elements.0.placeholder', 'null'),
('volumes.f38e29ae-f631-4ce5-a112-5d7cd773e4fa.fieldLayouts.ef04c9bf-d2da-4b35-aa69-d8b16f2bb443.tabs.0.elements.0.readonly', 'false'),
('volumes.f38e29ae-f631-4ce5-a112-5d7cd773e4fa.fieldLayouts.ef04c9bf-d2da-4b35-aa69-d8b16f2bb443.tabs.0.elements.0.requirable', 'false'),
('volumes.f38e29ae-f631-4ce5-a112-5d7cd773e4fa.fieldLayouts.ef04c9bf-d2da-4b35-aa69-d8b16f2bb443.tabs.0.elements.0.size', 'null'),
('volumes.f38e29ae-f631-4ce5-a112-5d7cd773e4fa.fieldLayouts.ef04c9bf-d2da-4b35-aa69-d8b16f2bb443.tabs.0.elements.0.step', 'null'),
('volumes.f38e29ae-f631-4ce5-a112-5d7cd773e4fa.fieldLayouts.ef04c9bf-d2da-4b35-aa69-d8b16f2bb443.tabs.0.elements.0.tip', 'null'),
('volumes.f38e29ae-f631-4ce5-a112-5d7cd773e4fa.fieldLayouts.ef04c9bf-d2da-4b35-aa69-d8b16f2bb443.tabs.0.elements.0.title', 'null'),
('volumes.f38e29ae-f631-4ce5-a112-5d7cd773e4fa.fieldLayouts.ef04c9bf-d2da-4b35-aa69-d8b16f2bb443.tabs.0.elements.0.type', '\"craft\\\\fieldlayoutelements\\\\AssetTitleField\"'),
('volumes.f38e29ae-f631-4ce5-a112-5d7cd773e4fa.fieldLayouts.ef04c9bf-d2da-4b35-aa69-d8b16f2bb443.tabs.0.elements.0.warning', 'null'),
('volumes.f38e29ae-f631-4ce5-a112-5d7cd773e4fa.fieldLayouts.ef04c9bf-d2da-4b35-aa69-d8b16f2bb443.tabs.0.elements.0.width', '100'),
('volumes.f38e29ae-f631-4ce5-a112-5d7cd773e4fa.fieldLayouts.ef04c9bf-d2da-4b35-aa69-d8b16f2bb443.tabs.0.name', '\"Content\"'),
('volumes.f38e29ae-f631-4ce5-a112-5d7cd773e4fa.fieldLayouts.ef04c9bf-d2da-4b35-aa69-d8b16f2bb443.tabs.0.sortOrder', '1'),
('volumes.f38e29ae-f631-4ce5-a112-5d7cd773e4fa.handle', '\"uploads\"'),
('volumes.f38e29ae-f631-4ce5-a112-5d7cd773e4fa.hasUrls', 'true'),
('volumes.f38e29ae-f631-4ce5-a112-5d7cd773e4fa.name', '\"Uploads\"'),
('volumes.f38e29ae-f631-4ce5-a112-5d7cd773e4fa.settings.path', '\"@root/web/uploads\"'),
('volumes.f38e29ae-f631-4ce5-a112-5d7cd773e4fa.sortOrder', '1'),
('volumes.f38e29ae-f631-4ce5-a112-5d7cd773e4fa.titleTranslationKeyFormat', 'null'),
('volumes.f38e29ae-f631-4ce5-a112-5d7cd773e4fa.titleTranslationMethod', 'null'),
('volumes.f38e29ae-f631-4ce5-a112-5d7cd773e4fa.type', '\"craft\\\\volumes\\\\Local\"'),
('volumes.f38e29ae-f631-4ce5-a112-5d7cd773e4fa.url', '\"$UPLOADS_URL\"');

-- --------------------------------------------------------

--
-- Table structure for table `blg_queue`
--

CREATE TABLE `blg_queue` (
  `id` int NOT NULL,
  `channel` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'queue',
  `job` longblob NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `timePushed` int NOT NULL,
  `ttr` int NOT NULL,
  `delay` int NOT NULL DEFAULT '0',
  `priority` int UNSIGNED NOT NULL DEFAULT '1024',
  `dateReserved` datetime DEFAULT NULL,
  `timeUpdated` int DEFAULT NULL,
  `progress` smallint NOT NULL DEFAULT '0',
  `progressLabel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `attempt` int DEFAULT NULL,
  `fail` tinyint(1) DEFAULT '0',
  `dateFailed` datetime DEFAULT NULL,
  `error` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `blg_relations`
--

CREATE TABLE `blg_relations` (
  `id` int NOT NULL,
  `fieldId` int NOT NULL,
  `sourceId` int NOT NULL,
  `sourceSiteId` int DEFAULT NULL,
  `targetId` int NOT NULL,
  `sortOrder` smallint UNSIGNED DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `blg_relations`
--

INSERT INTO `blg_relations` (`id`, `fieldId`, `sourceId`, `sourceSiteId`, `targetId`, `sortOrder`, `dateCreated`, `dateUpdated`, `uid`) VALUES
(3, 2, 2, NULL, 10, 1, '2021-11-15 03:28:50', '2021-11-15 03:28:50', '8ccb2147-26ed-4127-b7e5-49c366651d70'),
(4, 3, 2, NULL, 7, 1, '2021-11-15 03:28:50', '2021-11-15 03:28:50', 'bd521c85-9a40-4d41-9edb-e3ec140862fc'),
(5, 2, 11, NULL, 10, 1, '2021-11-15 03:28:50', '2021-11-15 03:28:50', 'cdc100a1-8f60-45e5-9cd8-da8fb3100031'),
(6, 3, 11, NULL, 7, 1, '2021-11-15 03:28:50', '2021-11-15 03:28:50', '5c2edda1-7fd8-464f-8980-f0d9b5a8276b'),
(7, 2, 12, NULL, 10, 1, '2021-11-15 03:33:52', '2021-11-15 03:33:52', '5cf7cb54-2541-4573-aa0a-fa9c7f987a20'),
(8, 3, 12, NULL, 7, 1, '2021-11-15 03:33:52', '2021-11-15 03:33:52', '26044f50-1867-4800-b619-e5e31a3bf998'),
(9, 2, 14, NULL, 10, 1, '2021-11-15 03:40:39', '2021-11-15 03:40:39', '744bfe93-8ff4-4cd2-a76d-4cbbe64720d7'),
(10, 3, 14, NULL, 7, 1, '2021-11-15 03:40:55', '2021-11-15 03:40:55', '265327e2-9d62-46df-9638-39b5e7a8a744'),
(11, 2, 15, NULL, 10, 1, '2021-11-15 03:41:07', '2021-11-15 03:41:07', '8dec9acd-0a7d-4e86-b101-ee309ed69c3b'),
(12, 3, 15, NULL, 7, 1, '2021-11-15 03:41:07', '2021-11-15 03:41:07', '29bdf02b-3602-48ef-a498-ab50023bbed8'),
(13, 2, 16, NULL, 10, 1, '2021-11-15 03:42:07', '2021-11-15 03:42:07', 'c764102e-cc9a-4df8-a35a-2f3670457384'),
(14, 3, 16, NULL, 7, 1, '2021-11-15 03:42:07', '2021-11-15 03:42:07', '54c535c9-68c6-480a-981a-22da34465cfd'),
(17, 3, 18, NULL, 7, 1, '2021-11-15 03:44:08', '2021-11-15 03:44:08', 'de40b791-301a-41ba-8819-3dce755d7aca'),
(18, 2, 18, NULL, 21, 1, '2021-11-15 03:45:30', '2021-11-15 03:45:30', 'f9f9f417-b90f-4032-9fa0-77b98ab62165'),
(19, 2, 28, NULL, 21, 1, '2021-11-15 03:45:35', '2021-11-15 03:45:35', 'f12c9a7f-5b17-412f-9334-a52084118e75'),
(20, 3, 28, NULL, 7, 1, '2021-11-15 03:45:35', '2021-11-15 03:45:35', '4fb31e7e-ca79-4ebe-9f51-b8eab8bf174d'),
(23, 2, 30, NULL, 21, 1, '2021-11-15 03:52:08', '2021-11-15 03:52:08', '9a75a5da-451a-4147-887c-e58c400a352e'),
(24, 3, 30, NULL, 7, 1, '2021-11-15 03:52:08', '2021-11-15 03:52:08', '3c1c07c6-fd8f-40fb-9533-a37e976da7c7'),
(25, 2, 31, NULL, 10, 1, '2021-11-15 03:52:36', '2021-11-15 03:52:36', 'a597e640-99d9-499f-b947-4df528c43a1c'),
(26, 3, 31, NULL, 7, 1, '2021-11-15 03:52:36', '2021-11-15 03:52:36', 'd1fa8298-d317-4445-aa5d-cfa984be00bb'),
(29, 2, 33, NULL, 21, 1, '2021-11-15 03:52:42', '2021-11-15 03:52:42', '2e1c36e0-f0e5-4ab1-9878-1bff9e46928b'),
(30, 3, 33, NULL, 7, 1, '2021-11-15 03:52:42', '2021-11-15 03:52:42', 'ac65666d-c331-4dc0-903d-18c4e0b98b96'),
(33, 2, 35, NULL, 21, 1, '2021-11-15 03:53:02', '2021-11-15 03:53:02', 'a84fa5e5-85e0-4668-8b17-b453ec15fe67'),
(34, 3, 35, NULL, 7, 1, '2021-11-15 03:53:02', '2021-11-15 03:53:02', '92d71fda-4e94-4d2b-a9b9-b7e7deba5c75'),
(37, 2, 38, NULL, 10, 1, '2021-11-15 18:16:35', '2021-11-15 18:16:35', 'f8c909e1-2746-4996-9fbe-e20b6854f789'),
(38, 3, 38, NULL, 7, 1, '2021-11-15 18:16:35', '2021-11-15 18:16:35', 'e28ead49-722e-4e90-8ab9-8b00fd363747'),
(41, 2, 40, NULL, 21, 1, '2021-11-15 18:16:52', '2021-11-15 18:16:52', '1ddda6b9-6f4b-4943-ae98-276a593b61d4'),
(42, 3, 40, NULL, 7, 1, '2021-11-15 18:16:52', '2021-11-15 18:16:52', '757865d4-c5d2-4338-84ba-09d8d64b3f22'),
(44, 3, 41, NULL, 7, 1, '2021-11-15 18:34:37', '2021-11-15 18:34:37', '77247126-2305-4a16-aa73-1b093970e071'),
(45, 2, 42, NULL, 20, 1, '2021-11-15 18:34:52', '2021-11-15 18:34:52', 'fea06d5d-a224-4ed9-8ceb-09be3513a9d1'),
(46, 3, 42, NULL, 7, 1, '2021-11-15 18:34:52', '2021-11-15 18:34:52', 'e0f47ba7-133a-41c8-89e9-aad3ceb652d7'),
(50, 2, 41, NULL, 26, 1, '2021-11-15 18:35:38', '2021-11-15 18:35:38', 'c9b5a0c3-8871-4a29-b61f-a6c27599f7cc'),
(51, 2, 44, NULL, 26, 1, '2021-11-15 18:35:38', '2021-11-15 18:35:38', '18b98738-e38a-437b-b982-3be60d248f25'),
(52, 3, 44, NULL, 7, 1, '2021-11-15 18:35:38', '2021-11-15 18:35:38', '5569ffe6-58dc-494c-93c0-b365a5370d8c'),
(53, 2, 45, NULL, 20, 1, '2021-11-15 18:39:39', '2021-11-15 18:39:39', '14ab7b6c-89c4-4af2-a5f2-a4f5e22d9e2f'),
(54, 3, 45, NULL, 7, 1, '2021-11-15 18:39:47', '2021-11-15 18:39:47', 'b69c3ef7-d123-4674-8472-630e74362f11'),
(55, 2, 46, NULL, 20, 1, '2021-11-15 18:39:53', '2021-11-15 18:39:53', '2a8ddfab-da0b-42ac-8f6d-b765c87866cc'),
(56, 3, 46, NULL, 7, 1, '2021-11-15 18:39:53', '2021-11-15 18:39:53', '87df9507-1cee-41b1-a692-6af36b86f9da'),
(57, 2, 47, NULL, 27, 1, '2021-11-15 18:41:44', '2021-11-15 18:41:44', '4283740c-7b4f-4fb9-bc60-10702aa7f2df'),
(58, 3, 47, NULL, 7, 1, '2021-11-15 18:41:48', '2021-11-15 18:41:48', '7691d667-cc85-4aac-aac7-f42361760242'),
(59, 2, 48, NULL, 27, 1, '2021-11-15 18:41:55', '2021-11-15 18:41:55', '60af7ffb-bd52-4e97-a165-070a2e981220'),
(60, 3, 48, NULL, 7, 1, '2021-11-15 18:41:55', '2021-11-15 18:41:55', '244fa441-a28a-4538-acc0-59a7ea6c9784'),
(61, 2, 49, NULL, 22, 1, '2021-11-15 18:42:49', '2021-11-15 18:42:49', '4061c1ab-b412-4758-adbf-0f920d3bb24c'),
(62, 2, 50, NULL, 22, 1, '2021-11-15 18:43:00', '2021-11-15 18:43:00', 'd7c8b5b6-60d5-4090-bff0-b23abe8e1aa7'),
(63, 2, 51, NULL, 19, 1, '2021-11-15 18:43:29', '2021-11-15 18:43:29', 'af030b75-da55-4c30-8d8b-9ecb81fa31d8'),
(64, 2, 52, NULL, 19, 1, '2021-11-15 18:43:39', '2021-11-15 18:43:39', '822f72d1-88ac-4161-8c7c-ef30182be904'),
(65, 2, 53, NULL, 25, 1, '2021-11-15 18:44:27', '2021-11-15 18:44:27', 'ced6d226-ca65-447a-a3e3-caa1eda7eece'),
(66, 2, 54, NULL, 25, 1, '2021-11-15 18:44:47', '2021-11-15 18:44:47', '3022fbcb-2d68-435f-966e-abef6f377a6e'),
(67, 2, 55, NULL, 24, 1, '2021-11-15 18:45:06', '2021-11-15 18:45:06', 'b389868e-dd55-4ae7-b7ce-f4ed1bbdca03'),
(68, 3, 55, NULL, 7, 1, '2021-11-15 18:45:13', '2021-11-15 18:45:13', '047bab27-7c90-480b-ae4f-9c8f09cccacf'),
(69, 2, 56, NULL, 24, 1, '2021-11-15 18:45:24', '2021-11-15 18:45:24', 'd81e33e0-49fa-4a39-82a1-3586e35e3617'),
(70, 3, 56, NULL, 7, 1, '2021-11-15 18:45:24', '2021-11-15 18:45:24', '8cfaea76-3738-4e8e-a1f2-3f63e2c384c6'),
(71, 2, 57, NULL, 23, 1, '2021-11-15 18:46:21', '2021-11-15 18:46:21', '0d76719d-5bfe-471d-9c82-7b37859b7d11'),
(72, 2, 58, NULL, 23, 1, '2021-11-15 18:46:32', '2021-11-15 18:46:32', '39c1229f-d020-44d7-934f-5264751f2bf3'),
(73, 2, 59, NULL, 25, 1, '2021-11-15 18:46:44', '2021-11-15 18:46:44', '6b920270-f55c-4228-ab97-afadcdfa0fbb'),
(74, 2, 60, NULL, 25, 1, '2021-11-15 18:46:44', '2021-11-15 18:46:44', 'e0a1323b-3d32-4d8e-9728-7d7235e3fff7'),
(75, 2, 61, NULL, 24, 1, '2021-11-15 18:46:44', '2021-11-15 18:46:44', '2c2cd836-79e1-4efb-a90c-12729863c34e'),
(76, 3, 61, NULL, 7, 1, '2021-11-15 18:46:44', '2021-11-15 18:46:44', '24656025-3436-40d7-8fe4-525507bd3e68'),
(77, 2, 62, NULL, 24, 1, '2021-11-15 18:46:45', '2021-11-15 18:46:45', '81e75cb4-4edb-4087-90e0-f0c8e7a2daff'),
(78, 3, 62, NULL, 7, 1, '2021-11-15 18:46:45', '2021-11-15 18:46:45', 'd537ca4c-fbba-4ee9-b4ac-14f70212471e'),
(79, 2, 63, NULL, 23, 1, '2021-11-15 18:46:45', '2021-11-15 18:46:45', 'e1694ed1-a934-499f-9799-9082cb664a28'),
(80, 2, 64, NULL, 23, 1, '2021-11-15 18:46:45', '2021-11-15 18:46:45', '0b8d4b06-f240-4256-93d9-03bff395b2e1'),
(81, 2, 65, NULL, 66, 1, '2021-11-15 18:50:46', '2021-11-15 18:50:46', '367f85d0-7bcb-4695-a5db-e505dfd4b307'),
(82, 3, 65, NULL, 69, 1, '2021-11-15 18:51:08', '2021-11-15 18:51:08', 'a50fda71-334c-47d4-8225-c1aa0c01d148'),
(83, 2, 70, NULL, 66, 1, '2021-11-15 18:51:18', '2021-11-15 18:51:18', '8e91994a-b3be-41a2-baeb-c9c08382a43d'),
(84, 3, 70, NULL, 69, 1, '2021-11-15 18:51:18', '2021-11-15 18:51:18', 'ed824954-053a-4bc9-a187-75f6f80dee58'),
(85, 2, 71, NULL, 67, 1, '2021-11-15 18:51:42', '2021-11-15 18:51:42', '17c077dc-c270-4b49-90a8-2ff3908c24a1'),
(86, 3, 71, NULL, 72, 1, '2021-11-15 18:51:54', '2021-11-15 18:51:54', 'c621bb3a-5855-46fd-bdae-b4177811256e'),
(87, 2, 73, NULL, 67, 1, '2021-11-15 18:52:00', '2021-11-15 18:52:00', 'b0396f7c-bcf4-4604-bfc5-27154dd3411d'),
(88, 3, 73, NULL, 72, 1, '2021-11-15 18:52:01', '2021-11-15 18:52:01', '594051a5-0b27-4fa3-a693-2ad1791055fa'),
(89, 2, 74, NULL, 68, 1, '2021-11-15 18:52:19', '2021-11-15 18:52:19', '66819391-33bb-490a-803e-c0615e1037cc'),
(90, 3, 74, NULL, 75, 1, '2021-11-15 18:52:32', '2021-11-15 18:52:32', '67361717-d30d-41bc-b579-e9b320af8986'),
(91, 2, 76, NULL, 68, 1, '2021-11-15 18:52:40', '2021-11-15 18:52:40', '491db068-4ced-45bf-8560-688b37ab239c'),
(92, 3, 76, NULL, 75, 1, '2021-11-15 18:52:40', '2021-11-15 18:52:40', 'a0a2d15a-6caa-47a6-842c-0f09d9a19bd5'),
(93, 2, 77, NULL, 66, 1, '2021-11-15 18:53:38', '2021-11-15 18:53:38', '950a3b71-f986-4596-be0d-8b233ce082d8'),
(94, 3, 77, NULL, 69, 1, '2021-11-15 18:53:38', '2021-11-15 18:53:38', '56320c25-457b-4b21-8306-2ac01d0edfe1'),
(95, 2, 78, NULL, 66, 1, '2021-11-15 18:53:38', '2021-11-15 18:53:38', '52cba796-62ed-4bf0-95e3-787881e5e0a8'),
(96, 3, 78, NULL, 69, 1, '2021-11-15 18:53:38', '2021-11-15 18:53:38', 'da8c6e52-47e0-42f6-8ba5-2a15696cad3c'),
(97, 2, 79, NULL, 67, 1, '2021-11-15 18:53:39', '2021-11-15 18:53:39', '1c6b45de-6281-4f03-a620-1d4035a4dccb'),
(98, 3, 79, NULL, 72, 1, '2021-11-15 18:53:39', '2021-11-15 18:53:39', '2e0d6e78-5a25-4657-bc8e-05d5da1f70cc'),
(99, 2, 80, NULL, 67, 1, '2021-11-15 18:53:39', '2021-11-15 18:53:39', 'fdb02f7d-0eab-45ba-ad58-23bf9bb28bd6'),
(100, 3, 80, NULL, 72, 1, '2021-11-15 18:53:39', '2021-11-15 18:53:39', 'def54dfe-1f2d-4a18-9c94-5c117b11f5d8'),
(101, 2, 81, NULL, 68, 1, '2021-11-15 18:53:39', '2021-11-15 18:53:39', '5c169571-2c05-4566-b9c4-20bcb684e432'),
(102, 3, 81, NULL, 75, 1, '2021-11-15 18:53:39', '2021-11-15 18:53:39', '8832cc1a-a99d-4f76-a312-58c9fb37f7a6'),
(103, 2, 82, NULL, 68, 1, '2021-11-15 18:53:39', '2021-11-15 18:53:39', '927bc1bf-3d9a-436c-b9ad-d9e8bd995726'),
(104, 3, 82, NULL, 75, 1, '2021-11-15 18:53:39', '2021-11-15 18:53:39', '5781b617-f891-4ac4-9eb1-457917df11e7');

-- --------------------------------------------------------

--
-- Table structure for table `blg_resourcepaths`
--

CREATE TABLE `blg_resourcepaths` (
  `hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `path` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `blg_resourcepaths`
--

INSERT INTO `blg_resourcepaths` (`hash`, `path`) VALUES
('10ac9518', '@app/web/assets/deprecationerrors/dist'),
('1130871d', '@app/web/assets/dashboard/dist'),
('1242dcd8', '@lib/fabric'),
('1657a953', '@app/web/assets/fields/dist'),
('22f43ecf', '@lib/fileupload'),
('27fdee93', '@app/web/assets/sites/dist'),
('2e5701d8', '@lib/iframe-resizer'),
('3196451d', '@app/web/assets/editentry/dist'),
('34b91f60', '@lib/d3'),
('3a1c3f31', '@lib/picturefill'),
('3db803f3', '@lib/jquery-touch-events'),
('3f3e097a', '@lib/prismjs'),
('4ff5434d', '@app/web/assets/admintable/dist'),
('53fbca0e', '@app/web/assets/plugins/dist'),
('59c40136', '@app/web/assets/editsection/dist'),
('5bf10e4a', '@lib/timepicker'),
('640e563f', '@lib/xregexp'),
('697c8444', '@app/web/assets/edittransform/dist'),
('6e6212b5', '@lib/selectize'),
('70408e6', '@lib/garnishjs'),
('79be37ce', '@app/web/assets/edituser/dist'),
('84933b2f', '@app/web/assets/routes/dist'),
('9926a843', '@lib/element-resize-detector'),
('a739f4cb', '@lib/velocity'),
('ab2682ef', '@app/web/assets/feed/dist'),
('aea2a3e9', '@app/web/assets/userpermissions/dist'),
('b0468bc0', '@app/web/assets/utilities/dist'),
('b6b603b6', '@bower/jquery/dist'),
('bb1e5b23', '@lib/jquery.payment'),
('c905847a', '@app/web/assets/pluginstore/dist'),
('ceac8c52', '@lib/vue'),
('cf70eedc', '@app/web/assets/recententries/dist'),
('d23dc951', '@app/web/assets/cp/dist'),
('d379ca92', '@lib/focus-visible'),
('d603dc2f', '@app/web/assets/login/dist'),
('e3472f27', '@lib/axios'),
('e3d40305', '@app/web/assets/graphiql/dist'),
('e56ec0a3', '@app/web/assets/generalsettings/dist'),
('e7c8e8dc', '@app/web/assets/installer/dist'),
('e9906f6e', '@lib/jquery-ui'),
('f1caacb2', '@app/web/assets/updateswidget/dist'),
('fc898feb', '@app/web/assets/fieldsettings/dist'),
('ff580073', '@app/web/assets/craftsupport/dist');

-- --------------------------------------------------------

--
-- Table structure for table `blg_revisions`
--

CREATE TABLE `blg_revisions` (
  `id` int NOT NULL,
  `sourceId` int NOT NULL,
  `creatorId` int DEFAULT NULL,
  `num` int NOT NULL,
  `notes` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `blg_revisions`
--

INSERT INTO `blg_revisions` (`id`, `sourceId`, `creatorId`, `num`, `notes`) VALUES
(1, 2, 1, 1, NULL),
(2, 2, 1, 2, NULL),
(3, 2, 1, 3, NULL),
(4, 2, 1, 4, NULL),
(5, 2, 1, 5, 'Applied “Draft 1”'),
(6, 14, 1, 1, ''),
(7, 14, 1, 2, ''),
(8, 18, 1, 1, ''),
(9, 18, 1, 2, 'Applied “Draft 1”'),
(10, 14, 1, 3, 'Applied “Draft 1”'),
(11, 18, 1, 3, 'Applied “Draft 1”'),
(12, 18, 1, 4, 'Applied “Draft 1”'),
(13, 14, 1, 4, 'Applied “Draft 1”'),
(14, 18, 1, 5, 'Applied “Draft 1”'),
(15, 41, 1, 1, ''),
(16, 41, 1, 2, 'Applied “Draft 1”'),
(17, 45, 1, 1, ''),
(18, 47, 1, 1, ''),
(19, 49, 1, 1, ''),
(20, 51, 1, 1, ''),
(21, 53, 1, 1, ''),
(22, 55, 1, 1, ''),
(23, 57, 1, 1, ''),
(24, 59, 1, 1, NULL),
(25, 61, 1, 1, NULL),
(26, 63, 1, 1, NULL),
(27, 65, 1, 1, ''),
(28, 71, 1, 1, ''),
(29, 74, 1, 1, ''),
(30, 77, 1, 1, NULL),
(31, 79, 1, 1, NULL),
(32, 81, 1, 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `blg_searchindex`
--

CREATE TABLE `blg_searchindex` (
  `elementId` int NOT NULL,
  `attribute` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `fieldId` int NOT NULL,
  `siteId` int NOT NULL,
  `keywords` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `blg_searchindex`
--

INSERT INTO `blg_searchindex` (`elementId`, `attribute`, `fieldId`, `siteId`, `keywords`) VALUES
(1, 'username', 0, 1, ' admin '),
(1, 'lastname', 0, 1, ''),
(1, 'fullname', 0, 1, ''),
(1, 'firstname', 0, 1, ''),
(2, 'title', 0, 1, ' articles '),
(2, 'slug', 0, 1, ' entries '),
(7, 'slug', 0, 1, ' books '),
(7, 'title', 0, 1, ' books '),
(12, 'title', 0, 1, ' articles '),
(10, 'filename', 0, 1, ' post 10 jpeg '),
(12, 'slug', 0, 1, ' entries '),
(10, 'extension', 0, 1, ' jpeg '),
(10, 'kind', 0, 1, ' image '),
(10, 'slug', 0, 1, ''),
(10, 'title', 0, 1, ' post 10 '),
(13, 'slug', 0, 1, ' temp riofzevtrmwqfseysqjvmkqqwqwnjlespzbv '),
(13, 'title', 0, 1, ' articles '),
(36, 'extension', 0, 1, ' jpeg '),
(14, 'field', 8, 1, ' rebuild trust after sexual betrayal '),
(14, 'slug', 0, 1, ' rebuild trust after sexual betrayal 2 '),
(14, 'title', 0, 1, ' rebuild trust after sexual betrayal '),
(36, 'filename', 0, 1, ' author jpeg '),
(18, 'title', 0, 1, ' rebuild trust after sexual betrayal '),
(18, 'slug', 0, 1, ' rebuild trust after sexual betrayal 2 2 '),
(19, 'filename', 0, 1, ' post 01 jpeg '),
(19, 'extension', 0, 1, ' jpeg '),
(19, 'kind', 0, 1, ' image '),
(19, 'slug', 0, 1, ''),
(19, 'title', 0, 1, ' post 01 '),
(20, 'filename', 0, 1, ' post 02 jpeg '),
(20, 'extension', 0, 1, ' jpeg '),
(20, 'kind', 0, 1, ' image '),
(20, 'slug', 0, 1, ''),
(20, 'title', 0, 1, ' post 02 '),
(21, 'filename', 0, 1, ' post 03 jpeg '),
(21, 'extension', 0, 1, ' jpeg '),
(21, 'kind', 0, 1, ' image '),
(21, 'slug', 0, 1, ''),
(21, 'title', 0, 1, ' post 03 '),
(22, 'filename', 0, 1, ' post 04 jpeg '),
(22, 'extension', 0, 1, ' jpeg '),
(22, 'kind', 0, 1, ' image '),
(22, 'slug', 0, 1, ''),
(22, 'title', 0, 1, ' post 04 '),
(23, 'filename', 0, 1, ' post 05 jpeg '),
(23, 'extension', 0, 1, ' jpeg '),
(23, 'kind', 0, 1, ' image '),
(23, 'slug', 0, 1, ''),
(23, 'title', 0, 1, ' post 05 '),
(24, 'filename', 0, 1, ' post 06 jpeg '),
(24, 'extension', 0, 1, ' jpeg '),
(24, 'kind', 0, 1, ' image '),
(24, 'slug', 0, 1, ''),
(24, 'title', 0, 1, ' post 06 '),
(25, 'filename', 0, 1, ' post 07 jpeg '),
(25, 'extension', 0, 1, ' jpeg '),
(25, 'kind', 0, 1, ' image '),
(25, 'slug', 0, 1, ''),
(25, 'title', 0, 1, ' post 07 '),
(26, 'filename', 0, 1, ' post 08 jpeg '),
(26, 'extension', 0, 1, ' jpeg '),
(26, 'kind', 0, 1, ' image '),
(26, 'slug', 0, 1, ''),
(26, 'title', 0, 1, ' post 08 '),
(27, 'filename', 0, 1, ' post 09 jpeg '),
(27, 'extension', 0, 1, ' jpeg '),
(27, 'kind', 0, 1, ' image '),
(27, 'slug', 0, 1, ''),
(27, 'title', 0, 1, ' post 09 '),
(18, 'field', 8, 1, ' rebuild trust after sexual betrayal '),
(36, 'kind', 0, 1, ' image '),
(36, 'slug', 0, 1, ''),
(36, 'title', 0, 1, ' author '),
(1, 'email', 0, 1, ' laviniamanzanares gmail com '),
(1, 'slug', 0, 1, ''),
(41, 'slug', 0, 1, ' lorem ipsum dolor sit amet consectetur adipiscing elit '),
(41, 'title', 0, 1, ' lorem ipsum dolor sit amet consectetur adipiscing elit '),
(41, 'field', 8, 1, ' lorem ipsum dolor sit amet consectetur adipiscing elit '),
(45, 'slug', 0, 1, ' quisque ac ligula a odio condimentum sagittis '),
(45, 'title', 0, 1, ' quisque ac ligula a odio condimentum sagittis '),
(45, 'field', 8, 1, ' quisque ac ligula a odio condimentum sagittis '),
(47, 'slug', 0, 1, ' aliquam erat volutpat '),
(47, 'title', 0, 1, ' aliquam erat volutpat '),
(47, 'field', 8, 1, ' aliquam erat volutpat '),
(49, 'title', 0, 1, ' vivamus urna lacus posuere quis urna eget iaculis suscipit ex '),
(49, 'slug', 0, 1, ' vivamus urna lacus posuere quis urna eget iaculis suscipit ex '),
(49, 'field', 8, 1, ' vivamus urna lacus posuere quis urna eget iaculis suscipit ex '),
(51, 'slug', 0, 1, ' suspendisse ultrices maximus massa ac aliquet quam pellentesque ut '),
(51, 'title', 0, 1, ' suspendisse ultrices maximus massa ac aliquet quam pellentesque ut '),
(51, 'field', 8, 1, ' suspendisse ultrices maximus massa ac aliquet quam pellentesque ut '),
(53, 'slug', 0, 1, ' duis sit amet facilisis sapien '),
(53, 'title', 0, 1, ' duis sit amet facilisis sapien '),
(53, 'field', 8, 1, ' duis sit amet facilisis sapien '),
(55, 'title', 0, 1, ' aenean faucibus mollis dui malesuada commodo '),
(55, 'slug', 0, 1, ' aenean faucibus mollis dui malesuada commodo '),
(55, 'field', 8, 1, ' aenean faucibus mollis dui malesuada commodo '),
(57, 'slug', 0, 1, ' in viverra est et pretium finibus '),
(57, 'title', 0, 1, ' in viverra est et pretium finibus '),
(57, 'field', 8, 1, ' in viverra est et pretium finibus '),
(59, 'slug', 0, 1, ' duis sit amet facilisis sapien 2 '),
(59, 'title', 0, 1, ' duis sit amet facilisis sapien '),
(59, 'field', 8, 1, ' duis sit amet facilisis sapien '),
(61, 'slug', 0, 1, ' aenean faucibus mollis dui malesuada commodo 2 '),
(61, 'title', 0, 1, ' aenean faucibus mollis dui malesuada commodo '),
(61, 'field', 8, 1, ' aenean faucibus mollis dui malesuada commodo '),
(63, 'slug', 0, 1, ' in viverra est et pretium finibus 2 '),
(63, 'title', 0, 1, ' in viverra est et pretium finibus '),
(63, 'field', 8, 1, ' in viverra est et pretium finibus '),
(65, 'slug', 0, 1, ' mauris vel odio dapibus metus ornare dignissim '),
(65, 'title', 0, 1, ' mauris vel odio dapibus metus ornare dignissim '),
(65, 'field', 8, 1, ' mauris vel odio dapibus metus ornare dignissim '),
(66, 'filename', 0, 1, ' man g9e6b97198 640 jpg '),
(66, 'extension', 0, 1, ' jpg '),
(66, 'kind', 0, 1, ' image '),
(66, 'slug', 0, 1, ''),
(66, 'title', 0, 1, ' man g9e6b97198 640 '),
(67, 'filename', 0, 1, ' autumn gce5060330 640 jpg '),
(67, 'extension', 0, 1, ' jpg '),
(67, 'kind', 0, 1, ' image '),
(67, 'slug', 0, 1, ''),
(67, 'title', 0, 1, ' autumn gce5060330 640 '),
(68, 'filename', 0, 1, ' mountain g5dd207968 640 jpg '),
(68, 'extension', 0, 1, ' jpg '),
(68, 'kind', 0, 1, ' image '),
(68, 'slug', 0, 1, ''),
(68, 'title', 0, 1, ' mountain g5dd207968 640 '),
(69, 'slug', 0, 1, ' nature '),
(69, 'title', 0, 1, ' nature '),
(71, 'title', 0, 1, ' aenean vel mauris odio '),
(71, 'slug', 0, 1, ' aenean vel mauris odio '),
(71, 'field', 8, 1, ' aenean vel mauris odio '),
(72, 'slug', 0, 1, ' season '),
(72, 'title', 0, 1, ' season '),
(74, 'slug', 0, 1, ' phasellus ullamcorper pretium volutpat '),
(74, 'title', 0, 1, ' phasellus ullamcorper pretium volutpat '),
(74, 'field', 8, 1, ' phasellus ullamcorper pretium volutpat '),
(75, 'slug', 0, 1, ' mountain '),
(75, 'title', 0, 1, ' mountain '),
(77, 'slug', 0, 1, ' mauris vel odio dapibus metus ornare dignissim 2 '),
(77, 'title', 0, 1, ' mauris vel odio dapibus metus ornare dignissim '),
(77, 'field', 8, 1, ' mauris vel odio dapibus metus ornare dignissim '),
(79, 'slug', 0, 1, ' aenean vel mauris odio 2 '),
(79, 'title', 0, 1, ' aenean vel mauris odio '),
(79, 'field', 8, 1, ' aenean vel mauris odio '),
(81, 'slug', 0, 1, ' phasellus ullamcorper pretium volutpat 2 '),
(81, 'title', 0, 1, ' phasellus ullamcorper pretium volutpat '),
(81, 'field', 8, 1, ' phasellus ullamcorper pretium volutpat ');

-- --------------------------------------------------------

--
-- Table structure for table `blg_sections`
--

CREATE TABLE `blg_sections` (
  `id` int NOT NULL,
  `structureId` int DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `handle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` enum('single','channel','structure') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'channel',
  `enableVersioning` tinyint(1) NOT NULL DEFAULT '0',
  `propagationMethod` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'all',
  `defaultPlacement` enum('beginning','end') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'end',
  `previewTargets` text COLLATE utf8_unicode_ci,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `dateDeleted` datetime DEFAULT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `blg_sections`
--

INSERT INTO `blg_sections` (`id`, `structureId`, `name`, `handle`, `type`, `enableVersioning`, `propagationMethod`, `defaultPlacement`, `previewTargets`, `dateCreated`, `dateUpdated`, `dateDeleted`, `uid`) VALUES
(1, NULL, 'Articles', 'articles', 'channel', 1, 'all', 'end', NULL, '2021-11-15 01:06:16', '2021-11-15 01:07:31', NULL, '7f3a3d16-e578-421a-991a-91842208b384');

-- --------------------------------------------------------

--
-- Table structure for table `blg_sections_sites`
--

CREATE TABLE `blg_sections_sites` (
  `id` int NOT NULL,
  `sectionId` int NOT NULL,
  `siteId` int NOT NULL,
  `hasUrls` tinyint(1) NOT NULL DEFAULT '1',
  `uriFormat` text COLLATE utf8_unicode_ci,
  `template` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `enabledByDefault` tinyint(1) NOT NULL DEFAULT '1',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `blg_sections_sites`
--

INSERT INTO `blg_sections_sites` (`id`, `sectionId`, `siteId`, `hasUrls`, `uriFormat`, `template`, `enabledByDefault`, `dateCreated`, `dateUpdated`, `uid`) VALUES
(1, 1, 1, 1, 'articles/{{ author.username }}/{{ slug }}', 'articles/_entry', 1, '2021-11-15 01:06:16', '2021-11-15 03:49:54', 'b4dec807-639c-4562-8428-aaa7088c5e04');

-- --------------------------------------------------------

--
-- Table structure for table `blg_sequences`
--

CREATE TABLE `blg_sequences` (
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `next` int UNSIGNED NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `blg_sessions`
--

CREATE TABLE `blg_sessions` (
  `id` int NOT NULL,
  `userId` int NOT NULL,
  `token` char(100) COLLATE utf8_unicode_ci NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `blg_sessions`
--

INSERT INTO `blg_sessions` (`id`, `userId`, `token`, `dateCreated`, `dateUpdated`, `uid`) VALUES
(1, 1, 'ARUEhAusGIJ-AyEItgagcUZgyNQazc6Jp4vAVUMIDfiEmdXCJxoeC1RIsRRzvaSvaR7G4ZEQ63xahwBiWnPFOEm1rsQ6TK0P_lSx', '2021-11-15 00:50:24', '2021-11-15 20:37:55', '9770092a-a5fa-473b-aeb6-4878466703af');

-- --------------------------------------------------------

--
-- Table structure for table `blg_shunnedmessages`
--

CREATE TABLE `blg_shunnedmessages` (
  `id` int NOT NULL,
  `userId` int NOT NULL,
  `message` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `expiryDate` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `blg_sitegroups`
--

CREATE TABLE `blg_sitegroups` (
  `id` int NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `dateDeleted` datetime DEFAULT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `blg_sitegroups`
--

INSERT INTO `blg_sitegroups` (`id`, `name`, `dateCreated`, `dateUpdated`, `dateDeleted`, `uid`) VALUES
(1, 'blogcraft.test', '2021-11-14 23:59:06', '2021-11-14 23:59:06', NULL, 'c4128e2f-c490-4b54-a451-731ad120bd0c');

-- --------------------------------------------------------

--
-- Table structure for table `blg_sites`
--

CREATE TABLE `blg_sites` (
  `id` int NOT NULL,
  `groupId` int NOT NULL,
  `primary` tinyint(1) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `handle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `language` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `hasUrls` tinyint(1) NOT NULL DEFAULT '0',
  `baseUrl` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sortOrder` smallint UNSIGNED DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `dateDeleted` datetime DEFAULT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `blg_sites`
--

INSERT INTO `blg_sites` (`id`, `groupId`, `primary`, `enabled`, `name`, `handle`, `language`, `hasUrls`, `baseUrl`, `sortOrder`, `dateCreated`, `dateUpdated`, `dateDeleted`, `uid`) VALUES
(1, 1, 1, 1, 'blogcraft.test', 'default', 'en-US', 1, '$PRIMARY_SITE_URL', 1, '2021-11-14 23:59:06', '2021-11-14 23:59:06', NULL, 'fe3b0bf5-0ec5-4aa7-a9bd-7f56f878127c');

-- --------------------------------------------------------

--
-- Table structure for table `blg_structureelements`
--

CREATE TABLE `blg_structureelements` (
  `id` int NOT NULL,
  `structureId` int NOT NULL,
  `elementId` int DEFAULT NULL,
  `root` int UNSIGNED DEFAULT NULL,
  `lft` int UNSIGNED NOT NULL,
  `rgt` int UNSIGNED NOT NULL,
  `level` smallint UNSIGNED NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `blg_structures`
--

CREATE TABLE `blg_structures` (
  `id` int NOT NULL,
  `maxLevels` smallint UNSIGNED DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `dateDeleted` datetime DEFAULT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `blg_systemmessages`
--

CREATE TABLE `blg_systemmessages` (
  `id` int NOT NULL,
  `language` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `subject` text COLLATE utf8_unicode_ci NOT NULL,
  `body` text COLLATE utf8_unicode_ci NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `blg_taggroups`
--

CREATE TABLE `blg_taggroups` (
  `id` int NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `handle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fieldLayoutId` int DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `dateDeleted` datetime DEFAULT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `blg_taggroups`
--

INSERT INTO `blg_taggroups` (`id`, `name`, `handle`, `fieldLayoutId`, `dateCreated`, `dateUpdated`, `dateDeleted`, `uid`) VALUES
(1, 'Article Tags', 'articleTags', 3, '2021-11-15 03:25:36', '2021-11-15 03:25:36', NULL, '4335f424-4db1-4fc1-b173-3a3d2cad684e');

-- --------------------------------------------------------

--
-- Table structure for table `blg_tags`
--

CREATE TABLE `blg_tags` (
  `id` int NOT NULL,
  `groupId` int NOT NULL,
  `deletedWithGroup` tinyint(1) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `blg_tags`
--

INSERT INTO `blg_tags` (`id`, `groupId`, `deletedWithGroup`, `dateCreated`, `dateUpdated`, `uid`) VALUES
(7, 1, NULL, '2021-11-15 03:26:43', '2021-11-15 03:26:43', 'e056522f-b5da-44b9-9089-56c3b424b912'),
(69, 1, NULL, '2021-11-15 18:51:06', '2021-11-15 18:51:06', '49c1a884-9696-41fe-b939-ea575cd55f56'),
(72, 1, NULL, '2021-11-15 18:51:51', '2021-11-15 18:51:51', '20a01478-2270-4f6b-9cd5-701eb62d83fb'),
(75, 1, NULL, '2021-11-15 18:52:27', '2021-11-15 18:52:27', '45896e23-6dad-40c4-9396-3ebd450ac47f');

-- --------------------------------------------------------

--
-- Table structure for table `blg_templatecacheelements`
--

CREATE TABLE `blg_templatecacheelements` (
  `id` int NOT NULL,
  `cacheId` int NOT NULL,
  `elementId` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `blg_templatecachequeries`
--

CREATE TABLE `blg_templatecachequeries` (
  `id` int NOT NULL,
  `cacheId` int NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `query` longtext COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `blg_templatecaches`
--

CREATE TABLE `blg_templatecaches` (
  `id` int NOT NULL,
  `siteId` int NOT NULL,
  `cacheKey` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `path` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `expiryDate` datetime NOT NULL,
  `body` mediumtext COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `blg_tokens`
--

CREATE TABLE `blg_tokens` (
  `id` int NOT NULL,
  `token` char(32) COLLATE utf8_unicode_ci NOT NULL,
  `route` text COLLATE utf8_unicode_ci,
  `usageLimit` tinyint UNSIGNED DEFAULT NULL,
  `usageCount` tinyint UNSIGNED DEFAULT NULL,
  `expiryDate` datetime NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `blg_tokens`
--

INSERT INTO `blg_tokens` (`id`, `token`, `route`, `usageLimit`, `usageCount`, `expiryDate`, `dateCreated`, `dateUpdated`, `uid`) VALUES
(1, 'smx5QW8CGCe6NbJGolbHSLLvWZonKtC8', '[\"preview/preview\",{\"elementType\":\"craft\\\\elements\\\\Entry\",\"sourceId\":2,\"siteId\":1,\"draftId\":2,\"revisionId\":null,\"provisional\":true}]', NULL, NULL, '2021-11-16 03:35:21', '2021-11-15 03:35:21', '2021-11-15 03:35:21', '192ab92e-08f0-417e-b19d-2448034ffe50');

-- --------------------------------------------------------

--
-- Table structure for table `blg_usergroups`
--

CREATE TABLE `blg_usergroups` (
  `id` int NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `handle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `blg_usergroups_users`
--

CREATE TABLE `blg_usergroups_users` (
  `id` int NOT NULL,
  `groupId` int NOT NULL,
  `userId` int NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `blg_userpermissions`
--

CREATE TABLE `blg_userpermissions` (
  `id` int NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `blg_userpermissions_usergroups`
--

CREATE TABLE `blg_userpermissions_usergroups` (
  `id` int NOT NULL,
  `permissionId` int NOT NULL,
  `groupId` int NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `blg_userpermissions_users`
--

CREATE TABLE `blg_userpermissions_users` (
  `id` int NOT NULL,
  `permissionId` int NOT NULL,
  `userId` int NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `blg_userpreferences`
--

CREATE TABLE `blg_userpreferences` (
  `userId` int NOT NULL,
  `preferences` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `blg_userpreferences`
--

INSERT INTO `blg_userpreferences` (`userId`, `preferences`) VALUES
(1, '{\"language\":\"en-US\"}');

-- --------------------------------------------------------

--
-- Table structure for table `blg_users`
--

CREATE TABLE `blg_users` (
  `id` int NOT NULL,
  `username` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `photoId` int DEFAULT NULL,
  `firstName` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lastName` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `admin` tinyint(1) NOT NULL DEFAULT '0',
  `locked` tinyint(1) NOT NULL DEFAULT '0',
  `suspended` tinyint(1) NOT NULL DEFAULT '0',
  `pending` tinyint(1) NOT NULL DEFAULT '0',
  `lastLoginDate` datetime DEFAULT NULL,
  `lastLoginAttemptIp` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `invalidLoginWindowStart` datetime DEFAULT NULL,
  `invalidLoginCount` tinyint UNSIGNED DEFAULT NULL,
  `lastInvalidLoginDate` datetime DEFAULT NULL,
  `lockoutDate` datetime DEFAULT NULL,
  `hasDashboard` tinyint(1) NOT NULL DEFAULT '0',
  `verificationCode` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `verificationCodeIssuedDate` datetime DEFAULT NULL,
  `unverifiedEmail` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `passwordResetRequired` tinyint(1) NOT NULL DEFAULT '0',
  `lastPasswordChangeDate` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `blg_users`
--

INSERT INTO `blg_users` (`id`, `username`, `photoId`, `firstName`, `lastName`, `email`, `password`, `admin`, `locked`, `suspended`, `pending`, `lastLoginDate`, `lastLoginAttemptIp`, `invalidLoginWindowStart`, `invalidLoginCount`, `lastInvalidLoginDate`, `lockoutDate`, `hasDashboard`, `verificationCode`, `verificationCodeIssuedDate`, `unverifiedEmail`, `passwordResetRequired`, `lastPasswordChangeDate`, `dateCreated`, `dateUpdated`, `uid`) VALUES
(1, 'admin', 36, NULL, NULL, 'laviniamanzanares@gmail.com', '$2y$13$akdbVeVPCjU6ao5UOPcuV.5eJleshNOTkDC7WUeDd4mFKCLkoAlTe', 1, 0, 0, 0, '2021-11-15 00:50:24', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, 0, '2021-11-14 23:59:09', '2021-11-14 23:59:09', '2021-11-15 03:59:38', 'ad07938d-fdb5-4a86-a71c-499c3f7270a2');

-- --------------------------------------------------------

--
-- Table structure for table `blg_volumefolders`
--

CREATE TABLE `blg_volumefolders` (
  `id` int NOT NULL,
  `parentId` int DEFAULT NULL,
  `volumeId` int DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `path` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `blg_volumefolders`
--

INSERT INTO `blg_volumefolders` (`id`, `parentId`, `volumeId`, `name`, `path`, `dateCreated`, `dateUpdated`, `uid`) VALUES
(1, NULL, 1, 'Uploads', '', '2021-11-15 03:22:44', '2021-11-15 03:22:44', '5cb64696-0bc0-478f-9ad4-7be450536034'),
(2, NULL, NULL, 'Temporary source', NULL, '2021-11-15 03:24:20', '2021-11-15 03:24:20', '29a74d74-b2b7-4a2d-a66e-a2a73ec92c27'),
(3, 2, NULL, 'user_1', 'user_1/', '2021-11-15 03:24:20', '2021-11-15 03:24:20', '4a27d39a-9890-4cce-af14-00ad453b5fbb');

-- --------------------------------------------------------

--
-- Table structure for table `blg_volumes`
--

CREATE TABLE `blg_volumes` (
  `id` int NOT NULL,
  `fieldLayoutId` int DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `handle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `hasUrls` tinyint(1) NOT NULL DEFAULT '1',
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `titleTranslationMethod` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'site',
  `titleTranslationKeyFormat` text COLLATE utf8_unicode_ci,
  `settings` text COLLATE utf8_unicode_ci,
  `sortOrder` smallint UNSIGNED DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `dateDeleted` datetime DEFAULT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `blg_volumes`
--

INSERT INTO `blg_volumes` (`id`, `fieldLayoutId`, `name`, `handle`, `type`, `hasUrls`, `url`, `titleTranslationMethod`, `titleTranslationKeyFormat`, `settings`, `sortOrder`, `dateCreated`, `dateUpdated`, `dateDeleted`, `uid`) VALUES
(1, 2, 'Uploads', 'uploads', 'craft\\volumes\\Local', 1, '$UPLOADS_URL', 'site', NULL, '{\"path\":\"@root/web/uploads\"}', 1, '2021-11-15 03:22:44', '2021-11-15 03:22:44', NULL, 'f38e29ae-f631-4ce5-a112-5d7cd773e4fa');

-- --------------------------------------------------------

--
-- Table structure for table `blg_widgets`
--

CREATE TABLE `blg_widgets` (
  `id` int NOT NULL,
  `userId` int NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sortOrder` smallint UNSIGNED DEFAULT NULL,
  `colspan` tinyint DEFAULT NULL,
  `settings` text COLLATE utf8_unicode_ci,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `blg_widgets`
--

INSERT INTO `blg_widgets` (`id`, `userId`, `type`, `sortOrder`, `colspan`, `settings`, `enabled`, `dateCreated`, `dateUpdated`, `uid`) VALUES
(1, 1, 'craft\\widgets\\RecentEntries', 1, NULL, '{\"siteId\":1,\"section\":\"*\",\"limit\":10}', 1, '2021-11-15 00:51:07', '2021-11-15 00:51:07', '667e37f2-2285-4c15-81cd-ab8f215a2688'),
(2, 1, 'craft\\widgets\\CraftSupport', 2, NULL, '[]', 1, '2021-11-15 00:51:07', '2021-11-15 00:51:07', '772680a2-36be-47db-b47d-71843b9bfe90'),
(3, 1, 'craft\\widgets\\Updates', 3, NULL, '[]', 1, '2021-11-15 00:51:07', '2021-11-15 00:51:07', 'eafcbfad-a46b-46ae-9ec9-366a5f2a3222'),
(4, 1, 'craft\\widgets\\Feed', 4, NULL, '{\"url\":\"https://craftcms.com/news.rss\",\"title\":\"Craft News\",\"limit\":5}', 1, '2021-11-15 00:51:08', '2021-11-15 00:51:08', '22ff7539-e114-42b6-a4b0-705bb0078b2f');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `blg_announcements`
--
ALTER TABLE `blg_announcements`
  ADD PRIMARY KEY (`id`),
  ADD KEY `blg_idx_laqlotqojhdvagnwbpvrzffarsozlnwaxfuq` (`userId`,`unread`,`dateRead`,`dateCreated`),
  ADD KEY `blg_idx_yrkkakemgsdxvvyoeffmdslfgebacyfzlcwz` (`dateRead`),
  ADD KEY `blg_fk_yyriptaccbdfqkssmkpzoqmdemshcdcwhezc` (`pluginId`);

--
-- Indexes for table `blg_assetindexdata`
--
ALTER TABLE `blg_assetindexdata`
  ADD PRIMARY KEY (`id`),
  ADD KEY `blg_idx_dtghunerxeufnhyyyftqrgyfxwkbhbrxfmil` (`sessionId`,`volumeId`),
  ADD KEY `blg_idx_tgzfobmwzbokdmoocoyxbpebrzrhypaaneks` (`volumeId`);

--
-- Indexes for table `blg_assets`
--
ALTER TABLE `blg_assets`
  ADD PRIMARY KEY (`id`),
  ADD KEY `blg_idx_chkcfxeilxlgpbwaqgxyyszffebgaqobuiqe` (`filename`,`folderId`),
  ADD KEY `blg_idx_anrxhogvsdbnnelnxiezolxvhqkflsypslqu` (`folderId`),
  ADD KEY `blg_idx_oeiytjnavsompofccqedkwuegvuvhxpvzkxl` (`volumeId`),
  ADD KEY `blg_fk_spvhdroqlwvtgnzodsrjxuofxhgpacwpqfuy` (`uploaderId`);

--
-- Indexes for table `blg_assettransformindex`
--
ALTER TABLE `blg_assettransformindex`
  ADD PRIMARY KEY (`id`),
  ADD KEY `blg_idx_drggtwpfjhorrcxeebckuqlckxmuikxnsaks` (`volumeId`,`assetId`,`location`);

--
-- Indexes for table `blg_assettransforms`
--
ALTER TABLE `blg_assettransforms`
  ADD PRIMARY KEY (`id`),
  ADD KEY `blg_idx_pjbqlkzyzcnpskuypngyerfvpotvxejjxuum` (`name`),
  ADD KEY `blg_idx_qalwfdaefiaknkekdcctozdbfjpqzrmcgkmo` (`handle`);

--
-- Indexes for table `blg_categories`
--
ALTER TABLE `blg_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `blg_idx_hofloccuipimaiwxznyxjpbcmhkzugjumkad` (`groupId`),
  ADD KEY `blg_fk_vjdjhkrgqtbovlawyokiydojeljiqfuopgml` (`parentId`);

--
-- Indexes for table `blg_categorygroups`
--
ALTER TABLE `blg_categorygroups`
  ADD PRIMARY KEY (`id`),
  ADD KEY `blg_idx_zvkxojjqihhbchndzaovjvuaswtoeausvmui` (`name`),
  ADD KEY `blg_idx_keuzhlfqklbhvptlcipqimrzepmpnmcohrqb` (`handle`),
  ADD KEY `blg_idx_lwjoxymxytiixxovjjapnidrziwbdbnnnnta` (`structureId`),
  ADD KEY `blg_idx_cxnlhspagsscwapplxhkjaxuipjyvibvkrpx` (`fieldLayoutId`),
  ADD KEY `blg_idx_moyfrmnudowhqfcbnfeyghmnqyosnhezyrqh` (`dateDeleted`);

--
-- Indexes for table `blg_categorygroups_sites`
--
ALTER TABLE `blg_categorygroups_sites`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `blg_idx_wfcryeubfcjzdljxvqanrerwjfqpzgwuicpw` (`groupId`,`siteId`),
  ADD KEY `blg_idx_zxrimcnpgeghyuhvsyirejftsbseowdemqth` (`siteId`);

--
-- Indexes for table `blg_changedattributes`
--
ALTER TABLE `blg_changedattributes`
  ADD PRIMARY KEY (`elementId`,`siteId`,`attribute`),
  ADD KEY `blg_idx_faimfdeulrppvsvfrmdldcipqdzgjmanlcks` (`elementId`,`siteId`,`dateUpdated`),
  ADD KEY `blg_fk_gxfsqtbsjwqsfpsohobwnmnofehnsncsfouv` (`siteId`),
  ADD KEY `blg_fk_nvhmnhsuxglhjdepnhqgxncjemjkjqvlgbiu` (`userId`);

--
-- Indexes for table `blg_changedfields`
--
ALTER TABLE `blg_changedfields`
  ADD PRIMARY KEY (`elementId`,`siteId`,`fieldId`),
  ADD KEY `blg_idx_edturxtvwegxlfutfwosgisswcjwzmifgqox` (`elementId`,`siteId`,`dateUpdated`),
  ADD KEY `blg_fk_prtewntgrztacwyimoszoakulaeqzceclsgg` (`siteId`),
  ADD KEY `blg_fk_kxvrncolgulkflmuktyokjneddqcbfmclzng` (`fieldId`),
  ADD KEY `blg_fk_oxsqbwnaaqrbtahlamcdogahsykjujyucfws` (`userId`);

--
-- Indexes for table `blg_content`
--
ALTER TABLE `blg_content`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `blg_idx_dargmmfbpknrioxobylsyvkcywpygiwxsgnm` (`elementId`,`siteId`),
  ADD KEY `blg_idx_nvrlqhufipyhtmyxmbvynzbjuekqhjeogdwn` (`siteId`),
  ADD KEY `blg_idx_hfkbhdbwbzxsitwkhpsgxynjgrjhpxosvmqe` (`title`);

--
-- Indexes for table `blg_craftidtokens`
--
ALTER TABLE `blg_craftidtokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `blg_fk_zcdweptnrqecwpctsvbuhsgcskgmniarkwyt` (`userId`);

--
-- Indexes for table `blg_deprecationerrors`
--
ALTER TABLE `blg_deprecationerrors`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `blg_idx_iiwuijbncpyxhrphwdkkuisudkohybmqvulo` (`key`,`fingerprint`);

--
-- Indexes for table `blg_drafts`
--
ALTER TABLE `blg_drafts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `blg_idx_pveulfrozvopxcdwcvbnkfkyvvkrewofjpvf` (`creatorId`,`provisional`),
  ADD KEY `blg_idx_hykbxmhkuiwcyuzurqdmqytxucntujxljgrj` (`saved`),
  ADD KEY `blg_fk_vhkbpehonjwudhsnrpyemolvldiuzmzsimie` (`sourceId`);

--
-- Indexes for table `blg_elementindexsettings`
--
ALTER TABLE `blg_elementindexsettings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `blg_idx_rrvzconlkirtyxmqrziurlnzetxtsvsdgowq` (`type`);

--
-- Indexes for table `blg_elements`
--
ALTER TABLE `blg_elements`
  ADD PRIMARY KEY (`id`),
  ADD KEY `blg_idx_ecryudbheaotovprvirrgmhkmntsogftsxhv` (`dateDeleted`),
  ADD KEY `blg_idx_biiqtvtfztmlkdtpbqmcltutycpzefqgpqzl` (`fieldLayoutId`),
  ADD KEY `blg_idx_lfeffqojnrzkexhjeuywguxlfmowlgfivgyb` (`type`),
  ADD KEY `blg_idx_anopfhssecayhfbndkmdtdsjwbyrsxmrzjlq` (`enabled`),
  ADD KEY `blg_idx_ylsobbmbgmcexmnuetvennqffjqsjkqwealb` (`archived`,`dateCreated`),
  ADD KEY `blg_idx_twedbwlzkpctcjvdilfhywlsjqggyiiyvkcg` (`archived`,`dateDeleted`,`draftId`,`revisionId`,`canonicalId`),
  ADD KEY `blg_fk_pzfbjziebojaonyrfolanwoxjticwxtagxdq` (`canonicalId`),
  ADD KEY `blg_fk_zryekljnycqjfwyakxmjbxntqqfroxzhjvsd` (`draftId`),
  ADD KEY `blg_fk_ppfgkwrofcneotvkfummlrtfmovnszpeungf` (`revisionId`);

--
-- Indexes for table `blg_elements_sites`
--
ALTER TABLE `blg_elements_sites`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `blg_idx_chlwdfvmvbywirxmhgdchermpxzqcbjsogvu` (`elementId`,`siteId`),
  ADD KEY `blg_idx_sjcdgzqsvvahvtqohnwnabunbredmpbuawkw` (`siteId`),
  ADD KEY `blg_idx_tjjmzrcruxwbugkvbaoqwmbguqhgblwpjwzc` (`slug`,`siteId`),
  ADD KEY `blg_idx_hflkctpdvurenvikrpeenxtctlijphyjkiny` (`enabled`),
  ADD KEY `blg_idx_sfebgxxpaejpzkeojgaygrhwfhutlqfjseme` (`uri`,`siteId`);

--
-- Indexes for table `blg_entries`
--
ALTER TABLE `blg_entries`
  ADD PRIMARY KEY (`id`),
  ADD KEY `blg_idx_vgxpvwglhntagdzcaxbzfzqyjmwhyfwbxqlv` (`postDate`),
  ADD KEY `blg_idx_gpcbousamfdkuayuvfzyuwhjpdyuhgaxxinl` (`expiryDate`),
  ADD KEY `blg_idx_wnybydxpoqjyxccctbhbcpuqbgoszmauplxp` (`authorId`),
  ADD KEY `blg_idx_qysrazwjpkpdfgsnypqwmckwunhfqisjidwg` (`sectionId`),
  ADD KEY `blg_idx_cedwwpwhuasqngghoyboejjsfsqzdunqpevx` (`typeId`),
  ADD KEY `blg_fk_kbhlkbgbnxcufbeyfgipwyfksnenmdytwqmt` (`parentId`);

--
-- Indexes for table `blg_entrytypes`
--
ALTER TABLE `blg_entrytypes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `blg_idx_trtibkfauzzsfjdgybgpbbfetcfqcdqygyfd` (`name`,`sectionId`),
  ADD KEY `blg_idx_truglbbficdzkjndjlzqtdegaplqjxgbtneo` (`handle`,`sectionId`),
  ADD KEY `blg_idx_yxzjksxqejxbexswkmcvhxlpbtphzclkyhep` (`sectionId`),
  ADD KEY `blg_idx_bhyvfplmugwnjbvdkwrtxnhkdyvlcjuyxqfa` (`fieldLayoutId`),
  ADD KEY `blg_idx_rnlkwmuaeewnwtzzpbrcmqexebouintrdjmz` (`dateDeleted`);

--
-- Indexes for table `blg_fieldgroups`
--
ALTER TABLE `blg_fieldgroups`
  ADD PRIMARY KEY (`id`),
  ADD KEY `blg_idx_jemrysgfytnxnpxvresvssacdevlxhmkerud` (`name`),
  ADD KEY `blg_idx_bforfwmaohkfzzculrjxyyqpfuvosqinjijs` (`dateDeleted`,`name`);

--
-- Indexes for table `blg_fieldlayoutfields`
--
ALTER TABLE `blg_fieldlayoutfields`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `blg_idx_ykdfxrmwbglumjjhzkqoyzehdibbovsnjzwc` (`layoutId`,`fieldId`),
  ADD KEY `blg_idx_ezbafrlxdslqznvyzosexjbjroprfqgamnly` (`sortOrder`),
  ADD KEY `blg_idx_zoykwbpjpjqzfpxhwhrgyawftqrfeiddrmly` (`tabId`),
  ADD KEY `blg_idx_qcmvtgbduogwudsrmjikhjxqzqmerwewjibj` (`fieldId`);

--
-- Indexes for table `blg_fieldlayouts`
--
ALTER TABLE `blg_fieldlayouts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `blg_idx_jpsqbdbuupsyqnriaqtrkltqgzumqdsmnbug` (`dateDeleted`),
  ADD KEY `blg_idx_cidmoupuksqudcvssffdpjeazispqybkbqob` (`type`);

--
-- Indexes for table `blg_fieldlayouttabs`
--
ALTER TABLE `blg_fieldlayouttabs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `blg_idx_fcvjvbepnubxujorfxmqxhyllvpklwnwufma` (`sortOrder`),
  ADD KEY `blg_idx_aqeycwxpicgvqntaljshpzlfdxmqthewaiua` (`layoutId`);

--
-- Indexes for table `blg_fields`
--
ALTER TABLE `blg_fields`
  ADD PRIMARY KEY (`id`),
  ADD KEY `blg_idx_ktbyoppnzmuwbztxkgptyzxypixahjrbngju` (`handle`,`context`),
  ADD KEY `blg_idx_vftgypgdzogcqiuunzhuvjawswkbsecudpbv` (`groupId`),
  ADD KEY `blg_idx_pplhbdgemjnycfgtrkzavylpwgntmmkclwyy` (`context`);

--
-- Indexes for table `blg_globalsets`
--
ALTER TABLE `blg_globalsets`
  ADD PRIMARY KEY (`id`),
  ADD KEY `blg_idx_qkzvlqxkqlpnhinqngzqiduwwvvzjphesegj` (`name`),
  ADD KEY `blg_idx_drzmfuhcsqipbiryooommxwinuzgqlfgzyiz` (`handle`),
  ADD KEY `blg_idx_mgpgnfxgdgvctqjiosqmiahnvmvukiotmrpo` (`fieldLayoutId`),
  ADD KEY `blg_idx_scopwhwhmnsiwyjkagalwtmtdhktnuqxkeui` (`sortOrder`);

--
-- Indexes for table `blg_gqlschemas`
--
ALTER TABLE `blg_gqlschemas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blg_gqltokens`
--
ALTER TABLE `blg_gqltokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `blg_idx_csvildclhtyofrkehkqdjmviesumaynuxsdk` (`accessToken`),
  ADD UNIQUE KEY `blg_idx_cgqjchyhajitkxlzeqftnwamjpgjejfqkujk` (`name`),
  ADD KEY `blg_fk_wcbdkvhyymmbdqzwjttcxtqqsbhcfffaosep` (`schemaId`);

--
-- Indexes for table `blg_info`
--
ALTER TABLE `blg_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blg_matrixblocks`
--
ALTER TABLE `blg_matrixblocks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `blg_idx_gmpnmdytysaudpfnneuoxxcfyhowfthgfobp` (`ownerId`),
  ADD KEY `blg_idx_zizveymfgxquydjhgxafnbtehqqlkdofwnhf` (`fieldId`),
  ADD KEY `blg_idx_lggjnzbizyximptsvphjlhcqogtjipsgkltg` (`typeId`),
  ADD KEY `blg_idx_dtubkjwtehagervjxewcidjhflcggyuqapbx` (`sortOrder`);

--
-- Indexes for table `blg_matrixblocktypes`
--
ALTER TABLE `blg_matrixblocktypes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `blg_idx_pqqlhfttxahogepbxzhpklfgqcpfqpohoimn` (`name`,`fieldId`),
  ADD KEY `blg_idx_srfnsdrlepotzusxoywkmkggykyxxzpgepbk` (`handle`,`fieldId`),
  ADD KEY `blg_idx_xjtlzdfakpbkfyshpdurzelehxulsaclooki` (`fieldId`),
  ADD KEY `blg_idx_tosbpjyhbyfaxdbfojfisnxbcbpbsfdgajzv` (`fieldLayoutId`);

--
-- Indexes for table `blg_migrations`
--
ALTER TABLE `blg_migrations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `blg_idx_bezzoekeztkzlwbfaffseikxqjhlbuflafmg` (`track`,`name`);

--
-- Indexes for table `blg_plugins`
--
ALTER TABLE `blg_plugins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `blg_idx_gxmycrkqanoiptndbdzgmgjnnkpzgiwhaoiy` (`handle`);

--
-- Indexes for table `blg_projectconfig`
--
ALTER TABLE `blg_projectconfig`
  ADD PRIMARY KEY (`path`);

--
-- Indexes for table `blg_queue`
--
ALTER TABLE `blg_queue`
  ADD PRIMARY KEY (`id`),
  ADD KEY `blg_idx_divplafanqpmeicmrfqntchldlfkkjivseom` (`channel`,`fail`,`timeUpdated`,`timePushed`),
  ADD KEY `blg_idx_ymoocsnwgtxbwklugafugoafviohqrvzbmfi` (`channel`,`fail`,`timeUpdated`,`delay`);

--
-- Indexes for table `blg_relations`
--
ALTER TABLE `blg_relations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `blg_idx_qgdgdprvhnkwxlqhxrxdjjstiuewbslwgccj` (`fieldId`,`sourceId`,`sourceSiteId`,`targetId`),
  ADD KEY `blg_idx_nyuzmljopmbprtnbnpuysrdugydftlsqtqoj` (`sourceId`),
  ADD KEY `blg_idx_ajlzavmofjdgvrhbrewiszjbnvqobztvphuy` (`targetId`),
  ADD KEY `blg_idx_hfjtzfrllnidjabrvdjygvthuoundegemzmx` (`sourceSiteId`);

--
-- Indexes for table `blg_resourcepaths`
--
ALTER TABLE `blg_resourcepaths`
  ADD PRIMARY KEY (`hash`);

--
-- Indexes for table `blg_revisions`
--
ALTER TABLE `blg_revisions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `blg_idx_yoxdlknhhkaazzoumsimjdtgsrhhxqwztmud` (`sourceId`,`num`),
  ADD KEY `blg_fk_kbxwrquounczrcatblhkygbmvhmrkgnsjwpo` (`creatorId`);

--
-- Indexes for table `blg_searchindex`
--
ALTER TABLE `blg_searchindex`
  ADD PRIMARY KEY (`elementId`,`attribute`,`fieldId`,`siteId`);
ALTER TABLE `blg_searchindex` ADD FULLTEXT KEY `blg_idx_ovyirxqtkcurkcddbmkyhbwbgjeqmtetudsl` (`keywords`);

--
-- Indexes for table `blg_sections`
--
ALTER TABLE `blg_sections`
  ADD PRIMARY KEY (`id`),
  ADD KEY `blg_idx_neyumcglmylceemegotcvymxpiphrphqpmbq` (`handle`),
  ADD KEY `blg_idx_ldeqcuivepjpffklpgzvbhkbeikynnejzdwr` (`name`),
  ADD KEY `blg_idx_lyihjjiudmyifndpraecfaipkqnmtphttadq` (`structureId`),
  ADD KEY `blg_idx_aqtzqhlgwnkeztejxilurprqhhrghpgqhrny` (`dateDeleted`);

--
-- Indexes for table `blg_sections_sites`
--
ALTER TABLE `blg_sections_sites`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `blg_idx_igkaqbvazpatwsoxnquppoycobpemttvmhfk` (`sectionId`,`siteId`),
  ADD KEY `blg_idx_cybhvbcbcskyeixubrajeqakuirpzfwafzlb` (`siteId`);

--
-- Indexes for table `blg_sequences`
--
ALTER TABLE `blg_sequences`
  ADD PRIMARY KEY (`name`);

--
-- Indexes for table `blg_sessions`
--
ALTER TABLE `blg_sessions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `blg_idx_tvltbwobkbpvrhvrwdcahobdggrizplhwuds` (`uid`),
  ADD KEY `blg_idx_sxvadqzxnobpanrzzsqzfdmmgefwhfrepuvu` (`token`),
  ADD KEY `blg_idx_webirzqnqgaiphertzzkwulxwdkpzznkiiew` (`dateUpdated`),
  ADD KEY `blg_idx_zrerdsnheukyvpkgznjwtjlowzovosnmcddz` (`userId`);

--
-- Indexes for table `blg_shunnedmessages`
--
ALTER TABLE `blg_shunnedmessages`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `blg_idx_cnnxuxtnzwttdoxnfqespqzykzeyrhqnjlim` (`userId`,`message`);

--
-- Indexes for table `blg_sitegroups`
--
ALTER TABLE `blg_sitegroups`
  ADD PRIMARY KEY (`id`),
  ADD KEY `blg_idx_vffmosxyylzdvbypyjbnztadtkqhrpmumbdf` (`name`);

--
-- Indexes for table `blg_sites`
--
ALTER TABLE `blg_sites`
  ADD PRIMARY KEY (`id`),
  ADD KEY `blg_idx_qzgflkoyqsxkskzoqgfdpnvblfozrbuxxytv` (`dateDeleted`),
  ADD KEY `blg_idx_zdszpcwjixnevpfzkgqmthrbehxysetzscmf` (`handle`),
  ADD KEY `blg_idx_txgihxwwfjuvmwwpdghafyvtepooaztvkdqw` (`sortOrder`),
  ADD KEY `blg_fk_ubdyvudpyqdelcollyarbrkbbudjkwczxrzt` (`groupId`);

--
-- Indexes for table `blg_structureelements`
--
ALTER TABLE `blg_structureelements`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `blg_idx_gfqovnuffgdujuhhlpldpdwbbefgqbwbrbjt` (`structureId`,`elementId`),
  ADD KEY `blg_idx_llasrmiasplsebbjpfvgipsvtldecewracsr` (`root`),
  ADD KEY `blg_idx_settialomjvarxzvtllnpqzypppwvyptjqny` (`lft`),
  ADD KEY `blg_idx_nodndstorqkkgwumszsmnjsftjcsiwvuwnkt` (`rgt`),
  ADD KEY `blg_idx_zoihnzqpjuuljsvdhwcgtvnvmmhgatwzcmxb` (`level`),
  ADD KEY `blg_idx_rkiwdmzlgzinqblxyyvtaorsljiykbvgfbtp` (`elementId`);

--
-- Indexes for table `blg_structures`
--
ALTER TABLE `blg_structures`
  ADD PRIMARY KEY (`id`),
  ADD KEY `blg_idx_jvvcdlfuhwrqeftkacvdcvvetjhdurzuklpa` (`dateDeleted`);

--
-- Indexes for table `blg_systemmessages`
--
ALTER TABLE `blg_systemmessages`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `blg_idx_rynqbdcybptxpcvittpmyvrxgsveeiqglbgm` (`key`,`language`),
  ADD KEY `blg_idx_ztmhsnvdbdteqspvesmtecyuklyixssmfltg` (`language`);

--
-- Indexes for table `blg_taggroups`
--
ALTER TABLE `blg_taggroups`
  ADD PRIMARY KEY (`id`),
  ADD KEY `blg_idx_drihnikijrvugjawagazdyxwyeyfqszyhwkd` (`name`),
  ADD KEY `blg_idx_yfdfekewlupqbggedbvekgasuwngaucljnll` (`handle`),
  ADD KEY `blg_idx_xpxphdtykxnklmwuacrmqvvlurnyuwrpydsz` (`dateDeleted`),
  ADD KEY `blg_fk_ghpqafeirzquxrmrqmtizhghdvvlefjuthhw` (`fieldLayoutId`);

--
-- Indexes for table `blg_tags`
--
ALTER TABLE `blg_tags`
  ADD PRIMARY KEY (`id`),
  ADD KEY `blg_idx_ljnjuqkbuqefrbafbahxidnfqwwrrftvczkq` (`groupId`);

--
-- Indexes for table `blg_templatecacheelements`
--
ALTER TABLE `blg_templatecacheelements`
  ADD PRIMARY KEY (`id`),
  ADD KEY `blg_idx_bluftsifchmnmpgldiuqinjtoovcggtnfphu` (`cacheId`),
  ADD KEY `blg_idx_yhiheowsqoohiwmoilrhinsexycfnifppxxn` (`elementId`);

--
-- Indexes for table `blg_templatecachequeries`
--
ALTER TABLE `blg_templatecachequeries`
  ADD PRIMARY KEY (`id`),
  ADD KEY `blg_idx_wmllfebnrvmvkwylwtpkcqzypjvbzyvoocxt` (`cacheId`),
  ADD KEY `blg_idx_bcvfpxhaeeqlptxtfxguuwlvsqwdogxbffxz` (`type`);

--
-- Indexes for table `blg_templatecaches`
--
ALTER TABLE `blg_templatecaches`
  ADD PRIMARY KEY (`id`),
  ADD KEY `blg_idx_yrczweotgenafrzqdrctwytsabenypryqedj` (`cacheKey`,`siteId`,`expiryDate`,`path`),
  ADD KEY `blg_idx_vjojnxzpgdqwvpkufmqopgojprcmzuomoagl` (`cacheKey`,`siteId`,`expiryDate`),
  ADD KEY `blg_idx_tjnmwusakseisqksderuqxlfgfnzmhzmfjem` (`siteId`);

--
-- Indexes for table `blg_tokens`
--
ALTER TABLE `blg_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `blg_idx_hhvrsiuihsauhobnrvpkbljftxwmclfkpdwy` (`token`),
  ADD KEY `blg_idx_vpmelojiwqqhaewqdabkdqhptdzshpagzhob` (`expiryDate`);

--
-- Indexes for table `blg_usergroups`
--
ALTER TABLE `blg_usergroups`
  ADD PRIMARY KEY (`id`),
  ADD KEY `blg_idx_xqunlxyvogpdsltyvjcgovapvxintcwdqfcq` (`handle`),
  ADD KEY `blg_idx_sdjcywiucuazmtrofeivtddcpabcjhwqdmxy` (`name`);

--
-- Indexes for table `blg_usergroups_users`
--
ALTER TABLE `blg_usergroups_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `blg_idx_zqeolwytrnwzwvclcsijmcmvdrtvreukggrz` (`groupId`,`userId`),
  ADD KEY `blg_idx_jaepzrzsdtxxtjcmmknkdhmlsdrrpabudcsp` (`userId`);

--
-- Indexes for table `blg_userpermissions`
--
ALTER TABLE `blg_userpermissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `blg_idx_nuiegylfgzhadbkpffqhmxgnnyfokmlkvtwg` (`name`);

--
-- Indexes for table `blg_userpermissions_usergroups`
--
ALTER TABLE `blg_userpermissions_usergroups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `blg_idx_hflbclxxlybrcjughpmppnookwwslasdwktx` (`permissionId`,`groupId`),
  ADD KEY `blg_idx_uxtlhojfkoqlkyuqtxsyjosdvowjjqkmgzoj` (`groupId`);

--
-- Indexes for table `blg_userpermissions_users`
--
ALTER TABLE `blg_userpermissions_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `blg_idx_mxuarhztkcvwpplpwcyfqhzbnjljhsrgrowb` (`permissionId`,`userId`),
  ADD KEY `blg_idx_jjlacvlnvjbdwgadwlffbrgnlskebmjcssvq` (`userId`);

--
-- Indexes for table `blg_userpreferences`
--
ALTER TABLE `blg_userpreferences`
  ADD PRIMARY KEY (`userId`);

--
-- Indexes for table `blg_users`
--
ALTER TABLE `blg_users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `blg_idx_ecgeusmibryeckxmwkayrksqwbdstqjjvmxq` (`uid`),
  ADD KEY `blg_idx_hcqujgubnzmvenisenriifqhnllzzultetja` (`verificationCode`),
  ADD KEY `blg_idx_diejlvfrmohzfgmgjbjyjtnjvhxjqlmsmscw` (`email`),
  ADD KEY `blg_idx_ehrmawtjjjecpmifcwmsetxttvrxamspzrlr` (`username`),
  ADD KEY `blg_fk_wjusncrnyeloofbxdghwrrpxkeadncvxwyxx` (`photoId`);

--
-- Indexes for table `blg_volumefolders`
--
ALTER TABLE `blg_volumefolders`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `blg_idx_msndjuohfshuqnmhkjmrorjcrlkwxfoyaklc` (`name`,`parentId`,`volumeId`),
  ADD KEY `blg_idx_gxneebbjdpwslhmctztrzmurhzlcmzbquikp` (`parentId`),
  ADD KEY `blg_idx_ompptvwhyfvzlqzzywshekqbgxqbpvqgpdnt` (`volumeId`);

--
-- Indexes for table `blg_volumes`
--
ALTER TABLE `blg_volumes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `blg_idx_lakbpnxxzufnlpceogpncbyvqqywcfcnxrff` (`name`),
  ADD KEY `blg_idx_godtmupxuefzadkejhddqtzhhklifdttagnj` (`handle`),
  ADD KEY `blg_idx_busnualkchgdlkcjbtujtypzvuhgheadedxj` (`fieldLayoutId`),
  ADD KEY `blg_idx_iknovekqvecmsknnwrmqegikpdkkzpfexgog` (`dateDeleted`);

--
-- Indexes for table `blg_widgets`
--
ALTER TABLE `blg_widgets`
  ADD PRIMARY KEY (`id`),
  ADD KEY `blg_idx_tkylaygdmicmuefljeizdpaamvfarsxtscul` (`userId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `blg_announcements`
--
ALTER TABLE `blg_announcements`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `blg_assetindexdata`
--
ALTER TABLE `blg_assetindexdata`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `blg_assettransformindex`
--
ALTER TABLE `blg_assettransformindex`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `blg_assettransforms`
--
ALTER TABLE `blg_assettransforms`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `blg_categorygroups`
--
ALTER TABLE `blg_categorygroups`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `blg_categorygroups_sites`
--
ALTER TABLE `blg_categorygroups_sites`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `blg_content`
--
ALTER TABLE `blg_content`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=83;

--
-- AUTO_INCREMENT for table `blg_craftidtokens`
--
ALTER TABLE `blg_craftidtokens`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `blg_deprecationerrors`
--
ALTER TABLE `blg_deprecationerrors`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=90;

--
-- AUTO_INCREMENT for table `blg_drafts`
--
ALTER TABLE `blg_drafts`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `blg_elementindexsettings`
--
ALTER TABLE `blg_elementindexsettings`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `blg_elements`
--
ALTER TABLE `blg_elements`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=83;

--
-- AUTO_INCREMENT for table `blg_elements_sites`
--
ALTER TABLE `blg_elements_sites`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=83;

--
-- AUTO_INCREMENT for table `blg_entrytypes`
--
ALTER TABLE `blg_entrytypes`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `blg_fieldgroups`
--
ALTER TABLE `blg_fieldgroups`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `blg_fieldlayoutfields`
--
ALTER TABLE `blg_fieldlayoutfields`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT for table `blg_fieldlayouts`
--
ALTER TABLE `blg_fieldlayouts`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `blg_fieldlayouttabs`
--
ALTER TABLE `blg_fieldlayouttabs`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `blg_fields`
--
ALTER TABLE `blg_fields`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `blg_globalsets`
--
ALTER TABLE `blg_globalsets`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `blg_gqlschemas`
--
ALTER TABLE `blg_gqlschemas`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `blg_gqltokens`
--
ALTER TABLE `blg_gqltokens`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `blg_info`
--
ALTER TABLE `blg_info`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `blg_matrixblocktypes`
--
ALTER TABLE `blg_matrixblocktypes`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `blg_migrations`
--
ALTER TABLE `blg_migrations`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=192;

--
-- AUTO_INCREMENT for table `blg_plugins`
--
ALTER TABLE `blg_plugins`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `blg_queue`
--
ALTER TABLE `blg_queue`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=220;

--
-- AUTO_INCREMENT for table `blg_relations`
--
ALTER TABLE `blg_relations`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=105;

--
-- AUTO_INCREMENT for table `blg_revisions`
--
ALTER TABLE `blg_revisions`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `blg_sections`
--
ALTER TABLE `blg_sections`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `blg_sections_sites`
--
ALTER TABLE `blg_sections_sites`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `blg_sessions`
--
ALTER TABLE `blg_sessions`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `blg_shunnedmessages`
--
ALTER TABLE `blg_shunnedmessages`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `blg_sitegroups`
--
ALTER TABLE `blg_sitegroups`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `blg_sites`
--
ALTER TABLE `blg_sites`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `blg_structureelements`
--
ALTER TABLE `blg_structureelements`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `blg_structures`
--
ALTER TABLE `blg_structures`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `blg_systemmessages`
--
ALTER TABLE `blg_systemmessages`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `blg_taggroups`
--
ALTER TABLE `blg_taggroups`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `blg_templatecacheelements`
--
ALTER TABLE `blg_templatecacheelements`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `blg_templatecachequeries`
--
ALTER TABLE `blg_templatecachequeries`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `blg_templatecaches`
--
ALTER TABLE `blg_templatecaches`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `blg_tokens`
--
ALTER TABLE `blg_tokens`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `blg_usergroups`
--
ALTER TABLE `blg_usergroups`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `blg_usergroups_users`
--
ALTER TABLE `blg_usergroups_users`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `blg_userpermissions`
--
ALTER TABLE `blg_userpermissions`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `blg_userpermissions_usergroups`
--
ALTER TABLE `blg_userpermissions_usergroups`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `blg_userpermissions_users`
--
ALTER TABLE `blg_userpermissions_users`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `blg_userpreferences`
--
ALTER TABLE `blg_userpreferences`
  MODIFY `userId` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `blg_volumefolders`
--
ALTER TABLE `blg_volumefolders`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `blg_volumes`
--
ALTER TABLE `blg_volumes`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `blg_widgets`
--
ALTER TABLE `blg_widgets`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `blg_announcements`
--
ALTER TABLE `blg_announcements`
  ADD CONSTRAINT `blg_fk_uptgrmtabgczeebwcmwhutglopuluwisizku` FOREIGN KEY (`userId`) REFERENCES `blg_users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `blg_fk_yyriptaccbdfqkssmkpzoqmdemshcdcwhezc` FOREIGN KEY (`pluginId`) REFERENCES `blg_plugins` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `blg_assetindexdata`
--
ALTER TABLE `blg_assetindexdata`
  ADD CONSTRAINT `blg_fk_fxkonmcgwfowmyuybfjitbmnxyuqckgqlmlj` FOREIGN KEY (`volumeId`) REFERENCES `blg_volumes` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `blg_assets`
--
ALTER TABLE `blg_assets`
  ADD CONSTRAINT `blg_fk_dwnxwbhklgsipmeafyrgdoyisgypojphzxda` FOREIGN KEY (`volumeId`) REFERENCES `blg_volumes` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `blg_fk_lnvjbmjozyhtdlieyzfztkiiktrdawjmznxi` FOREIGN KEY (`folderId`) REFERENCES `blg_volumefolders` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `blg_fk_spvhdroqlwvtgnzodsrjxuofxhgpacwpqfuy` FOREIGN KEY (`uploaderId`) REFERENCES `blg_users` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `blg_fk_wycrozrhirzrkblaqoozwufksoezqpcvjbrh` FOREIGN KEY (`id`) REFERENCES `blg_elements` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `blg_categories`
--
ALTER TABLE `blg_categories`
  ADD CONSTRAINT `blg_fk_begqevjyzegmuvzdroskenkwevuizpcxuhgi` FOREIGN KEY (`id`) REFERENCES `blg_elements` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `blg_fk_juezjvgubfkvkjzydhverenfmchiypynrqpe` FOREIGN KEY (`groupId`) REFERENCES `blg_categorygroups` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `blg_fk_vjdjhkrgqtbovlawyokiydojeljiqfuopgml` FOREIGN KEY (`parentId`) REFERENCES `blg_categories` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `blg_categorygroups`
--
ALTER TABLE `blg_categorygroups`
  ADD CONSTRAINT `blg_fk_alwmglpyelydwfowvqedxzwjxryqexhleopd` FOREIGN KEY (`fieldLayoutId`) REFERENCES `blg_fieldlayouts` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `blg_fk_gvslwymsddqluvqvlcnxxyiddeitundyeffz` FOREIGN KEY (`structureId`) REFERENCES `blg_structures` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `blg_categorygroups_sites`
--
ALTER TABLE `blg_categorygroups_sites`
  ADD CONSTRAINT `blg_fk_jeuiwmqfgoqrthyzlvzkvvulybrhgipisjyr` FOREIGN KEY (`groupId`) REFERENCES `blg_categorygroups` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `blg_fk_wmpnhkhassfhtyizmuukpvochdgthshvhlfn` FOREIGN KEY (`siteId`) REFERENCES `blg_sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `blg_changedattributes`
--
ALTER TABLE `blg_changedattributes`
  ADD CONSTRAINT `blg_fk_gxfsqtbsjwqsfpsohobwnmnofehnsncsfouv` FOREIGN KEY (`siteId`) REFERENCES `blg_sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `blg_fk_nvhmnhsuxglhjdepnhqgxncjemjkjqvlgbiu` FOREIGN KEY (`userId`) REFERENCES `blg_users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `blg_fk_qgtfhcuqxgkaaqrianzsnmkqfhaauretyzla` FOREIGN KEY (`elementId`) REFERENCES `blg_elements` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `blg_changedfields`
--
ALTER TABLE `blg_changedfields`
  ADD CONSTRAINT `blg_fk_kxvrncolgulkflmuktyokjneddqcbfmclzng` FOREIGN KEY (`fieldId`) REFERENCES `blg_fields` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `blg_fk_lclmzxuysoqarjhkieiqqdvsgvjjzqymhsqq` FOREIGN KEY (`elementId`) REFERENCES `blg_elements` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `blg_fk_oxsqbwnaaqrbtahlamcdogahsykjujyucfws` FOREIGN KEY (`userId`) REFERENCES `blg_users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `blg_fk_prtewntgrztacwyimoszoakulaeqzceclsgg` FOREIGN KEY (`siteId`) REFERENCES `blg_sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `blg_content`
--
ALTER TABLE `blg_content`
  ADD CONSTRAINT `blg_fk_pbjsuzlqwdlujvvxmvztuiciyoenenbnrlkv` FOREIGN KEY (`elementId`) REFERENCES `blg_elements` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `blg_fk_uzaazvibliawzdgsqfhtqhetvlpogltzoqyg` FOREIGN KEY (`siteId`) REFERENCES `blg_sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `blg_craftidtokens`
--
ALTER TABLE `blg_craftidtokens`
  ADD CONSTRAINT `blg_fk_zcdweptnrqecwpctsvbuhsgcskgmniarkwyt` FOREIGN KEY (`userId`) REFERENCES `blg_users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `blg_drafts`
--
ALTER TABLE `blg_drafts`
  ADD CONSTRAINT `blg_fk_ejaqfcahizsouofjkkaieuqcwbduzfhetwsb` FOREIGN KEY (`creatorId`) REFERENCES `blg_users` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `blg_fk_vhkbpehonjwudhsnrpyemolvldiuzmzsimie` FOREIGN KEY (`sourceId`) REFERENCES `blg_elements` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `blg_elements`
--
ALTER TABLE `blg_elements`
  ADD CONSTRAINT `blg_fk_ktkjasnbxbczlodiuyxtycahjsxmoaaxlogs` FOREIGN KEY (`fieldLayoutId`) REFERENCES `blg_fieldlayouts` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `blg_fk_ppfgkwrofcneotvkfummlrtfmovnszpeungf` FOREIGN KEY (`revisionId`) REFERENCES `blg_revisions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `blg_fk_pzfbjziebojaonyrfolanwoxjticwxtagxdq` FOREIGN KEY (`canonicalId`) REFERENCES `blg_elements` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `blg_fk_zryekljnycqjfwyakxmjbxntqqfroxzhjvsd` FOREIGN KEY (`draftId`) REFERENCES `blg_drafts` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `blg_elements_sites`
--
ALTER TABLE `blg_elements_sites`
  ADD CONSTRAINT `blg_fk_mwopsbqctguqitqeknjflfwligunejfvmfqn` FOREIGN KEY (`siteId`) REFERENCES `blg_sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `blg_fk_ybpvmdzcasbwusedclgbbdhhcoxbeodrkqic` FOREIGN KEY (`elementId`) REFERENCES `blg_elements` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `blg_entries`
--
ALTER TABLE `blg_entries`
  ADD CONSTRAINT `blg_fk_bgisvafqndtkjcdtlsykcqzcquuermyapjga` FOREIGN KEY (`id`) REFERENCES `blg_elements` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `blg_fk_kbhlkbgbnxcufbeyfgipwyfksnenmdytwqmt` FOREIGN KEY (`parentId`) REFERENCES `blg_entries` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `blg_fk_scldhfazmwstpyujrbdoxfiivkebdfwntipf` FOREIGN KEY (`authorId`) REFERENCES `blg_users` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `blg_fk_toalnjbiiaixqhlslscyhakzyklsrwtkohpm` FOREIGN KEY (`sectionId`) REFERENCES `blg_sections` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `blg_fk_xyzlbnwdzktjkleoxfahmlyjkskbnmogmkad` FOREIGN KEY (`typeId`) REFERENCES `blg_entrytypes` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `blg_entrytypes`
--
ALTER TABLE `blg_entrytypes`
  ADD CONSTRAINT `blg_fk_icbpvoodakodmspthdnuizejgvfebepvkepk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `blg_fieldlayouts` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `blg_fk_mclzhqnazvkkbyzueptpztqknbzgsfzmwzuv` FOREIGN KEY (`sectionId`) REFERENCES `blg_sections` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `blg_fieldlayoutfields`
--
ALTER TABLE `blg_fieldlayoutfields`
  ADD CONSTRAINT `blg_fk_ismsyhuhtgpmtjjauyccpaayvopzoylejvts` FOREIGN KEY (`tabId`) REFERENCES `blg_fieldlayouttabs` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `blg_fk_mcxtztaxhpsgewngpkdgqknbxejrwtjzmwym` FOREIGN KEY (`layoutId`) REFERENCES `blg_fieldlayouts` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `blg_fk_psmbtnyjpqmikgfrkccgfyjdvbfonfjvodxa` FOREIGN KEY (`fieldId`) REFERENCES `blg_fields` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `blg_fieldlayouttabs`
--
ALTER TABLE `blg_fieldlayouttabs`
  ADD CONSTRAINT `blg_fk_egmkxgcigjyqtwfzvqpszajiykrdnjhvgfag` FOREIGN KEY (`layoutId`) REFERENCES `blg_fieldlayouts` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `blg_fields`
--
ALTER TABLE `blg_fields`
  ADD CONSTRAINT `blg_fk_dezigkzrslernqaqjbfcrcbliawwqjyepivq` FOREIGN KEY (`groupId`) REFERENCES `blg_fieldgroups` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `blg_globalsets`
--
ALTER TABLE `blg_globalsets`
  ADD CONSTRAINT `blg_fk_kvwusmmhxcdpckcjrwerytztumepldvuzwdu` FOREIGN KEY (`id`) REFERENCES `blg_elements` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `blg_fk_mteufieopjfdqwdmpvlkrrpuazykevhmrpbc` FOREIGN KEY (`fieldLayoutId`) REFERENCES `blg_fieldlayouts` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `blg_gqltokens`
--
ALTER TABLE `blg_gqltokens`
  ADD CONSTRAINT `blg_fk_wcbdkvhyymmbdqzwjttcxtqqsbhcfffaosep` FOREIGN KEY (`schemaId`) REFERENCES `blg_gqlschemas` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `blg_matrixblocks`
--
ALTER TABLE `blg_matrixblocks`
  ADD CONSTRAINT `blg_fk_cphifbfepyskzkkjhxgahgdxmjagpirhlmdn` FOREIGN KEY (`fieldId`) REFERENCES `blg_fields` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `blg_fk_dhprteefbnyjrheutkllzcxcsynacqhsuexd` FOREIGN KEY (`id`) REFERENCES `blg_elements` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `blg_fk_vpmjyltzkomqrigwdjixpmunejxcqeqyrrsl` FOREIGN KEY (`typeId`) REFERENCES `blg_matrixblocktypes` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `blg_fk_wmbknvzwgegrbekvapnbqgqodrlduserraus` FOREIGN KEY (`ownerId`) REFERENCES `blg_elements` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `blg_matrixblocktypes`
--
ALTER TABLE `blg_matrixblocktypes`
  ADD CONSTRAINT `blg_fk_idxczuhlqlxwjepgzmgsygxiywxkmntvfspn` FOREIGN KEY (`fieldId`) REFERENCES `blg_fields` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `blg_fk_pdgtdudjwysoysrnpveegreyqxrkpqvukotl` FOREIGN KEY (`fieldLayoutId`) REFERENCES `blg_fieldlayouts` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `blg_relations`
--
ALTER TABLE `blg_relations`
  ADD CONSTRAINT `blg_fk_brhyxykknusmtjkfhuvwkknhtnjgutzukskw` FOREIGN KEY (`fieldId`) REFERENCES `blg_fields` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `blg_fk_dslqbqtqdbnifmyjklgnzluynzqnurtaikmq` FOREIGN KEY (`sourceSiteId`) REFERENCES `blg_sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `blg_fk_efwzbfqcfserxykaadcholsnwaahoozjrcee` FOREIGN KEY (`targetId`) REFERENCES `blg_elements` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `blg_fk_mrocqxruhokwocwjsrihowilwjerhudpfhpg` FOREIGN KEY (`sourceId`) REFERENCES `blg_elements` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `blg_revisions`
--
ALTER TABLE `blg_revisions`
  ADD CONSTRAINT `blg_fk_kbxwrquounczrcatblhkygbmvhmrkgnsjwpo` FOREIGN KEY (`creatorId`) REFERENCES `blg_users` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `blg_fk_ltoowqakhbhcyomjsiifmaaszozvcvqupfql` FOREIGN KEY (`sourceId`) REFERENCES `blg_elements` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `blg_sections`
--
ALTER TABLE `blg_sections`
  ADD CONSTRAINT `blg_fk_knorgrtbzfemwnouycqkymtwzblanfpaccqv` FOREIGN KEY (`structureId`) REFERENCES `blg_structures` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `blg_sections_sites`
--
ALTER TABLE `blg_sections_sites`
  ADD CONSTRAINT `blg_fk_euhrtewkzmnbbwltvmnvpfdxozjvcgbrwbxx` FOREIGN KEY (`sectionId`) REFERENCES `blg_sections` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `blg_fk_vspsspvjoalfjcwowipbcinqvfmarhaaczac` FOREIGN KEY (`siteId`) REFERENCES `blg_sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `blg_sessions`
--
ALTER TABLE `blg_sessions`
  ADD CONSTRAINT `blg_fk_dkabdxdetzeoiohwumdaemncncasruvjomup` FOREIGN KEY (`userId`) REFERENCES `blg_users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `blg_shunnedmessages`
--
ALTER TABLE `blg_shunnedmessages`
  ADD CONSTRAINT `blg_fk_owtgzuajsdiyrxgytefytaafvekgaunioqks` FOREIGN KEY (`userId`) REFERENCES `blg_users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `blg_sites`
--
ALTER TABLE `blg_sites`
  ADD CONSTRAINT `blg_fk_ubdyvudpyqdelcollyarbrkbbudjkwczxrzt` FOREIGN KEY (`groupId`) REFERENCES `blg_sitegroups` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `blg_structureelements`
--
ALTER TABLE `blg_structureelements`
  ADD CONSTRAINT `blg_fk_lwgjhhkjfstsobydtkgkckakjzzaqrtmmafl` FOREIGN KEY (`structureId`) REFERENCES `blg_structures` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `blg_fk_wtzoceaxafblwnfvaubechgpwsfncuccisqz` FOREIGN KEY (`elementId`) REFERENCES `blg_elements` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `blg_taggroups`
--
ALTER TABLE `blg_taggroups`
  ADD CONSTRAINT `blg_fk_ghpqafeirzquxrmrqmtizhghdvvlefjuthhw` FOREIGN KEY (`fieldLayoutId`) REFERENCES `blg_fieldlayouts` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `blg_tags`
--
ALTER TABLE `blg_tags`
  ADD CONSTRAINT `blg_fk_jrxizscaghitboyodkonjsqqzejiundbqxmu` FOREIGN KEY (`id`) REFERENCES `blg_elements` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `blg_fk_tmkunuapedtomltvkpfcfxnryqtcxljlchmf` FOREIGN KEY (`groupId`) REFERENCES `blg_taggroups` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `blg_templatecacheelements`
--
ALTER TABLE `blg_templatecacheelements`
  ADD CONSTRAINT `blg_fk_glapfdnhkyivdqenujfffpmfqvrljnszjxir` FOREIGN KEY (`elementId`) REFERENCES `blg_elements` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `blg_fk_pjgckbdczaiptdkrnlebxljtctqjugnzopqv` FOREIGN KEY (`cacheId`) REFERENCES `blg_templatecaches` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `blg_templatecachequeries`
--
ALTER TABLE `blg_templatecachequeries`
  ADD CONSTRAINT `blg_fk_kwgdfrsjxaycwmqlzbhbfhhztjqbdylzsizp` FOREIGN KEY (`cacheId`) REFERENCES `blg_templatecaches` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `blg_templatecaches`
--
ALTER TABLE `blg_templatecaches`
  ADD CONSTRAINT `blg_fk_pqdhsywylvcsrkghgjeibilqefxwbibgecbl` FOREIGN KEY (`siteId`) REFERENCES `blg_sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `blg_usergroups_users`
--
ALTER TABLE `blg_usergroups_users`
  ADD CONSTRAINT `blg_fk_gppzxnpiunxtmvrjroxqhzqobfthfsetndny` FOREIGN KEY (`groupId`) REFERENCES `blg_usergroups` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `blg_fk_szfzqsibkzndqxsvgczrrupzpqlannoxozcd` FOREIGN KEY (`userId`) REFERENCES `blg_users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `blg_userpermissions_usergroups`
--
ALTER TABLE `blg_userpermissions_usergroups`
  ADD CONSTRAINT `blg_fk_cfakgfmxbvfjvvzmqhinkpaxvuendjlqhhrk` FOREIGN KEY (`groupId`) REFERENCES `blg_usergroups` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `blg_fk_ypykabeoohcmubyrtvzwdjnkcyupaopkkzqr` FOREIGN KEY (`permissionId`) REFERENCES `blg_userpermissions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `blg_userpermissions_users`
--
ALTER TABLE `blg_userpermissions_users`
  ADD CONSTRAINT `blg_fk_buqojrhnsxeuupflctejkmhgcaxwtxzihhnh` FOREIGN KEY (`userId`) REFERENCES `blg_users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `blg_fk_rzqavodwpnjiwkgmvskzbfwlrwvycdhcatwc` FOREIGN KEY (`permissionId`) REFERENCES `blg_userpermissions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `blg_userpreferences`
--
ALTER TABLE `blg_userpreferences`
  ADD CONSTRAINT `blg_fk_bddujmaodglcubxnmrssxhxgefwtromtsyjk` FOREIGN KEY (`userId`) REFERENCES `blg_users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `blg_users`
--
ALTER TABLE `blg_users`
  ADD CONSTRAINT `blg_fk_wjusncrnyeloofbxdghwrrpxkeadncvxwyxx` FOREIGN KEY (`photoId`) REFERENCES `blg_assets` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `blg_fk_zmmijvojaqcmzpyxuimukkqtnkqqxcqwvsbi` FOREIGN KEY (`id`) REFERENCES `blg_elements` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `blg_volumefolders`
--
ALTER TABLE `blg_volumefolders`
  ADD CONSTRAINT `blg_fk_aucsytmxhkcbvuzdyzvkuqqwyhyypptgdtqg` FOREIGN KEY (`volumeId`) REFERENCES `blg_volumes` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `blg_fk_bldxjuhshitjfxhdvdhomzcoxanayjnwmown` FOREIGN KEY (`parentId`) REFERENCES `blg_volumefolders` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `blg_volumes`
--
ALTER TABLE `blg_volumes`
  ADD CONSTRAINT `blg_fk_xtukshnzrtdbafirzzoewtiunyhfoddknyvq` FOREIGN KEY (`fieldLayoutId`) REFERENCES `blg_fieldlayouts` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `blg_widgets`
--
ALTER TABLE `blg_widgets`
  ADD CONSTRAINT `blg_fk_thfngomqtvycfvxalrnhvbkewpmqyvvbxqtf` FOREIGN KEY (`userId`) REFERENCES `blg_users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
